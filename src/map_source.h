﻿#ifndef MAP_SOURCE_H
#define MAP_SOURCE_H
#include <mutex>
#include <string>
#include <vector>

/// Base class for map sources
/// TODO Add packet query: vector<Tiles> -> vector<string>
class MapSource
{
protected:
    enum CacheType {SASPlanet, GlobalMapper};
public:
    struct Info {
        const std::string& name;
        const std::vector<int>& zoom_level;
        bool is_topo;
        const std::string& crs;
    };
    enum LayerType {Sat=false, Topo=true};

    virtual std::string tileUrl(int x, int y, int zoom) const = 0;
    virtual std::string tilePath(int x, int y, int zoom) const = 0;
    Info

    info() const {return {name, zoom_level, is_topo, crs};}

    /// Source name without extra qualification - Google, Yandex, ...
    const std::string name;
    /// available zoom levels
    const std::vector<int> zoom_level;
    const bool is_topo;
    const std::string crs;
    const std::string cache_name;
    const std::string content_type;
    bool operator==(const MapSource& rhs) {
        return name==rhs.name && is_topo==rhs.is_topo;
    }
protected:
    MapSource(const std::string map_name,
              const std::vector<int> zoom,
              const bool topo,
              const std::string crs_name,
              const std::string cache_name,
              const std::string content_type) :
        name(map_name), zoom_level(zoom), is_topo(topo),
        crs(crs_name), cache_name(cache_name),
        content_type(content_type){}

    unsigned long long random(int max) const {return static_cast<unsigned long long>(rand() % max);}
    std::string tilePath(int x, int y, int zoom, CacheType cache_type) const;

};


class MapSourceCustom : public MapSource
{
public:
    MapSourceCustom(const std::string map_name,
              std::vector<int> zoom,
              const bool topo,
              const std::string crs_name) :
        MapSource(map_name, zoom, topo, crs_name, std::string(), std::string()) {}
    std::string tileUrl(int x, int y, int zoom) const override {return "";}
    std::string tilePath(int x, int y, int zoom) const override {return "";}
};



#endif //MAP_SOURCE_H

