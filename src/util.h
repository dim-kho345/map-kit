#ifndef UTIL_H
#define UTIL_H
#include <string>
std::string date2String(time_t unix_date);

///return prefix formatted as yyyy-mm-dd_Location
std::string kitPrefix(time_t unix_date, std::string location);

/// required by some grids
float delphiDate(time_t unix_time);

std::string directoryFromPath(const std::string& full_path);

std::string filenameFromPath(std::string full_path);

#endif // UTIL_H
