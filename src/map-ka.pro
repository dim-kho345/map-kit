#-------------------------------------------------
#
# Project created by QtCreator 2019-12-07T15:03:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network printsupport

TARGET = map-ka
TEMPLATE = app

CONFIG += c++17

INCLUDEPATH += c:/msys64/usr/local/include

LIBS += -LC:/msys64/usr/local/lib -lgdal -lproj -lsqlite3 -lopencv_world430 -ljpeg \
         -lturbojpeg -lzip -lzlib -lws2_32 -liconv -lpsapi -LC:/msys64/mingw64/lib -lssl -lcrypto

SOURCES += main.cpp\
    container/local_model.cpp \
    crs.cpp \
    crs_image.cpp \
    geo.cpp \
    gui/dialog_download.cpp \
    gui/dialog_import_map.cpp \
    gui/dialog_import_points.cpp \
    gui/dialog_settings.cpp \
    gui/gui.cpp \
    gui/gui_adapter.cpp \
    gui/mainwindow.cpp \
    gui/map_navigator.cpp \
    gui/map_view.cpp \
    gui/table_maps.cpp \
    gui/table_printouts.cpp \
    gui/wiki_points.cpp \
    log.cpp \
    map_loader.cpp \
    model/model_impl.cpp \
    model/model_map.cpp \
    ozi.cpp \
    path_resolver.cpp \
    portable/util_port.cpp \
    proxy/proxy_factory.cpp \
    proxy/remote_client.cpp \
    proxy/remote_server.cpp \
    queue.cpp \
    serv_func.cpp \
    service/download.cpp \
    service/generator2coords.cpp \
    service/generator_grid.cpp \
    service/guru_converter.cpp \
    service/hlg_reader.cpp \
    service/image_enhancer.cpp \
    service/img2jnx/geowarp.cpp \
    service/img2jnx/jnx_converter.cpp \
    service/img2jnx/kmz_converter.cpp \
    service/img2ozf/color_transform.cpp \
    service/img2ozf/ozf_converter.cpp \
    service/img2ozf/palette.cpp \
    service/map_source.cpp \
    service/map_specs.cpp \
    service/map_supplier.cpp \
    service/packager.cpp \
    service/printout_producer.cpp \
    service/service.cpp \
    gui/table_folders.cpp \
    service/service_factory.cpp \
    service/stitcher.cpp \
    service/wiki2wpt.cpp \
    settings.cpp \
    tiles/sas_planet.cpp \
    tiles/sqlite.cpp \
    tiles/storage_factory.cpp \
    util.cpp

HEADERS  += \
    config.h \
    container/local_model.h \
    crs.h \
    crs_image.h \
    gui/data_roles.h \
    gui/dialog_download.h \
    gui/dialog_import_map.h \
    gui/dialog_settings.h \
    gui/gui.h \
    gui/gui_adapter.h \
    gui/mainwindow.h \
    gui/map_navigator.h \
    gui/map_view.h \
    gui/table_maps.h \
    gui/ui_dialog_settings.h \
    gui/ui_mainwindow.h \
    gui/ui_navigator.h \
    log.h \
    map_loader.h \
    map_source.h \
    model/model.h \
    model/model_impl.h \
    ozi.h \
    path_resolver.h \
    portable/util_port.h \
    print_options.h \
    proxy/factory.h \
    proxy/remote_client.h \
    proxy/remote_server.h \
    service/download.h \
    service/factory.h \
    service/generator2coords.h \
    service/generator_grid.h \
    service/guru_converter.h \
    service/hlg_reader.h \
    service/image_enhancer.h \
    service/img2jnx/geowarp.h \
    service/img2jnx/jnx_converter.h \
    service/img2jnx/kmz_converter.h \
    service/img2ozf/color_transform.h \
    service/img2ozf/ozf_converter.h \
    service/img2ozf/palette.h \
    service/map_specs.h \
    service/map_supplier.h \
    geo.h \
    queue.h \
    service.h \
    serv_func.h \
    service/packager.h \
    service/printout_producer.h \
    service/stitcher.h \
    service/wiki2wpt.h \
    settings.h \
    tiles/factory.h \
    tiles/sas_planet.h \
    tiles/sqlite.h \
    tiles/tiles_storage.h \
    util.h

FORMS += \
    gui/dialog_settings.ui \
    gui/mainwindow.ui \
    gui/navigator.ui

RESOURCES += \
    resource.qrc

win32:RC_ICONS += ../resource/globus.ico
