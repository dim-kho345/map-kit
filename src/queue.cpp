#include <typeinfo>
#include <map>
#include <algorithm>
#include <utility>
#include <thread>
#include "queue.h"
#include "service/factory.h"
#include "log.h"

void ServiceQueue::serviceFinished(ServiceFunction func, int ctx, bool complete)
{
    auto model_ctx=queued.at(ctx).service->model_ctx;

    Logger::printf("Service finished: func/queue_ctx/model_id/compl: %s, %i, %i, %i",
                   servFuncName(func).c_str(),
                   ctx,
                   model_ctx,
                   complete);

    auto& item = queued.at(ctx);
    if(item.is_running)
        free_resources |= item.service->resources;
    item.is_running=false;
    item.deleted=true;
    gui->onProgress(item.service->function(), item.service->model_ctx, item.service->queue_ctx, -1);

    ///@todo а че не под switch-ем
    if(func==DownloadMap)
    {
        model->onTilesReady(model_ctx, complete);
    }
    else
        switch (func)
        {
        case LoadMap:
        {
            auto srv = static_cast<ServiceLoadMap*>(item.service.get());
            model->onImageReady(model_ctx, complete,
                                srv->img_width, srv->img_height,
                                srv->icon_width, srv->icon_height);
        }
            break;
        case Stitch:
        {
            auto srv = static_cast<ServiceStitcher*>(item.service.get());
            model->onImageReady(model_ctx, complete,
                                srv->img_width, srv->img_height,
                                srv->icon_width, srv->icon_height);
        }
            break;
        case Conv2Ozf:
            model->onOzfReady(model_ctx, complete);
            break;
        case Conv2Kmz:
            model->onKmzReady(model_ctx, complete);
            break;
        case Conv2Jnx:
            model->onJnxReady(model_ctx, complete);
            break;
        case Conv2Guru:
            model->onGuruReady(model_ctx, complete);
            break;
        case Enhance:
            model->onEnhanceReady(model_ctx);
            break;
        case GenGrid:
        {
            auto srv = static_cast<ServiceGeneratorGrid*>(item.service.get());
            model->onGridReady(srv->grid);
        }
            break;
        case Wiki2Wpt:
            model->onWikiPntReady();
            break;
        case ZipFolder:
            model->onZipFolderReady(model_ctx, complete);
            break;
        }
    queueDispather();
}

void ServiceQueue::enqueueService(int model_id, std::unique_ptr<ServiceBase> service)
{
    for(auto& s : queued)
    {
        if(s.second.deleted)
            continue;
        // only unique service for one model context and only single
        // function for non-map associated functions
        if((s.second.service->function()==service->function() && model_id==s.second.service->model_ctx) ||
               (model_id==-1 && s.second.service->function()==service->function()))
            return;
    }

    auto queue_ctx = generateQueueId();
    service->model_ctx = model_id;
    service->queue_ctx = queue_ctx;
    service->setParentQueue(this);

    queued.emplace(queue_ctx, QueueItem(std::move(service)));
    auto& srv = queued.at(queue_ctx).service;
    gui->onProgress(srv->function(), model_id, queue_ctx, 0, servFuncName(srv->function()) + ": awaiting start");
    queueDispather();
}


void ServiceQueue::abortService(int id)
{
    auto& service = queued.at(id).service;
    if(!queued.at(id).deleted)
        service->abort();
    //else
//        serviceFinished(service->function(), service->queue_ctx, false);
}

void ServiceQueue::abortServiceByFunction(ServiceFunction func)
{
    for (auto&  s : queued)
        if(s.second.service->function() == func)
            abortService(s.first);

}

/// @todo внимание - удалется только первый бегущий сервис. Все нужно делать в цикле
/// чтобы прошарить ВСЕ сервисы касающиеся данной карты.Вобщем, сейчас тут баг.
void ServiceQueue::abortServiceByMap(int map_id)
{
    auto queue_it = std::find_if(queued.cbegin(), queued.cend(),
                                 [map_id](const decltype (queued)::value_type& val)->bool
    {
        auto& srv = val.second.service;
        ///@todo этот артефакт удалить, model_ctx сейчас уникальный, проверка вводилась из-за этого
        if(isMapAssociated(srv->function()) &&
                srv->model_ctx == map_id &&
                val.second.is_running)
            return true;
        return false;
    });
    if(queue_it != queued.cend())
        abortService(queue_it->first);
}

void ServiceQueue::abortServices(AbortFilterId fid, int map_id)
{
}

void ServiceQueue::abortServiceByFolder(int folder_id)
{
    auto queue_it = std::find_if(queued.cbegin(), queued.cend(),
                                 [folder_id](const decltype (queued)::value_type& val)->bool
    {
        auto& srv = val.second.service;
        if( isFolderAssociated(srv->function()) &&
                srv->model_ctx == folder_id)
            return true;
        return false;
    });

    if(queue_it != queued.cend())
        abortService(queue_it->first);
}


void ServiceQueue::abortServiceByPrint(int print_id)
{
    auto queue_it = std::find_if(queued.cbegin(), queued.cend(),
                                 [print_id](const decltype (queued)::value_type& val)->bool
    {
        auto& srv = val.second.service;
        if( isPrintAssociated(srv->function()) &&
                srv->model_ctx == print_id)
            return true;
        return false;
    });

    if(queue_it != queued.cend())
        abortService(queue_it->first);
}


/// @todo вообще то запускать надо не в цикле, а только выбирать кандидата в цикле.
/// Так можно сохранить целостность очереди без ухищрений.
void ServiceQueue::queueDispather()
{
    /* Since function is reentrant, invariant of container integrity should be kept
       so "deleted" flag used instead of erasure and consecutive possible container node invalidation

       Вообще то в цикле нужно выбирать кандидата, запуская его вне цикла
       И только когда кандидатов не осталось, выходить из функции
    */
    for(auto& s : queued)
    {
        auto& service=s.second.service;
        if(s.second.deleted || s.second.is_running)
            continue;
        auto res = service->resources;
        // every service that doesn't need any resources, should be ran immediately
        if(!res)
        {
            s.second.is_running=true;
            service->start();
        }
        // здесь нужен else, чтобы возможно удаленный к этому времени service
        // не пытался запуститься второй раз при res==0 && free_resourses==0
        else
            // if all resources available
            if((res & free_resources) == res)
            {
                free_resources &= ~res;
                s.second.is_running=true;
                service->start();
            }
    }
}

bool ServiceQueue::abortFilter(ServiceQueue::AbortFilterId f,  const struct ServiceQueue::QueueItem &it)
{
    //switch (f)
    //{
    return true;
    //}
}
