#include <QFile>
#include <QDir>
#include <QRegExp>
#include <QTextStream>
#include "ozi.h"
#include "settings.h"
#include "geo.h"
#include "util.h"

void OziMapHeader::writeHeader(GeoRegion region,
                               ImgSize img_size,
                               bool is_topo,
                               int grid_step,
                               std::string img_path,
                               const std::string &map_path)
{
    using namespace std;
    auto last_slash = img_path.rfind('/');
    if(last_slash != string::npos)
        img_path = img_path.substr(last_slash+1);

    QString img_filename(QString::fromStdString(img_path));

    QFile ozi_map(QString::fromStdString(map_path));

    if (!ozi_map.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }

    uint32_t x,y;
    int num=1;
    int deg;
    double min;

    ///@todo взять CRS из карты
    ImageCRS img_crs(region, CRS::EPSG4326,img_size.width,img_size.height,CRS::EPSG4326);
    GeoPoint gp;
    LLPoint p(gp);
    auto& w = img_size.width;
    auto& h = img_size.height;

    QTextStream str(&ozi_map);

    str << "OziExplorer Map Data File Version 2.2\n";
    str << img_filename << "\n";
    str << img_filename << "\n";
    str << "1 ,Map Code,\n";
    str << "WGS 84,, 0.0000, 0.0000,WGS 84\n";
    str << "Reserved 1\nReserved 2\n";
    str << "Magnetic Variation,,,E\n";
    str << "Map Projection,Mercator,PolyCal,No,AutoCalOnly,No,BSBUseWPX,No\n";

    str.setRealNumberPrecision(5);
    for(uint32_t i=0;i<3;i++)
        for(uint32_t j=0;j<3;j++)
        {
            x=(w-1)*i/2;
            y=(h-1)*j/2;
            str << "Point0" << num++ << ",xy, " << x << ", " << y << ",in, deg, ";
            gp=img_crs.geoPoint(x,y);
            GeoCalc::toDegMin(p.lat,deg,min);
            str << deg << ", " << min << ",N, ";
            GeoCalc::toDegMin(p.lon,deg,min);
            str << deg << ", " << min << ",E, grid,,,,N\n";
        }
    while(num!=31)
    {
        str << "Point" << num++ << ",xy, , ,in, deg, , ,N, , ,W, grid, , , ,N\n";
    }
    LLPoint nw(region.nw), se(region.se);
    str << "Projection Setup,,,,,,,,,,\n";
    str << "Map Feature = MF ; Map Comment = MC     These follow if they exist\n";
    str << "Track File = TF      These follow if they exist\n";
    str << "Moving Map Parameters = MM?    These follow if they exist\n";
    str << "MM0,Yes" << endl << "MMPNUM,4" << endl;
    str << "MMPXY,1,0,0" << endl;
    str << "MMPXY,2," << w << ",0" << endl;
    str << "MMPXY,3," << w << ',' << h << endl;
    str << "MMPXY,4,0," << h << endl;
    str.setRealNumberPrecision(8);
    str << "MMPLL,1, " << nw.lon << ", "  << nw.lat << endl;
    str << "MMPLL,2, " << se.lon << ", "  << nw.lat << endl;
    str << "MMPLL,3, " << se.lon << ", "  << se.lat << endl;
    str << "MMPLL,4, " << nw.lon << ", "  << se.lat << endl;
    str << "MM1B,170.352987\n";
    str << "MOP,Map Open Position,0,0\n";
    str << "IWH,Map Image Width/Height," << w << ',' << h << endl;

    if(grid_step != TurnOffGrid)
    {
        if(!is_topo)
            str << "GRGRID,Yes," << grid_step << " m,No,16777215,16777215,"
                                                 "No Labels,0,16777215,8,1,Yes,No,No,x";
        else
            str << "GRGRID,Yes," << grid_step << " m,No,255,255,"
                                                 "No Labels,0,16777215,8,1,Yes,No,No,x";
    }

    ozi_map.close();
}


bool OziMapHeader::tryOpenFile(QString fpath)
{
    assert(fpath.contains((QRegExp("[/\\\\]"))));
    int32_t h=0,w=0;
    GeoPoint nw,se;
    QString line;
    QStringList item;
    QFile map_file(fpath),img_file;
    if(!map_file.open(QFile::ReadOnly | QFile::Text))
    {
        error="Error opening MAP file";
        return false;
    }
    QTextStream str(&map_file);

    // check general headers
    line=str.readLine();
    if(!line.startsWith("OziExplorer Map Data File Version"))
    {
        error="Not an OZI MAP file";
        return false;
    }

    std::vector<QString> trial_path;
    trial_path.push_back(str.readLine());
    trial_path.push_back(str.readLine());
    for(auto& t : trial_path)
    {
        if(t.contains(QRegExp("[/\\\\]")))
        {
            t.replace('\\','/');
            if(QFile::exists(t))
                img_path = t;
        }
        else
        {
            QDir d(QString::fromStdString(directoryFromPath(fpath.toStdString())));
            img_path = d.absoluteFilePath(t);
        }
        img_file.setFileName(img_path);
        if(img_file.open(QFile::ReadOnly))
        {
            img_file.close();
            break;
        }
        else
            img_path.clear();
    }
    if(img_path.isEmpty())
    {
        error="Failed to open image";
        return false;
    }

    // adobt georeference from IWH and MMPLL fields
    while(!str.atEnd())
    {
        line=str.readLine();
        line=line.simplified();
        item=line.split(QRegExp(" *, *"));

        if(item.isEmpty())
            continue;

        if(item.at(0)=="Map Projection")
        {
            if(item.at(1)!="Mercator")
            {
                error="Only mercator projection supported";
                return false;
            }
        }
        if(item.at(0)=="IWH")
        {
            w=item.at(2).toLong();
            h=item.at(3).toLong();
        }
        if(item.at(0)=="MMPLL")
        {
            if(item.at(1)==QChar('1'))
                nw=GeoPoint{item.at(2).toDouble(), item.at(3).toDouble()};
            if(item.at(1)==QChar('3'))
                se=GeoPoint{item.at(2).toDouble(), item.at(3).toDouble()};
        }
    }

    // check for absurd
    if(h<=0 || w<=0 || h>50000 || w>50000 || !nw || !se)
    {
        error="Coordinate binding invalid";
        return false;
    }

    map_info.size.height=h;
    map_info.size.width=w;
    map_info.reg={nw, se};
    map_info.img_path = img_path.toStdString();
    return true;
}


bool OziWptLoader::load(const std::string& path)
{
    QFile file(QString::fromStdString(path));
    if(!file.open(QFile::ReadOnly | QFile::Text))
        return false;
    QString line;
    QStringList param;
    QTextStream str(&file);
    line = str.readLine();
    if(!line.startsWith("OziExplorer Waypoint File"))
    {
        err = "Not an Ozi points file";
        return false;
    }
    line = str.readLine();
    if(!line.startsWith("WGS 84"))
    {
        err = "Wrong datum. WGS84 supported only";
        return false;
    }
    str.readLine();
    str.readLine();
    int num;
    int num_old=0;

    poi.clear();
    while(!str.atEnd())
    {
        line=str.readLine();
        line=line.simplified();
        param=line.split(QRegExp(" *, *"));

        num=param.at(0).toInt();

        GeoPoint c{param.at(3).toDouble(),
                    param.at(2).toDouble()};

        if(num!= (num_old+1) || !c)
        {
            err = "Error in line num "+std::to_string(num);
            poi.clear();
            return false;
        }
        ++num_old;
        poi.emplace_back(param.at(1).toStdString(),param.at(7).toInt(),c);
    }
    return true;
}


void OziWptLoader::parse()
{
    grid.clear();
    // TODO switch to STL regexp
    QRegExp is_grid("[A-Z][A-Z]?\\d{1,2}");

    // differentiate users points from grid points
    auto is_grid_func=[&](const POIPoint& p ) {return is_grid.exactMatch(QString::fromStdString(p.name));} ;
    copy_if(poi.begin(),poi.end(),std::back_inserter(grid),is_grid_func);
    poi.erase(remove_if(poi.begin(),poi.end(),is_grid_func),poi.end());

    if(grid.size() < 30)
    {
        bound1={0,0,0};
        bound2={0,0,0};
        return;
    }

    auto top_left=* std::min_element(grid.begin(),grid.end(),
                                     [&](const LLPoint& p1,const LLPoint &p2) {
        if(p1.lon < p2.lon && p1.lat > p2.lat)
            return true;
        else
            return false;
    });

    int zone=GeoCalc::utmZone(top_left);
    grid_orig_name = top_left.name;

    // get grid bounds
    double x1=10000000,x2=0,y1=100000000,y2=0;
    for(auto& g : grid)
    {
        UTMPoint utm=g.toUTM(zone);
        if(utm.x<x1) x1=round(utm.x);
        if(utm.x>x2) x2=round(utm.x);
        if(utm.y<y1) y1=round(utm.y);
        if(utm.y>y2) y2=round(utm.y);
    }
    bound1={x1,y2,zone};
    bound2={x2,y1,zone};

    // get grid step analyzing first 20 points
    int cnt250{0},cnt500{0},cnt1000{0};


    // calculate every to every point distance, filter those
    // which approximately equal to one of possible grid steps, append to statistics
    for (auto it=grid.cbegin();it!=grid.cbegin()+20;it++)
    {
        float min_distance=2000;
        for(auto& j : grid)
        {
            float d=GeoCalc::distance(*it,j);
            if(fabsf(d)>1.0 && d<min_distance)
                min_distance=d;
        }
        if(fabsf(min_distance-250) <5) ++cnt250;
        if(fabsf(min_distance-500) <10) ++cnt500;
        if(fabsf(min_distance-1000) <10) ++cnt1000;
    }

    if(!cnt250 && !cnt500 && !cnt1000)
        return;
    // voting result
    if(cnt250>cnt500)
        grid_step=250;
    else
        grid_step=500;
    if(cnt1000>cnt250 && cnt1000>cnt500)
        grid_step=1000;

    return;
}

std::pair<unsigned, unsigned> OziWptLoader::getGridFirstPoint() const
{
    return {grid_orig_name[0]-'A',
                std::stoul(grid_orig_name.substr(1))-1};
}


