#ifndef LOG_H
#define LOG_H

#include <mutex>
#include <fstream>
#include <chrono>
#include <thread>

/// @todo Записывать в лог по окончании работы map_id folder_id, все сервисы и т.п. списки


class Logger
{
public:
    template <class ...Args>
    void static write(Args&&... args)
    {
        using namespace std::chrono;
        std::lock_guard<std::mutex> lock(mx);
        auto time = system_clock::now().time_since_epoch();
        auto ts = duration_cast<milliseconds>(time).count();
        log << ts << ':' << std::this_thread::get_id() << ": ";

        (log << ... << args) << std::endl;
    }

    template <class ...Args>
    void static printf(const char* format, Args&&... args)
    {
        using namespace std::chrono;
        std::lock_guard<std::mutex> lock(mx);
        auto time = system_clock::now().time_since_epoch();
        auto ts = duration_cast<milliseconds>(time).count();
        log << ts << ':' << std::this_thread::get_id() << ": ";
        sprintf_s(line, sizeof(line), format, args...);
        (log << std::string(line)) << std::endl;
    }

private:
    static char line[256];
    static std::mutex mx;
    static std::ofstream log;
};

#endif // LOG_H
