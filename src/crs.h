#ifndef CRS_H
#define CRS_H
#include <utility>
#include <string>
#include <unordered_map>
#include <memory>
#include "gdal/gdal.h"
#include "gdal/ogr_spatialref.h"


class CRS
{
public:
    using CoordRefSys = int;


    constexpr static int NoChange = -2;
    constexpr static int InvalidCRS = -1;
    constexpr static int EPSG3395 = 3395;
    constexpr static int EPSG3785 = 3785;
    constexpr static int EPSG4326 = 4326;

    // base of UTM zones, first zone goes as 32600+1
    constexpr static int EPSG_UTM = 32600;

    static std::pair<double, double> convert(CoordRefSys src,
                                             CoordRefSys dst,
                                             double x,
                                             double y);

    using Transform = std::pair<CoordRefSys, CoordRefSys>;
    static std::unordered_map<Transform, std::unique_ptr<OGRCoordinateTransformation>> transform;

    static CoordRefSys crsFromString(const std::string& crs_name);
    static std::unordered_map<std::string, CoordRefSys> crs_table;
private:
    static const char* wktFromCRSEnum(CoordRefSys crs);
};

namespace std {

// EPSG max code is 32768, thus dst/src combination fits into 32-bit word
template<>
struct hash<CRS::Transform>
{
    size_t operator()(const CRS::Transform& val) const {
        return val.first*0x10000 + val.second;
    }
};

}



#endif // CRS_H
