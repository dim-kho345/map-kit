#include "serv_func.h"

static std::map<ServiceFunction, const std::string> name
{
    {DownloadMap, "download"},
    {Stitch,"stitch"},
    {Conv2Ozf,"img2ozf"},
    {Conv2Jnx,"img2jnx"},
    {Conv2Kmz,"img2kmz"},
    {Conv2Guru,"img2guru"},
    {Enhance,"enh"},
    {Printout,"print"},
    {ZipFolder,"zip"},
    {UploadZip,"upload"},
    {LoadWpt,"loadwpt"},
    {CoordFile,"coord2"},
    {Wiki2Wpt,"wiki2wpt"},
    {Gen2Coords,"gen2coord"},
    {GenGrid,"gengrid"},
    {LoadMap,"loadmap"}
};


const std::string servFuncName(ServiceFunction func)
{
    return name.at(func);
}
