#include <fstream>
#include "settings.h"
#include "log.h"
#include "windows.h"

Settings settings;

Settings::Settings(const std::string &path, bool read_only):
    read_only(read_only)
{
    openFile(path);
}

void Settings::openFile(const std::string& file_path)
{
    using namespace std;
    path=file_path;
    char line[256];
    string str;
    ifstream inf(path);
    if(!inf.is_open())
        return;
    while(!inf.eof())
    {
        inf.getline(line,sizeof(line));
        str=line;
        auto pos = str.find('=');
        if(pos == string::npos)
            continue;
        const auto& tag = str.substr(0, pos);
        const auto& value = str.substr(pos+1);
        setValue(tag, value);
    }
    valid=true;
}

void Settings::setValue(const std::string& tag, const std::string& value)
{
    value_map[tag] = value;
}

const std::string& Settings::value(const std::string& tag) const
{
    try {
        return value_map.at(tag);
    } catch (const std::out_of_range&) {
        throw(std::out_of_range("Invalid requested value " + tag +" at " + path));
    }

}

void Settings::save() const
{
    using namespace std;
    ofstream outf(path);
    for(auto& v : value_map)
        outf << v.first << '=' << v.second << '\n';
}


const std::string& Settings::operator[](const std::string& tag) {
    try{
        return value(tag);
    }
    catch(std::out_of_range& e)    {
        throw(std::out_of_range("Non-existent parameter: " + tag));
    }
    catch(std::exception& e)    {
        Logger::write("Settings class exception: ", tag);
        throw;
    }
}
