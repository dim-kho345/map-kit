#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <map>

/// General maintainer for settings formatted as "Parameter=Value" text lines
class Settings
{
public:
    using ValueMap = std::map<std::string, std::string>;

    Settings()=default;
    Settings(const std::string& path, bool read_only=false);
    ~Settings() {
        if(valid && !read_only)
            save();
    }

    const std::string& value(const std::string& tag) const;
    ValueMap& values() {return value_map;}

    void openFile(const std::string &file_path);
    void setValue(const std::string& tag, const std::string& value) ;

    void save() const;

    const std::string& operator[](const std::string& tag);

    bool isValid() const {return valid;}
private:
    bool valid{false};
    bool read_only;
    std::string path;
    ValueMap value_map;
};

extern Settings settings;

#endif // SETTINGS_H
