#ifndef PRINT_FORMATS_H
#define PRINT_FORMATS_H
#include <variant>
#include <vector>

enum PrintFormat : int
{
    A0,
    A1,
    A2,
    A3,
    A4
};

enum PrintType : int
{
    Whole,
    Splitted,
    Map4Group
};


using PrintOptions  = std::vector<std::variant<PrintFormat, PrintType>>;

#endif // PRINT_FORMATS_H

