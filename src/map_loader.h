#ifndef MAPLOADER_H
#define MAPLOADER_H

#include "crs_image.h"
#include "service.h"


/**
 * @brief Load map from bitmap
 * @details crop or extent source image to fit given geo region
 * The image is treated to have EPSG3785 projection
 */

class MapLoader : public ServiceLoadMap
{
public:
    MapLoader(GUI* p_gui, Resources res=Memory) :
        ServiceLoadMap(p_gui, res) {}
    void start() override;
    void abort() override {}
private:
    bool load(std::string ozi_map, GeoRegion kit_reg);
    std::string stage;
    enum ErrorCode{

    };
};

#endif // MAPLOADER_H
