#include <ctime>
#include <sstream>
#include <iomanip>
#include <timezoneapi.h>
#include "util.h"

std::string date2String(time_t unix_date)
{
    using namespace std;
    string res;
    stringstream ss(res);
    auto tm = localtime(&unix_date);
    ss << put_time(tm, "%Y-%m-%d");
    return ss.str();
}

std::string kitPrefix(time_t unix_date, std::string location)
{
    return date2String(unix_date)+ '_' + location;
}

float delphiDate(time_t unix_time)
{
    TIME_ZONE_INFORMATION tz;
    GetTimeZoneInformation(&tz);
    double time = unix_time - tz.Bias*60;
    time+=25569.0*86400;
    return time/=86400;
}

std::string directoryFromPath(const std::string& full_path)
{
    using namespace std;
    auto pos=full_path.find_last_of("/\\");
    if(pos != string::npos)
        return full_path.substr(0,pos);
    else return full_path;
}

std::string filenameFromPath(std::string full_path)
{
    using namespace std;
    auto pos=full_path.find_last_of("/\\");
    if(pos != string::npos)
        return full_path.substr(pos+1);
    else return full_path;
}
