#include "crs_image.h"

ImageCRS::ImageCRS(const GeoRegion r, CRS::CoordRefSys epsg_of_region,
                   unsigned width, unsigned height, CRS::CoordRefSys epsg):
    epsg(epsg),

    width(width),
    height(height)
{
    GeoPoint nw = CRS::convert(epsg_of_region, epsg, r.nw.x, r.nw.y);
    GeoPoint se = CRS::convert(epsg_of_region, epsg, r.se.x, r.se.y);
    x0 = nw.x;
    y0 = nw.y;
    dx = (se.x-nw.x)/width;
    dy = (se.y-nw.y)/height;
    resolution = (1/dx)/cos(r.nw.y*M_PI/180.0);
}

GeoPoint ImageCRS::geoPoint(double x,double y) const
{
    return {x0+x*dx,y0+y*dy};
}

GeoPoint ImageCRS::geoPoint(double x,double y, CRS::CoordRefSys dst) const
{
    Point p{x0+x*dx,y0+y*dy};
    return CRS::convert(epsg, dst, p.x, p.y);
}

Point ImageCRS::imgPoint(double x, double y) const
{
    Point res;
    x-=x0; y-=y0;
    res.x=x/dx;
    res.y=y/dy;
    return res;
}

Point ImageCRS::imgPoint(const Point& p) const
{
    return imgPoint(p.x, p.y);
}

Point ImageCRS::imgPoint(const Point& p, CRS::CoordRefSys src) const
{
    GeoPoint t(CRS::convert(src, epsg, p.x, p.y));
    return imgPoint(t);
}

Point ImageCRS::imgPointBounded (const Point& p) const
{
    return imgPoint(p.x, p.y);
}

Point ImageCRS::imgPointBounded(double x, double y) const
{
    auto p=imgPoint(x,y);
    if(p.x < 0) p.x = 0;
    if(p.y < 0) p.y = 0;
    if(p.x >= width-1) p.x = width-1;
    if(p.y >= height-1) p.y = height-1;
    return p;
}
