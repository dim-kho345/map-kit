#ifndef OZI_H
#define OZI_H

#include <QString>
#include <vector>
#include "geo.h"
#include "crs_image.h"

class OziMapHeader
{
public:

    static constexpr int TurnOffGrid{-0xff};
    struct Info
    {
        GeoRegion reg;
        ImgSize size;
        std::string img_path;
    };
    OziMapHeader() {}

    static void writeHeader(GeoRegion region,
                            ImgSize img_size,
                            bool is_topo,
                            int grid_step,
                            std::string img_path,
                            const std::string &map_path);

    /// @brief Parse MAP file, fill map_info and img_path structs
    /// @param fname absolute path to Ozi .map
    bool tryOpenFile(QString fpath);

    std::string getError() const {return error.toStdString();}

    Info info() {return map_info;}
    const QString& getMapImageFilepath() const {return img_path;}
private:
    /// filled by tryOpenFile()
    Info map_info;
    /// filed by tryOpenFile()
    QString img_path;

    QString error;
};

///@todo избавиться от "get" префиксов
class OziWptLoader
{
public:
    /**
     * @brief Load grid and/or custom points (POI)
     * @return False in case of parse error. If parsed without error, but num_points==0, return true
     */
    bool load(const std::string& path);

    /// Differentiate POI from grid points, get grid parameters if available
    void parse();

    bool hasGrid() const {return bound1 && bound2 && grid_step;}

    /// Grid bounds
    UTMRegion getBounds() const {
        return {bound1,bound2};
    }

    int getGridStep() const { return grid_step; }
    std::pair<unsigned, unsigned> getGridFirstPoint() const;


    /// Get points, (excluding grid points if parse() called)
    const std::vector<POIPoint>& points() const {return poi;}
    size_t numPoints() const {return poi.size();}
    const std::string& error() const {return err;}
private:
    /// calculated grid bounds
    UTMPoint bound1,bound2;
    /// scratch vector for grid points
    std::vector<POIPoint> grid;
    /// Points of Interest (all but grid points)
    std::vector<POIPoint> poi;
    std::vector<GeoPoint> gpoi;

    /// top left point name
    std::string grid_orig_name;
    std::string err;
    int grid_step{0};
};




#endif // OZI_H
