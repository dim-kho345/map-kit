#include "packager.h"
#include <QDir>
#include <QFile>
#include <QProcess>
#include <QCoreApplication>
#include <algorithm>
#include <set>

Packager::Packager(GUI* p_gui, Resources res):
    ServiceZip(p_gui, res)
{

}


void Packager::start()
{
    t = std::thread(&Packager::job,this);
    t.detach();
    //job();
}

void Packager::job()
{
    if(copyFiles())
    {
        if(!no_zip)
            zip();
        finished(true);
    }
    else
    {
        std::string res;
        for (auto& s : error_text)
        {
            res.append(s);
            res.append("\n");
        }
        res.pop_back();
        finishedVerbose(false, GUI::Warning, res);
    }
}


bool Packager::copyFiles()
{
    QDir d(QDir::currentPath());
    if(!d.mkpath(QString::fromStdString(kit_folder + '/' + folder)))
    {
        error_text.emplace_back("Не удалось создать папку " + folder);
        return false;
    }
    for(auto& f : file_copy)
    {
        auto dir = f.second.substr(0,f.second.rfind('/'));
        if(!d.mkpath(QString::fromStdString(dir)))
        {
            error_text.emplace_back("Не удалось создать папку " + dir);
            continue;
        }
        auto file = QString::fromStdString(f.second);
        if (QFile::exists(file))
            if(!QFile::remove(file))
            {
                error_text.emplace_back("Не удалось удалить файл " + f.second);
                continue;
            }
        if(!QFile::copy(QString::fromStdString(f.first),file))
        {
            error_text.emplace_back("Не удалось скопировать " + f.first + " -> " + f.second);
            continue;
        }
    }
    if(error_text.empty())
        return true;
    else
        return false;
}

void Packager::zip()
{
    QProcess zipper;
    zipper.setProgram(QCoreApplication::applicationDirPath()+"/7z.exe");
    zipper.setWorkingDirectory(QString::fromStdString(kit_folder));
    zipper.setArguments({"a", QString::fromStdString(zip_path),QString::fromStdString(folder)});
    zipper.start();
    zipper.waitForFinished();
}
