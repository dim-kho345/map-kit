#include <locale>
#include "hlg_reader.h"
#include "settings.h"

GeoRegion HLGReader::region(const std::string& path)
{
    try
    {
        Settings hlg(path,ReadOnly);
        return {
            {std::stod(hlg["PointLon_0"]), std::stod(hlg["PointLat_0"])},
            {std::stod(hlg["PointLon_2"]), std::stod(hlg["PointLat_2"])}
        };
    }
    catch (std::exception& ex)
    {
        return GeoRegion();
    }
}


std::string HLGReader::location(std::string path)
{
    using namespace std;

    // erase leading paths
    auto pos = path.rfind('/');
    if(pos != string::npos)
        path = path.substr(pos+1);

    //erase file extension
    pos = path.rfind('.');
    if(pos != string::npos)
        path = path.substr(0,pos);

    // strip leading date and non-letter non-numeric symbols
    auto name_beg = path.end()-1;
    while (isalnum(*name_beg, locale())
           &&
           name_beg!=path.begin())
        --name_beg;

    if(name_beg >= path.end()-1)
        return "InvalidName";
    return {isalnum(*name_beg, locale()) ? name_beg : name_beg+1, path.end()};
}
