#ifndef PRINTOUTPRODUCER_H
#define PRINTOUTPRODUCER_H

#include <atomic>
#include <QtPrintSupport/QPrinter>
#include <QPainter>
#include <thread>
#include "service.h"
#include "crs_image.h"

class PrintoutProducer : public ServicePrintout
{
public:
    PrintoutProducer(GUI* p_gui, Resources res=Memory);
    void start() override;
    void abort() override {abort_request=true;}
    using ServiceBase::report;
private:
    void job();
    std::atomic<int> abort_request{false};
    std::thread t;


    static constexpr bool GridOff{false};
    static constexpr bool GridOn{true};
    struct SuitCase
    {
        bool landscape;
        int cols;
        int rows;
    };

    using SuitCaseArr = std::vector<SuitCase>;
    using SuitCaseMap = std::map<double,SuitCase>;
    static const SuitCaseArr suit_a3_src;
    static const SuitCaseArr suit_a2_src;
    static const SuitCaseArr suit_a1_src;
    static const SuitCaseArr suit_a0_src;

    static SuitCaseMap suit_a3;
    static SuitCaseMap suit_a2;
    static SuitCaseMap suit_a1;
    static SuitCaseMap suit_a0;

    // grid UTM coords settings
    int32_t x1,x2;
    int32_t y1,y2;

    QString point_name;
    QPainter paint;

    static const std::unordered_map<QPageSize::PageSizeId,double> label_scale;

    static const std::unordered_map<PrintFormat, QPageSize::PageSizeId> page_format;

    bool printWhole();
    bool printWhole2();
    bool printSplitted();

    /// отношение ширины к высоте
    float imageAspect();
    /**
         * @brief Посчитать отношения ширины к высоте для склеек
         * @details Полученные отношения запихиваются в упорядоченный контейнер
         * и последовательно сравнивая их с отношением сторон изображения карты,
         * подбираем наилучшую раскладку склейки
         */
    static void calcSuitCases();

    /**
         * @brief Печать области карты с сеткой и точками
         * @details Основной воркер печатного дела
         * @param im изображение
         * @param p1 верхняя левая граница, Ш/Д
         * @param p2 правая нижняя граница, Ш/Д
         * @param pdf
         * @param scale размер листа по отноношению к размеру карты. т.е
         * как если взять размер листа в точках(пикселях) и разделить на нужную
         * сторону изображения карты
         * @param label_scale масштаб для обозначений точек, включая шрифт
         * @param print_grid нужно ли печатать сетку
         */
    void printArea(QImage* im, GeoRegion reg,
                   QPagedPaintDevice *pdf, double scale,
                   double label_scale, bool print_grid);

    void printPoint(const ImageCRS &im_c, const POIPoint &p, double scale, double label_scale);
    /// Подобрать масштаб для полного умещения. paper_size в мм
    static double fitImage2Paper(QSizeF paper_size,bool landscape,int32_t width,int32_t height);

};


#endif // PRINTOUTPRODUCER_H
