#include "service/factory.h"
#include "download.h"
#include "stitcher.h"
#include "image_enhancer.h"
#include "packager.h"
#include "printout_producer.h"
#include "generator_grid.h"
#include "img2ozf/ozf_converter.h"
#include "img2jnx/jnx_converter.h"
#include "img2jnx/kmz_converter.h"
#include "guru_converter.h"
#include "map_loader.h"

std::unique_ptr<ServiceBase> ServiceFactory::getServiceImpl(ServiceFunction func,
                                                        GUI* gui)
{
    switch(func)
    {
    case ServiceFunction::DownloadMap:
        return std::unique_ptr<ServiceBase>(new TileDownloader(gui));
    case ServiceFunction::LoadMap:
        return std::unique_ptr<ServiceBase>(new MapLoader(gui));
    case ServiceFunction::Stitch:
        return std::unique_ptr<ServiceBase>(new Stitcher(gui));
    case ServiceFunction::Conv2Ozf:
        return std::unique_ptr<ServiceBase>(new OzfConverter(gui));
    case ServiceFunction::Conv2Jnx:
        return std::unique_ptr<ServiceBase>(new JnxConverter(gui));
    case ServiceFunction::Conv2Kmz:
        return std::unique_ptr<ServiceBase>(new KmzConverter(gui));
    case ServiceFunction::Conv2Guru:
        return std::unique_ptr<ServiceBase>(new GuruConverter(gui));
    case ServiceFunction::Enhance:
        return std::unique_ptr<ServiceBase>(new ImageEnhancer(gui));
    case ServiceFunction::GenGrid:
        return std::unique_ptr<ServiceBase>(new GeneratorGrid(gui));
    case ServiceFunction::ZipFolder:
        return std::unique_ptr<ServiceBase>(new Packager(gui));
    case ServiceFunction::Printout:
        return std::unique_ptr<ServiceBase>(new PrintoutProducer(gui));
    case ServiceFunction::Gen2Coords:
        return std::unique_ptr<ServiceBase>(new ServiceGenerator2Coord(gui));

    default:
        break;
    }
}
