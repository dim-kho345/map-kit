#ifndef SERVICEGENERATORGRID_H
#define SERVICEGENERATORGRID_H

#include <QFile>
#include <QTextStream>
#include <QXmlStreamWriter>
#include "geo.h"
#include "service.h"




class PointsCloud
{
public:
    PointsCloud(std::string fname) :
         file(QString::fromStdString(fname)) {}
    virtual bool writeHeader()=0;
    virtual void writePoint(GeoPoint p,QString& name)=0;
    virtual void finishPoints() {}
    virtual bool startTrack(const QString&, int=0) {return true;}
    virtual void writeTrackPoint(GeoPoint) {}
    virtual void close()=0;
protected:
    QString point_col_name{"   "};
    QFile file;
};

class GPXFile : public PointsCloud
{
public:
    GPXFile(std::string fname) :
        PointsCloud (fname) {}
    bool writeHeader() override;
    void writePoint(GeoPoint p,QString& name) override;
    bool startTrack(const QString& name,int=0) override;
    void writeTrackPoint(GeoPoint p) override;
    void close() override;
private:
    QXmlStreamWriter xml;
    QString date_time;
};

class KMLFile : public PointsCloud
{
public:
    KMLFile(std::string fname, time_t unix_time) :
        PointsCloud (fname), date(unix_time) {}
    bool writeHeader() override;
    void writePoint(GeoPoint p,QString& name) override;
    void finishPoints() override;
    bool startTrack(const QString& name, int=0) override;
    void writeTrackPoint(GeoPoint p) override;
    void close() override;
private:
    QXmlStreamWriter xml;
    QString date_time;
    time_t date;
};

class DMXFile : public PointsCloud
{
public:
    DMXFile(std::string fname) :
        PointsCloud (fname) {}
    bool writeHeader() override;
    void writePoint(GeoPoint p,QString& name) override;
    void close() override;
private:
    QXmlStreamWriter xml;
    QString date_time;
    QTextStream str;
};

class WptFile : public PointsCloud
{
public:
    WptFile(std::string fname, time_t unix_time,std::string trk_fname) :
        PointsCloud(fname),trk_file(QString::fromStdString(trk_fname)), unix_time(unix_time) {}
    bool writeHeader() override;
    void writePoint(GeoPoint p,QString& name) override;
    bool startTrack(const QString& name, int num_points) override;
    void writeTrackPoint(GeoPoint p) override;
    void close() override;
private:
    bool points_opened{false},track_opened{false};
    QTextStream str;
    QTextStream tr_str;
    QFile trk_file;
    time_t unix_time;
    int num;
};


class GeneratorGrid : public ServiceGeneratorGrid
{
public:
    GeneratorGrid(GUI* p_gui, Resources res=Memory) :
        ServiceGeneratorGrid(p_gui, res) {}
    void start() override;
    void abort() override {}
    GeoPoint cvtUTM2EN(int x, int y, int zone);
    std::vector<std::unique_ptr<PointsCloud>> generator;
};

#endif // SERVICEGENERATORGRID_H
