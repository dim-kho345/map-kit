#ifndef WIKI2WPT_H
#define WIKI2WPT_H
#include <QProcess>
#include "service.h"


class Wiki2WptExecutor : public ServiceWiki2Wpt
{
public:
    Wiki2WptExecutor(GUI* pgui) :
        ServiceWiki2Wpt(pgui)
    {}
    void start() override;
    void abort() override {}
    bool failed() {return !poi.size();}
    auto points() {return poi;}
private:
    std::vector<POIPoint> poi;
    static bool setGrid(int grid_step);
    /// формат даты в Deplhi
    static bool setDate(float date);
    static bool setPlace(const std::string &place);
    static void setConfig();
    static void filterWpt(std::string in_filename, std::string nono_filename);
    static bool fix100(const std::string& path);
    QProcess proc;
};


#endif // WIKI2WPT_H
