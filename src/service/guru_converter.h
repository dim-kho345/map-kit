#ifndef GURUCONVERTER_H
#define GURUCONVERTER_H

#include "service.h"
#include "gui/gui.h"
#include <thread>
#include <atomic>
#include <sqlite3.h>
#include <opencv2/opencv.hpp>


class GuruConverter : public ServiceImg2Guru
{
public:
    GuruConverter(GUI* p_gui, Resources res=Memory);
    //~GuruConverter();
    void start() override;
    void abort() override;
    bool abortRequested() {
        return abort_request;
    }
    using ServiceBase::report;
private:
    void job();
    void convert();
    int zoomToGuru(int zoom) {return 17-zoom;}
    int zoomFromGuru(int zoom) {return 17-zoom;}
    std::pair<bool,std::string> createLayer(cv::Mat& im_src, sqlite3* db, int db_zoom, bool stich_from_tiles);
    std::atomic<int> abort_request{false};
    std::thread t;
    static constexpr int n_layers{SQLITEDB_LAYERS};
};

#endif // GURUCONVERTER_H
