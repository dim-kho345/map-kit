#include <fstream>
#include "service.h"
#include <QProcess>

void ServiceGenerator2Coord::start()
{
    using namespace std;

    ofstream outf(path, ios_base::binary);
    if(!outf.is_open())
        return;
    outf << location_name << "\n\n";
    outf  << "� ������� �������\\������\\���� �����, �������� � ������ ��� ����������� Garmin -\n";
    outf << GeoCalc::toDegMinString(geopoint) << "\n\n";
    outf.precision(7);
    outf << 'N' << geopoint.y << " "  << 'E' << geopoint.x << "\n\n";
    outf << GeoCalc::toDegMinSecString(geopoint) << "\n\n";
    outf.close();
    QProcess::startDetached("notepad", {QString::fromStdString(path)});

    ///@todo finished ��������� � ����� ������, � ��� ������� - ���� ��� ��������� �������, �� ������ �� ������
    // finished() �� ���������, ��������� � ������� �� �������
}
