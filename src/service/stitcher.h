#ifndef STITCHER_H
#define STITCHER_H
#include <thread>
#include <atomic>
#include "service.h"

class Stitcher : public ServiceStitcher
{
public:
    Stitcher(GUI* p_gui, Resources res=Memory);
    void start() override;
    void abort() override;
private:
    std::atomic_bool abort_request{false};
    void job();
    std::thread t;
    std::vector<Tile> task;
    TileRegion treg;

};

#endif // STITCHER_H
