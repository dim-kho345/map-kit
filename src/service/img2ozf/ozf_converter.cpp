#include <future>
#include "log.h"
#include "ozf_converter.h"
#include "palette.h"
#include "color_transform.h"




OzfConverter::OzfConverter(GUI* p_gui, Resources res) :
    ServiceImg2Ozf(p_gui, res),
    lut_100(new uint8_t[lut_size]),
    lut_scaled(new uint8_t[lut_size]),
    lut_rgb(new uint32_t[lut_size])
{
    tile_off.reserve(64000);
}

void  OzfConverter::start()
{
    //job();
    t=std::thread(&OzfConverter::job,this);
    t.detach();
}

void  OzfConverter::abort()
{
    abort_request=true;
    gui->onProgress(function(), model_ctx, -1, -1, "Aborting...");
}





void OzfConverter::job()
{
    using namespace std;
    cv::Mat im;
    report(0, "Loading "+image_path_in.substr(image_path_in.rfind('/')));
    Logger::write("Loading ",image_path_in);
    im=cv::imread(image_path_in,cv::IMREAD_COLOR);
    if(im.total()!=0)
    {
        if (convertFromMat(image_path_out.c_str(),im))
            finished(true);
        else
            finished(false);
    }
    else
        finished(false);
}




bool OzfConverter::convertFromMat(std::string destfile, cv::Mat& img)
{
    fout.open(destfile,std::ios_base::binary);
    if(!fout.is_open())
        return false;
    image_src.release();
    image_src.create(img.rows,img.cols,CV_8UC3);
    image_src=img;
    img.release();
    width=image_src.cols;
    height=image_src.rows;
    hdr1 = {0x7778, 0,64,1,0x436};
    hdr2 = {40,width,height,1,8,0,width*height,0,0,0x100,0x100};
    zhdr.zoom_level_count=0;
    separator=0x77777777;

    // level zoom, percent
    std::vector<float> zlevel{100.0,25.0,10.0,5.0,2.5,1.0};
    int z_idx=0;

    // add zoom levels while zoomed sided
    for(auto z : zlevel)
    {
        auto zw=z*width*0.01f;
        auto zh=z*height*0.01f;
        if(zw>=64 && zh>=64)
        {
            zhdr.zoom_level[z_idx++]=z;
            ++zhdr.zoom_level_count;
        }
        else
            break;
    }

    // add additional requred by format zoom levels
    zhdr.zoom_level[z_idx++]=130.0f*100/std::max(width,height);
    zhdr.zoom_level[z_idx]=300.0f*100/std::max(width,height);

    fout.write(reinterpret_cast<char*>(&hdr1),sizeof(Header1));
    fout.write(reinterpret_cast<char*>(&hdr2),sizeof(Header2));
    fout.write(reinterpret_cast<char*>(&separator),4);
    fout.write(reinterpret_cast<char*>(&zhdr),2+4*(zhdr.zoom_level_count+2));

    auto pal_gen = std::make_unique<PaletteGenerator>();
    PaletteApplier pal_apl;
    Logger::write("loaded");

    report(10,"Scanning colors");
    pal_gen->scanImage((char*)image_src.ptr(),height*width);
    Logger::write("scanned");

    if(abort_request)
    {
        image_src.release(); // TODO обернуть в деструктор
        return false;
    }

    report(20,"Calculating palette");
    pal_gen->popCountMethod(254,10);
    Logger::write("popcounted");

    palette_100=pal_gen->paletteCopy();
    palette_100.push_back(0);
    pal_apl.correlatePalette(pal_gen->paletteRef(),lut_100.get());

    Logger::write("correlated");

    if(abort_request)
    {
        image_src.release(); // TODO обернуть в деструктор
        return false;
    }

    pal_gen->popCountMethod(82,30);
    palette_scaled=pal_gen->paletteCopy();
    palette_scaled.push_back(0);
    pal_apl.correlatePalette(pal_gen->paletteRef(),lut_scaled.get());
    delete(pal_gen.release());
    Logger::write("second palette done");

    for(int z=0;z<(zhdr.zoom_level_count+2);z++)
    {
        if(!writeZoomLevel(z))
        {
            image_src.release(); // TODO обернуть в деструктор
            return false;
        }
    }
    uint32_t ppinfo = fout.tellp();
    fout.write(reinterpret_cast<char*>(pinfo),4*(zhdr.zoom_level_count+2));
    fout.write(reinterpret_cast<char*>(&ppinfo),4);
    fout.flush();
    fout.close();

    image_src.release();
}


bool OzfConverter::writeZoomLevel(int zoom)
{
    using namespace cv;
    infohdr.width=zoom ? static_cast<uint32_t>(hdr2.image_width*zhdr.zoom_level[zoom]*0.01) :
                         static_cast<uint32_t>(hdr2.image_width);
    infohdr.height=zoom ? static_cast<uint32_t>(hdr2.image_height*zhdr.zoom_level[zoom]*0.01) :
                          static_cast<uint32_t>(hdr2.image_height);
    infohdr.tiles_x = (infohdr.width&0x3f) ? static_cast<uint16_t>(infohdr.width/64+1) :
                                             static_cast<uint16_t>(infohdr.width/64);
    infohdr.tiles_y = (infohdr.height&0x3f) ? static_cast<uint16_t>(infohdr.height/64+1) :
                                              static_cast<uint16_t>(infohdr.height/64);
    report(30+zoom*10,"Writing zoom level " + std::to_string(zoom));
    Mat scaled;

    // начиная со 1-го zoom уровня, масштабируем не само изображение, а его 1/2 размера
    // копию, для ускорения.
    if(zoom==0)
    {
        scaled=image_src;
        Mat image50;
        resize(image_src,image50,Size(width/2, height/2));
        image_src=image50;
        image50.release();
    }
    else {
        scaled.create(infohdr.height, infohdr.width, CV_8UC3);
        resize(image_src, scaled, Size(infohdr.width, infohdr.height));
    }
    Mat indexed;
    indexed.create(infohdr.tiles_y*64, infohdr.tiles_x*64,CV_8U);
    memset(indexed.ptr(),zoom ? 0x53 : 0xff, indexed.total());
    PaletteApplier pal;
    pal.applyPaletteIndex(reinterpret_cast<char*>(indexed.ptr()),
                          reinterpret_cast<char*>(scaled.ptr()),
                          indexed.cols,
                          infohdr.width,
                          infohdr.height,
                          zoom ? lut_scaled.get() : lut_100.get());


    Logger::write("Palette applied");

    fout.write(reinterpret_cast<char*>(&separator),4);


    unsigned hw_c = std::thread::hardware_concurrency();
    if(infohdr.tiles_x*infohdr.tiles_y < 50)
        hw_c=1;
    std::vector<std::future<TilesVector>> ztask_res;
    std::vector<TilesVector> z_res;
    std::vector<char*> ztile_storage;

    auto tile_height_per_task = std::max(unsigned(1),infohdr.tiles_y/hw_c);
    unsigned tile_height_total = infohdr.tiles_y;
    unsigned start_tile_y=0;

    while(tile_height_total)
    {
        // little oversized
        ztile_storage.push_back((char*)operator new(indexed.total()*10/hw_c/9));
        auto task_height = std::min(tile_height_total, tile_height_per_task);


        ztask_res.emplace_back(std::async([this](
                                          //OZF2Image* op,
                                              char* dst,
                                              char* src,
                                              unsigned start,
                                              unsigned stop,
                                              unsigned w)->TilesVector
        {
            return this->compressImageChunk(dst,src,start,stop,w);
        }, ztile_storage.back(),
        (char*)indexed.ptr(),
        start_tile_y*64,
        (start_tile_y+task_height)*64,
        indexed.cols));
        tile_height_total-=task_height;
        start_tile_y+=task_height;
    }

    try
    {
        for(auto& zr : ztask_res)
        {
            z_res.push_back(zr.get());
        }
    }

    catch (std::exception& e){
        Logger::write("Error converting OZF ",ztile_storage.size());
        for(auto p : ztile_storage)
            operator delete(p);
        return false;
    }

    tile_off.clear();
    uint32_t file_offset = fout.tellp();
    // scan results of every async
    for(unsigned i=0; i<z_res.size();i++)
    {
        uint32_t task_compressed_size=0;
        for(uint32_t t_size : z_res[i])
        {
            tile_off.push_back(file_offset);
            file_offset += t_size;
            task_compressed_size += t_size;
        }
        fout.write(ztile_storage[i],task_compressed_size);
        operator delete(ztile_storage[i]);
    }



    pinfo[zoom]=fout.tellp();
    fout.write(reinterpret_cast<char*>(&infohdr),sizeof(Info));
    uint32_t raw_palette[256];
    std::fill_n(raw_palette,256,0);

    if(zoom)
        std::transform(palette_scaled.begin(),palette_scaled.end(),raw_palette,
                       [](const uint32_t val)->uint32_t
        { return ColorTransform::conv18_24(val); } );
    else
        std::transform(palette_100.begin(),palette_100.end(),raw_palette,
                       [](const uint32_t val)->uint32_t
        { return ColorTransform::conv18_24(val); } );
    raw_palette[zoom ? 0x53 : 0xff] = 0xffffff;
    /*for(auto i=0;i<256;i++){
        uint32_t& p = raw_palette[i];
        p = ((p&0xff) << 16) | ((p&0xff0000) >> 16) | (p&0xff00);}*/

    fout.write(reinterpret_cast<char*>(raw_palette),1024);
    fout.write(reinterpret_cast<char*>(tile_off.data()),
               tile_off.size()*4);
    fout.write(reinterpret_cast<char*>(pinfo+zoom),4);
    Logger::write ("Wrote zoom ", zoom);
    return true;
}


OzfConverter::TilesVector OzfConverter::compressImageChunk(
        char* dst,
        char* src,
        unsigned  start_line,
        unsigned end_line,
        unsigned width)
{
    TilesVector tile_size;
    tile_size.reserve(32768);
    z_stream zstr;

    alignas(16) char tile[4096],ztile[4096+2048];

    zstr.next_in=reinterpret_cast<Bytef*>(tile);
    zstr.zfree = Z_NULL;
    zstr.zalloc = Z_NULL;
    zstr.opaque = Z_NULL;
    if (deflateInit2(&zstr,
                     7,
                     Z_DEFLATED,
                     15,
                     8,
                     Z_DEFAULT_STRATEGY))
        throw(std::runtime_error("deflateInit error"));

    int res;
    for(auto h=start_line;h<end_line;h+=64)
    {
        for(unsigned  w=0; w<width; w+=64)
        {
            for(auto y=0;y<64;y++)
                memcpy(tile+y*64,src+(63-y+h)*width+w,64);

            /*for(auto xb=0;xb<64*64;xb+=64)
            {
                for(auto yb=0;yb<64;yb++)
                    memcpy(tile+yb*64,buffer+yb*64*64+xb,64);
*/
            zstr.next_in=reinterpret_cast<unsigned char*>(tile);
            zstr.avail_in=4096;
            res=deflateReset(&zstr);
            if(res!=Z_OK)
            {
                deflateEnd(&zstr);
                throw(std::runtime_error("deflateReset error"));
            }
            zstr.next_out=reinterpret_cast<unsigned char*>(ztile);
            zstr.avail_out=sizeof(ztile);
            res = deflate(&zstr,Z_SYNC_FLUSH);
            if(res<0)
                throw(std::runtime_error("deflate error"));
            if(abort_request)
            {
                // надо подумать. реорганизоватьб, заодно сделать счетчик нормальный
                return TilesVector();
            }
            tile_size.push_back(zstr.total_out);
            memcpy(dst, ztile, zstr.total_out);
            dst += zstr.total_out;

            //   }
        }
    }
    deflateEnd(&zstr);
    return tile_size;
}

