﻿#ifndef PALETTEGENERATOR_H
#define PALETTEGENERATOR_H

#include <stdint.h>
#include <vector>

class PaletteGenerator
{
    struct Node
    {
        Node() : r(0),g(0),b(0),pop(0),mask(0) {}
        uint32_t r,g,b;
        uint32_t pop;
        uint32_t mask;
#ifdef DEBUG
        uint32_t pad[3]{0,0,0};
#endif
        bool operator <(const Node& rhs)
        {
            return this->pop < rhs.pop;
        }


    };

    struct RemovalCandidate{
        uint32_t idx;
        unsigned layer;
    };

public:
    PaletteGenerator()
    {
        l6_sv.reserve(lay_size[6]);
        l5_sv.reserve(lay_size[5]);
        palette.reserve(256);
    }

    // size in pixels
    void scanImage(char* img,size_t size);

    // return first num_colors most popular colors with required minimum
    // squared euclidean distance beetwen them
    void popCountMethod(unsigned num_colors, int distance);
    void octMethod();
    std::vector<uint32_t> paletteCopy() {return palette;}
    std::vector<uint32_t>& paletteRef() {return palette;}
private:
    static uint32_t lay_size[7];
    static unsigned bitpop[16];

    Node* node[6] {&l0,l1,l2,l3,l4,l5};
    alignas(4) Node l0;
    Node l1[8];
    Node l2[64];
    Node l3[512];
    Node l4[4096];
    alignas(64) Node l5[32768];
    alignas(64) uint32_t l6[32768*8];

    uint32_t total_leafs{0};

    struct L6ItemView
    {
        L6ItemView(uint32_t c, uint32_t p) : col(c), pop(p){}
        uint32_t col;
        uint32_t pop;
        bool operator <(const L6ItemView& rhs) {return this->pop > rhs.pop;}
    };
    // sorted vector of 18-bit colors
    std::vector<L6ItemView> l6_sv;
    std::vector<L6ItemView> l5_sv;

    std::vector<uint32_t> palette;

    // populate tree levels 0-5 from layer6
    void populateTree();
    // get l0-l5 node index
    uint32_t color2Node(unsigned layer,uint32_t color);
    // get l6 index
    uint32_t color2Node(uint8_t *c);
    uint32_t maskFromColor(unsigned layer, uint32_t color); 
    void preparePalette();
    RemovalCandidate findMinPopItem();
    void reduce(RemovalCandidate rc);
//#ifdef DEBUG
//#endif
    // give base address of lower layer l(n)->l(n+1)
    uint32_t lowerLayer(unsigned lay, uint32_t c, unsigned child);
};





class PaletteApplier
{
public:
    PaletteApplier();
    void correlatePalette(const std::vector<uint32_t>& pal,uint8_t* lut);
    void correlatePalette(const std::vector<uint32_t>& pal,uint32_t* lut);

    ///@brief Quantinize RGB
    ///@param size [in] RGB24 data(r8 g8 b8 r8 g8 b8) pixel count
    void applyPaletteRGB(uint8_t* rgb24,size_t size, uint32_t* lut);

    ///@brief Transform RGB to Index, overwriting first 1/3 of data
    void applyPaletteIndex(char* dst,
                           char* src,
                           unsigned width_dst,
                           unsigned width_src,
                           unsigned height_src,
                           uint8_t * const lut);
private:
    static constexpr uint32_t lut_size=64*64*64;
};


#endif // PALETTEGENERATOR_H
