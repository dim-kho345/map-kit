#ifndef COLORTRANSFORM_H
#define COLORTRANSFORM_H

#include <stdint.h>

class ColorTransform
{
public:
    enum Component {Red=0,Green,Blue};
    static uint32_t conv18_24(uint32_t col);
    static uint32_t conv24_18(uint32_t col);

    template<class T>
    static T redFrom18(uint32_t col)
    {
        return static_cast<T> ((col>>12) & 0x3f);
    }

    template<class T>
    static T greenFrom18(uint32_t col)
    {
        return static_cast<T> ((col>>6) & 0x3f);
    }

    template<class T>
    static T blueFrom18(uint32_t col)
    {
        return static_cast<T> ((col>>0) & 0x3f);
    }


    static int_fast32_t sq_distance(uint32_t c1, uint32_t c2);

};

#endif // COLORTRANSFORM_H
