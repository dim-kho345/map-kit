#include "color_transform.h"


uint32_t ColorTransform::conv18_24(uint32_t col)
{
    return ((col & 0x3f000)  << 6) |
           ((col & 0xfc0)    << 4) |
            (col & 0x3f)     << 2;
}

uint32_t ColorTransform::conv24_18(uint32_t col)
{
    return ((col & 0xfc0000) >> 6) |
           ((col & 0xfc00)   >> 4) |
            (col & 0xfc)     >> 2;
}


int_fast32_t ColorTransform::sq_distance(uint32_t col1, uint32_t col2)
{
    int_fast32_t c1[3],c2[3];
    c1[0]=ColorTransform::redFrom18<int_fast32_t>(col1);
    c1[1]=ColorTransform::greenFrom18<int_fast32_t>(col1);
    c1[2]=ColorTransform::blueFrom18<int_fast32_t>(col1);
    c2[0]=ColorTransform::redFrom18<int_fast32_t>(col2);
    c2[1]=ColorTransform::greenFrom18<int_fast32_t>(col2);
    c2[2]=ColorTransform::blueFrom18<int_fast32_t>(col2);
    /*auto y1=c1[0]*76+c1[1]*150+c1[2]*29;
    auto y2=c2[0]*76+c2[1]*150+c2[2]*29;
    auto y_med=(y1+y2)>>1;
    y_med+=(264*64-y_med)/256;*/

    auto dr = c1[0]-c2[0];
    auto dg = c1[1]-c2[1];
    auto db = c1[2]-c2[2];
    return (dr*dr + dg*dg + db*db);//*y_med;
}
