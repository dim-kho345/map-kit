﻿#include <algorithm>
#include <future>
#include <cstring>
#include "color_transform.h"
#include "palette.h"


void PaletteGenerator::populateTree()
{

    for(uint32_t i=0;i<lay_size[6];i++)
    {
        uint32_t r,g,b,cnt;
        // привести цвета к 18-битам
        r=l6[i]*(i>>12);
        g=l6[i]*((i>>6) & 0x3f);
        b=l6[i]*(i&0x3f);
        cnt=l6[i];
        if(cnt) ++total_leafs;

        // населить узлы дерева
        for(unsigned lay=0;lay<6;lay++)
        {
            uint32_t idx = color2Node(lay,i);
            auto pnode=node[lay]+idx;
            pnode->pop+=cnt;
            pnode->r+=r;
            pnode->g+=g;
            pnode->b+=b;

            auto mask = maskFromColor(lay, i);
            if(cnt)
            {
                pnode->mask |= mask;
            }
        }
    }
}

void PaletteGenerator::scanImage(char* img,size_t size)
{
    memset(l6,0,sizeof(l6));
    while(size--)
    {
        ++l6[color2Node(reinterpret_cast<uint8_t*>(img))];
        img+=3;
    }
    //populateTree();
    preparePalette();
}

uint32_t PaletteGenerator::color2Node(unsigned layer,uint32_t color)
{
    unsigned rlayer = 6-layer;
    uint32_t mask=0x3f-(1<<rlayer)+1;
    color = ((color&(mask<<12)) >> (3*rlayer)) |
            ((color&(mask<<6))  >> (2*rlayer)) |
            ((color&(mask<<0))  >> (rlayer));
    return color;
}


uint32_t PaletteGenerator::color2Node(uint8_t* c)
{
    // truncate to 6 bit and shift to 12,6,0
    return ((uint32_t)(c[2]&0xfc) << 10) |
           ((uint32_t)(c[1]&0xfc) << 4)  |
           ((uint32_t)(c[0]&0xfc) >> 2);
}

uint32_t PaletteGenerator::maskFromColor(unsigned layer, uint32_t color)
{
    unsigned rlay=5-layer;
    uint32_t shift = (1<<0 | 1<<6 | 1<<12) << rlay;
    shift = (color&shift) >> rlay;
    shift = ( shift      &0x1)  |
            ((shift>>5)  &0x2)  |
            ((shift>>10) &0x4);
    return (1<<shift);
}


void PaletteGenerator::preparePalette()
{
    for (uint_fast32_t i=0;i<lay_size[6];i++)
    {
        if(l6[i])
            l6_sv.emplace_back(i,l6[i]);
    }
    std::sort(l6_sv.begin(), l6_sv.end());
}

void PaletteGenerator::popCountMethod(unsigned num_colors, int distance)
{
    size_t l6_idx=0;
    palette.clear();
    for(uint_fast32_t pal_size=0;pal_size<num_colors;)
    {
        bool accept{true};

        auto size = pal_size>1 ? pal_size : 0;

        // maintain the euclidean distance beetwen colors
        for(size_t j=0;j<size;j++)
        {
            if(ColorTransform::sq_distance(l6_sv[l6_idx].col,palette[j]) < distance)
            {
                accept=false;
                break;
            }
        }

        if(accept)
        {
            palette.emplace_back(l6_sv[l6_idx].col);

            //std::cout<< i << ' ' << std::hex << palette[i] << std::dec << ' ' << l6_idx << ' ' << l6_sv[l6_idx].pop << std::endl;
            ++pal_size;
        }
        if(++l6_idx > (l6_sv.size()-1))
        {
#ifdef DEBUG
            std::cout << "Candidates run out " << i << std::endl;
#endif
            break;
        }
    }
}

PaletteGenerator::RemovalCandidate PaletteGenerator::findMinPopItem()
{
    RemovalCandidate rc_min;
    uint_fast32_t pop_min=static_cast<uint32_t>(1)<<31;


    /*for(uint_fast32_t i=0;i<lay_size[6];i++)
     // walk around l6 since it's leaf format differs
        if (l6[i] < pop_min)
        {
            rc_min={i,6};
            pop_min=l6[i];
        }*/

    for(unsigned lay=0;lay<6;lay++)
    {
        for(uint_fast32_t i=0;i<lay_size[lay];i++)
        {
            Node* nd = node[lay]+i;
            if((nd->pop <= pop_min) && nd->mask)
            {
                rc_min={i,lay};
                pop_min=nd->pop;
            }
        }
    }
    return  rc_min;
}

void PaletteGenerator::reduce(RemovalCandidate rc)
{
    Node* nd = node[rc.layer]+rc.idx;

    //вычищаем все chidren и добавляем вместо этого корень в счетчик
    total_leafs -= bitpop[nd->mask&0xf];
    total_leafs -= bitpop[nd->mask>>4];
    ++total_leafs;
    //logger << "reduced " << rc.layer << ':' << rc.idx << ' ' << bitpop[nd->mask&0xf] + bitpop[nd->mask>>4] << '\n';
    nd->mask=0;
    // calculate all children and clear their pop

    for(unsigned child=0; child<8; child++)
    {
        if(rc.layer<5)
        {
            auto tmp = node[rc.layer+1]+lowerLayer(rc.layer,rc.idx,child);
            //logger << "child "<< rc.layer << ':' << rc.idx << ' ' << tmp->pop << '\n';
            tmp->pop=0;
        }
        else
        {
            auto idx = lowerLayer(rc.layer,rc.idx,child);
            //logger << "child "<< rc.layer << ':' << rc.idx << ' ' << l6[idx] << '\n';
            l6[idx]=0;

        }
    }
}



uint32_t PaletteGenerator::lowerLayer(unsigned lay,uint32_t c,unsigned child)
{
    uint32_t mask = (1<<(lay))-1;
    c = (c&(mask<<0))       <<1 |
        (c&(mask<<lay))    <<2 |
        (c&(mask<<lay*2))  <<3;
    //++lay;
    c |= (child&0x1)<<0 |
         (child&0x2)<<lay |
         (child&0x4)<<(lay*2);
    return c;
}


void PaletteGenerator::octMethod()
{
    using namespace  std;
    // next leaf for removal

    while(total_leafs>254)
    {
        RemovalCandidate rc;
        rc = findMinPopItem();
        reduce(rc);
    }
    for(unsigned layer=0;layer<6;layer++)
    {
        Node* nd;
        for(uint_fast16_t i=0;i<lay_size[layer];i++)
        {
            nd=node[layer]+i;
            // if it is leaf and populated with rgb data
            if (nd->pop && !nd->mask)
            {
                uint32_t r,g,b;
                r=nd->r/nd->pop;
                g=nd->g/nd->pop;
                b=nd->b/nd->pop;
                palette.emplace_back(r<<12 | g<<6 | b);
            }
        }
    }
    for(size_t i=0;i<lay_size[6];i++)
        if(l6[i])
            palette.push_back(l6[i]);
    //copy_if(l6,l6+lay_size[6],back_inserter(palette),[](uint32_t col)->bool
                                                   // {return col;});
}

uint32_t PaletteGenerator::lay_size[7] = {1,8,64,512,4096,32768,32768*8};
unsigned PaletteGenerator::bitpop[16] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4};










PaletteApplier::PaletteApplier()
{

}

void PaletteApplier::correlatePalette(const std::vector<uint32_t> &pal,
                                      uint8_t* lut)
{
    auto pal_size = pal.size();
    auto func = [&](unsigned lut_start_idx, unsigned lut_end_idx)
    {
    for(uint32_t i=lut_start_idx;i<lut_end_idx;i++)
    {
        int_fast32_t min_dist=1<<30;
        uint8_t min_index=0;
        for(uint_fast32_t pal_idx=0;pal_idx<pal_size;pal_idx++)
        {
            int_fast32_t dist;
            dist=ColorTransform::sq_distance(i,pal[pal_idx]);
            if(dist <= min_dist)
            {
                min_dist=dist;
                min_index=pal_idx;
            }
        }
        lut[i]=min_index;
    }
    };

    using namespace std;
    vector<future<void>> res;

    auto color_space = lut_size;
    unsigned start_color=0;
    unsigned colors_per_thread = lut_size/thread::hardware_concurrency();


    while(color_space)
    {
        auto curr_col_space = min(colors_per_thread, color_space);
        res.push_back(async(func, start_color, start_color + curr_col_space));
        color_space -= curr_col_space;
        start_color += curr_col_space;
    }

//    auto f1 = std::async(func,0,lut_size/2);
//    auto f2 = std::async(func,lut_size-lut_size/2,lut_size);

//    f1.get();
//    f2.get();
    for(auto& r : res)
        r.get();

}

void PaletteApplier::correlatePalette(const std::vector<uint32_t> &pal,
                                      uint32_t* lut)
{
    auto pal_size = pal.size();
    for(uint32_t i=0;i<lut_size;i++)
    {
        int_fast32_t min_dist=1<<30;
        uint32_t min_color=0;
        for(uint_fast32_t p=0;p<pal_size;p++)
        {
            int_fast32_t dist;
            dist=ColorTransform::sq_distance(i,pal[p]);
            if(dist <= min_dist)
            {
                min_dist=dist;
                min_color=pal[p];

            }
        }
        lut[i]=ColorTransform::conv18_24(min_color);
    }
}


void PaletteApplier::applyPaletteRGB(uint8_t *rgb24, size_t size, uint32_t *lut)
{
    auto func = [lut](uint8_t *data, size_t size)
    {
        while(size--)
        {
            uint32_t pixel=static_cast<uint32_t>(data[0])<<16;
            pixel |= static_cast<uint32_t>(data[1])<<8;
            pixel |= static_cast<uint32_t>(data[2]);
            pixel = lut[ColorTransform::conv24_18(pixel)];
            data[0]=(pixel>>16) & 0xff;
            data[1]=(pixel>>8) & 0xff;
            data[2]= pixel & 0xff;
            data+=3;
        }
    };
    auto f1 = std::thread(func,rgb24, size/2);
    auto f2 = std::thread(func,rgb24+size/2*3, size-size/2);
    f1.join();
    f2.join();
}

void PaletteApplier::applyPaletteIndex(char* dst,
                                       char* src,
                                       unsigned width_dst,
                                       unsigned width_src,
                                       unsigned height_src,
                                       uint8_t *const lut)
{
    auto func = [=](uint8_t * dst, uint8_t* src, unsigned lines)
    {
        while(lines--)
        {
            for(unsigned int i=0; i<width_src; i++)
            {
            uint32_t pixel=static_cast<uint32_t>(src[2])<<16;
            pixel |= static_cast<uint32_t>(src[1])<<8;
            pixel |= static_cast<uint32_t>(src[0]);
            *dst++ = lut[ColorTransform::conv24_18(pixel)];
            src+=3;
            }
            dst += width_dst - width_src;
        }
    };
    auto f1 = std::async(func,
                          reinterpret_cast<uint8_t*>(dst),
                          reinterpret_cast<uint8_t*>(src),
                          height_src/2);
    auto f2 = std::async(func,
                          reinterpret_cast<uint8_t*>(dst+height_src/2*width_dst),
                          reinterpret_cast<uint8_t*>(src+height_src/2*width_src*3)  ,
                          height_src-height_src/2);
    f1.get();
    f2.get();
};
