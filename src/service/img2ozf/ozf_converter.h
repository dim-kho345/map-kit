#ifndef OZFCONVERTER_H
#define OZFCONVERTER_H

#include <thread>
#include <atomic>
#include "service.h"
#include "gui/gui.h"
#include <opencv2/opencv.hpp>
#include <zlib.h>
#include <fstream>

class OzfConverter : public ServiceImg2Ozf
{
public:
    OzfConverter(GUI* p_gui, Resources res=Memory);
    void start() override;
    void abort() override;
private:
    void job();
    std::atomic<int> abort_request{false};
    std::thread t;

    bool convertFromMat(std::string destfile, cv::Mat &img);
    static constexpr auto max_zoom_levels=10;
    static constexpr auto lut_size=64*64*64;
    std::ofstream fout;

    #pragma pack(2)
    struct Header1
    {
        uint16_t magic;          // set it to 0x7780 for ozfx3 and 0x7778 for ozf2
        uint32_t locked;          // if set to 1, than ozi refuses to export the image (doesn't seem to work for ozfx3 files though); just set to 0
        uint16_t tile_width;     // set always to 64
        uint16_t version;        // set always to 1
        uint32_t old_header_size; // set always to 0x436; this has something to do with files having magic 0x7779                 // (haven't seen any of those, they are probably rare, but ozi has code to open them)
    };

    struct Header2
    {
        uint32_t header_size;		// always 40
        uint32_t image_width;		// width of image in pixels
        uint32_t image_height;		// height of image in pixels
        uint16_t depth;				// set to 1
        uint16_t bpp;				// set to 8
        uint32_t reserved1;			// set to 0
        uint32_t memory_size;		// height * width; probably not used and contains junk if it exceeds 0xFFFFFFFF (which is perfectly ok)
        uint32_t reserved2;			// set to 0
        uint32_t reserved3;			// set to 0
        uint32_t unk2;              // set to 0x100
        uint32_t unk3;				// set to 0x100
    };
    struct ZoomHeader
    {
        uint16_t zoom_level_count;
        float zoom_level[max_zoom_levels];
    };
    struct Info
    {
        uint32_t width;
        uint32_t height;
        uint16_t tiles_x;
        uint16_t tiles_y;
    };
    using TilesVector  = std::vector<uint32_t>;

    #pragma pack ()
    uint32_t separator;
    Header1 hdr1;
    Header2 hdr2;
    ZoomHeader zhdr;
    Info infohdr;
    TilesVector tile_off;
    std::string file;


    uint32_t pinfo[max_zoom_levels];

    char output[64*64+256];
    char rgb[64*64*3];

    std::vector<uint32_t> palette_100;
    std::vector<uint32_t> palette_scaled;
    std::unique_ptr<uint8_t[]> lut_100;
    std::unique_ptr<uint8_t[]> lut_scaled;
    std::unique_ptr<uint32_t[]> lut_rgb;

    z_stream zstr;
    uint32_t width,height;
    cv::Mat image_src;
    bool writeZoomLevel(int zoom);
    TilesVector compressImageChunk(char* dst,
                            char* src,
                            unsigned  start_line,
                            unsigned end_line,
                            unsigned width);
};

#endif // OZFCONVERTER_H



