#include <thread>
#include "opencv2/opencv.hpp"
#include "stitcher.h"
#include "settings.h"
#include "log.h"

Stitcher::Stitcher(GUI* p_gui, Resources res) :
    ServiceStitcher(p_gui, res)
{
}


void Stitcher::start()
{
    auto crs = CRS::crsFromString(map_source->crs);
    treg.nw = Tile(region.nw, zoom, crs);
    treg.se = Tile(region.se, zoom, crs);

    for(auto x=treg.nw.x; x<=treg.se.x;x++)
        for(auto y=treg.nw.y; y<=treg.se.y;y++)
        {
            task.emplace_back(x,y);
        }
     Logger::printf("Stitch: %i tiles to go", task.size());

    // это все после проверки
       t=std::thread(&Stitcher::job,this);
        t.detach();

}

void Stitcher::job()
{
    struct TileCleaner
    {
        using TilesVector = decltype (task);
        TileCleaner(TilesVector& tiles) :
            tiles(tiles) {}
        ~TileCleaner()
            {tiles.clear();}
        TilesVector& tiles;
    };

    TileCleaner tc(task);
    try {
        Logger::write("Started stitching thread");
        using namespace cv;
        using namespace std;
        unsigned w,h;
        constexpr unsigned gui_skip_period = 5;
        unsigned gui_cnt = 0;
        Mat mtile,im,roi;
        string base_path(settings["SASPath"] + '/');
        Tile t;
        auto base_x=treg.nw.x;
        auto base_y=treg.nw.y;
        w=(treg.se.x-treg.nw.x+1)*TILE_SIDE;
        h=(treg.se.y-treg.nw.y+1)*TILE_SIDE;

        gui->onProgress(function(), model_ctx, queue_ctx, 0, "Stitching");

        im.create(h,w,CV_8UC3);

        if(im.total()!=w*h)
        {
            return;
        }

        for(auto& t : task)
        {
            auto path = base_path + map_source->tilePath(t.x, t.y, zoom);
            mtile=imread(path, IMREAD_COLOR);
            if(mtile.empty())
                continue;
            Mat win(im,  { static_cast<int>((t.x-base_x)*TILE_SIDE),
                           static_cast<int>((t.y-base_y)*TILE_SIDE),
                           TILE_SIDE,TILE_SIDE });
            mtile.copyTo(win);
            mtile.release();
            if(std::atomic_load(&abort_request)==true)
            {
                Logger::write("Abort requested");
                im.release();
                finished(false);
                return;
            }

            if(++gui_cnt % gui_skip_period == 0)
            {
                gui->onProgress(function(), model_ctx, queue_ctx, gui_cnt*70/task.size(), "Stitching");
            }
        }
        w=treg.imageWidth();
        h=treg.imageHeight();
        Logger::write("Finished stitching w/h=",w,'/',h);
        Mat saved(im,{ static_cast<int>(treg.nw.off_x),
                       static_cast<int>(treg.nw.off_y),
                       static_cast<int>(w),
                       static_cast<int>(h)});

        /* для наложения слоев WGS84 меркатор скейлим в WebMercator
        if(map_source->info().crs=="EPSG3395")
        {
            Mat resized;
            emit progress(tr("Scaling WebMercator to WGS84"));
            // старые угловые тайлы больше не понадобятся
            treg.t1=geo_math->transEN2Tile(treg.p1,Datum::EPSG3785,treg.zoom);
            treg.t2=geo_math->transEN2Tile(treg.p2,Datum::EPSG3785,treg.zoom);
            w=treg.imageWidth();
            h=treg.imageHeight();
            resize(saved,resized,Size(w,h),0,0,INTER_AREA);
            treg.width=w;
            treg.height=h;
            im.release();
            saved.release();
            resized.copyTo(saved);
            resized.release();
        }*/
        //emit progress("Saving screen version");
        gui->onProgress(function(), model_ctx, queue_ctx, 70, "Generating screen version");
        Mat scr_version;
        double scale=icon_side_size/(std::max<double>(w,h));
        resize(saved,scr_version,Size(0,0),scale,scale,INTER_AREA);
        icon_width = scr_version.cols;
        icon_height = scr_version.rows;
        if(!imwrite(icon_path_out,scr_version))
        {
            scr_version.release();
            im.release();
            finishedVerbose(false, GUI::Warning, "Не удалось сохранить " + icon_path_out);
            return;

        }
        im.release();
        scr_version.release();
        Logger::write("Saved screen version");

        if(std::atomic_load(&abort_request)==true)
        {
            Logger::write("Abort request");
            saved.release();
            finished(false);
            return;
        }

        gui->onProgress(function(), model_ctx, queue_ctx, 80, "Enhancing image for OZF");
        /*if(map_source->info().is_topo==false)
        {
            Mat ch[3];
            cvtColor(saved,saved,COLOR_BGR2YCrCb);
            //emit progress(tr("Saving sattelite YCrCb"));
            //imwrite(image_path_out,saved);
            split(saved,ch);
            //emit progress(tr("Enhancing OZI sattelite"));
            // количество блоков не должно меняться от масштаба
            Size blocks((w/TILE_SIDE)>>(zoom-15),
                        (h/TILE_SIDE)>>(zoom-15));
            auto cl = createCLAHE(1.5,blocks);
            cl->apply(ch[0],ch[0]);
            merge(ch,3,saved);
            ch->release();
            cvtColor(saved,saved,COLOR_YCrCb2BGR);
        }*/
        if(std::atomic_load(&abort_request)==true)
        {
            saved.release();
            finished(false);
            return;
        }
        gui->onProgress(function(), model_ctx, queue_ctx, 90, "Saving stitched image");
        if(!imwrite(image_path_out,saved))
            finishedVerbose(false, GUI::Warning, "Не удалось сохранить " + image_path_out);
        img_width = saved.cols;
        img_height = saved.rows;
        saved.release();
    }
    catch (const cv::Exception& ex)
    {
        Logger::printf("Stitcher opencv exception %s: %s: %s", ex.msg, ex.func, ex.file);
        finishedVerbose(false, GUI::Critical, ex.msg);
    }
    catch (const std::exception& ex)
    {
        Logger::write("Stitcher std exception: ", ex.what());
        finishedVerbose(false, GUI::Critical, ex.what());
    }
    finished(true);
}


void Stitcher::abort()
{
    abort_request=true;
    gui->onProgress(function(), model_ctx, -1, -1, "Aborting...");
}
