#ifndef DOWNLOAD_H
#define DOWNLOAD_H
#include <deque>
#include <set>
#include <QObject>
#include <memory>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include "geo.h"
#include "service.h"

struct DTile : Tile
{
    DTile() = default;
    DTile(int src_x, int src_y, int zoom) :
        Tile(src_x,src_y), ttl(5), zoom(zoom) {}
    DTile(const Tile& rhs):
        Tile(rhs) {}
    int ttl;
    int zoom;
};

enum : int {
    NoError = 0,
    MissedTile,
    WriteTileFiled,
    TooMuchErrors,
    HostNotFound
};

class TileDownloader: public ServiceDownloadMap
{
 public:

    TileDownloader(GUI* p_gui) :
       ServiceDownloadMap(p_gui)  {
        if (net_mgr.use_count()==0)
            net_mgr.reset(new QNetworkAccessManager);
    }

    void start() override;
    void abort() override;

    static std::shared_ptr<QNetworkAccessManager> net_mgr;
    std::deque<DTile> queue;
    TileRegion tile_region;
    unsigned int total_tiles;
    unsigned n_missed_tiles{0};
    static constexpr int error_limit = 500;
    /// собираем некритичные ошибки
    std::set<int> error_code;
    /// критическая ошибка
    int fail_code{NoError};
    int error_count{0};
    bool abort_req{false};
    unsigned active_workers{0};
    class DownloadWorker* worker[6];
};


class DownloadWorker: public QObject
{
    Q_OBJECT
public:


    DownloadWorker(TileDownloader& mgr) :
        QObject(dynamic_cast<QObject*>(mgr.gui)),
        mgr(mgr){}
    void start() {reqReply();}

private slots:
    void error(QNetworkReply::NetworkError err);
    void reqReply();
private:

    TileDownloader& mgr;
    QNetworkReply* reply{nullptr};

    DTile tile;
    QString url;
    QString path;

};

#endif // DOWNLOAD_H
