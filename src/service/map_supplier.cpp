#include <QProcess>
#include <QDir>
#include <QStringList>
#include <QApplication>
#include <algorithm>
#include <sstream>
#include "service/map_supplier.h"
#include "service/map_specs.h"
#include "log.h"



MapSupplier::~MapSupplier()
{
    /*for(auto& p :source_executable)
    {
        p.second->close();
        p.second->waitForFinished();
    }*/
}

MapSupplier::MapSupplier()
{

    using namespace  std;
    QDir dir(QApplication::applicationDirPath());
    QStringList proc_to_run;
    auto files = dir.entryList();
    copy_if(files.cbegin(), files.cend(), std::back_inserter(proc_to_run), [](const QString& str)->bool {
        if(str.contains(QRegExp("rmaps\\d\\.exe")))
            return  true;
        return false;
    });

    char line[256];
    MapSourceIdent ident;

    for(auto& p : proc_to_run)
    {
        auto proc = shared_ptr<QProcess>(new QProcess(),[](QProcess* p){
                p->close();
                p->waitForFinished();
                delete p;

    });
        proc->setProgram(p);
        Logger::write("Launching map supplier ",p.toStdString());
        proc->setProcessChannelMode(QProcess::ProcessChannelMode::MergedChannels);
        proc->setReadChannel(QProcess::ProcessChannel::StandardOutput);
        proc->start();
        if(!proc->waitForStarted(3000))
            continue;
        proc->write("LIST\n");
        proc->waitForReadyRead(500);

        try
        {
            while(proc->canReadLine())
            {
                int num_zoom_levels;
                vector<int> zoom_level;

                proc->readLine(line,sizeof(line));

                line[sizeof(line)-1]=0; //подстрахуемся
                if(strlen(line)==sizeof(line)-1 || strlen(line)<3)
                    continue;

                istringstream ss(line);

                ss >> ident.name >> ident.type >> num_zoom_levels;
                if (ident.name.empty() || ident.type.empty() || num_zoom_levels<0 || num_zoom_levels>10)
                    Logger::printf("ERR remote maps %s %s", proc->program().toStdString(),line);
                for(auto i=0;i<num_zoom_levels;i++)
                {
                    unsigned zoom;
                    ss >> zoom;
                    zoom_level.push_back(zoom);
                }
                string crs, cache_name, content_type;
                ss >> crs >> cache_name >> content_type;

                source_executable.insert(make_pair(ident,proc));
                source.push_back(make_unique<MapSourceRemote>  (ident.name,
                                                                zoom_level,
                                                                ident.type!="SAT" ? true : false,
                                                                crs,
                                                                cache_name,
                                                                content_type,
                                                                source_executable.at(ident).get()));
            }
        }
        catch (ios_base::iostate e)
        {
            Logger::printf("Remote maps >> exception %s %s", proc->program().toStdString(),line);
        }

    }
}

std::vector<MapSource*> MapSupplier::availableMaps(){
    std::vector<MapSource*> res;
    for(auto& s : source)
        res.push_back(s.get());
    return res;
}
