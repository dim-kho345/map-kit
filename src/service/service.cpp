﻿#include "queue.h"


void ServiceBase::finished(bool complete)
{
    // some services run without queue due to their simplicity
    if(queue)
        reinterpret_cast<ServiceQueue*>(queue)->serviceFinished(function(), queue_ctx, complete);
}

void ServiceBase::finishedVerbose(bool complete, GUI::UserMessageType type, std::string message)
{
    gui->onUserMessage(type,message);
    finished(complete);
}

void ServiceBase::report(int progress, const std::string& messg)
{
    gui->onProgress(function(), model_ctx, queue_ctx, progress, messg);
}
