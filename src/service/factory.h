#ifndef SERVICEFACTORY_H
#define SERVICEFACTORY_H
#include <memory>
#include "service.h"


class ServiceFactory
{

    static std::unique_ptr<ServiceBase> getServiceImpl(ServiceFunction func,
                                                       GUI* gui);
public:
    template <class T>
    static std::unique_ptr<T> getService(ServiceFunction func, GUI* gui) {
        return std::unique_ptr<T>(static_cast<T*>(getServiceImpl(func, gui).release()));
    }
};

#endif // SERVICEFACTORY_H
