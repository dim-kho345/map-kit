#ifndef HLGREADER_H
#define HLGREADER_H
#include "geo.h"

class HLGReader
{
public:
    static constexpr bool ReadOnly{true};
    /// Parse HLG, extract region
    static GeoRegion region(const std::string& path);

    /// Parse HLG filename, extract location name
    static std::string location(std::string path);
};

#endif // HLGREADER_H
