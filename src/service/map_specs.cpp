#include <cstring>
#include <sstream>
#include "map_specs.h"
#include "settings.h"
#include "geo.h"


///@todo временно, доступ к тайлам будет осуществляться через отдельный класс TileStorage
std::string MapSourceRemote::tileUrl(int x, int y, int zoom) const
{
    std::ostringstream ss;
    ss << "TILE " << name << (is_topo? " TOPO " : " SAT ") << x << ' ' << y << ' ' << zoom << std::endl;
    map_proc->write(ss.str().c_str());
    map_proc->waitForReadyRead(300);
    std::string resp = map_proc->readAll().data();
    resp = resp.substr(0,resp.find_last_not_of("\xa\x10"));
    return resp;
}

std::string MapSourceRemote::tilePath(int x, int y, int zoom) const
{

    return cache_name + '/' + MapSource::tilePath(x, y, zoom, SASPlanet);
}




