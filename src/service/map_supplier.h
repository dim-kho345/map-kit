#ifndef MAP_SUPPLIER_H
#define MAP_SUPPLIER_H

#include <vector>
#include <memory>
#include <QProcess>
#include <unordered_map>
#include "map_source.h"

/// Map Source Idendifier, composed from name and type
struct MapSourceIdent
{
    std::string name;
    std::string type;
    bool operator==(const MapSourceIdent& rhs)const {
        return name==rhs.name && type==rhs.type;
    }
};

template<>
struct std::hash<MapSourceIdent>
{
    size_t operator() (const MapSourceIdent& id) const
    {
        return hash<string>().operator()(id.name + id.type);
    }
};


///Online tiled map sources supplier
class MapSupplier{
public:

    ~MapSupplier();

    /**
        @brief launch every executable named rmaps[0-9].exe, acquire map sources from it (LIST command),
        generate MapSourceRemote objects
    */
    MapSupplier();

    std::vector<MapSource*> availableMaps();
private:
    std::unordered_map<MapSourceIdent, std::shared_ptr<QProcess>> source_executable;
    std::vector<std::unique_ptr<MapSource>> source;
};



#endif //MAP_SUPPLIER_H
