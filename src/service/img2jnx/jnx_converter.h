#ifndef JNXCONVERTER_H
#define JNXCONVERTER_H

#include <thread>
#include <atomic>
#include <fstream>
#include <opencv2/opencv.hpp>

#include <gdal/gdal.h>
#include <gdal/gdal_priv.h>
#include "service.h"
#include "gui/gui.h"



class JnxConverter : public ServiceImg2Jnx
{
public:
    JnxConverter(GUI* p_gui, Resources res=Memory);
    ~JnxConverter();
    void start() override;
    void abort() override;
    bool abortRequested() {
        return abort_request;
    }
    using ServiceBase::report;
private:
    void job();
    std::atomic<int> abort_request{false};
    std::thread t;



    enum Error{
        AbortRequested=1,
        NoError=0,
        OutputFileError=-1,
        TooMuchTiles=-2,
        MapNameOversized=-3,
        InputFileError=-4
    };
    Error convertFromMap();
    Error setMapName(std::string name);
    void scanJNX(std::string in_file, std::string out_file);
    inline int32_t deg2int(double d) {
        return static_cast<int32_t>(round(d*k_deg2int));
    }
    std::string generateJnxId();
#pragma pack(1)
    struct MainHeader
    {
        uint32_t version{4};
        uint32_t dev_id{0};
        int32_t coord[4];
        uint32_t total_levels{1};
        uint32_t subscr_date{0};
        uint32_t subscr_id{0};
        uint32_t tile_coord_crc{0};
        uint32_t sig_ver{0};
        uint32_t sign_offset{0};
        uint32_t draw_order{30};
    };

    struct LevelHeader
    {
        uint32_t total_tiles;
        uint32_t tile_table_offset;
        uint32_t scale;
        uint32_t pad{0};
        char copyright_cstr{0};
    };

    struct MapDescription1
    {
        uint32_t version{9};
        char guid[37];
        char prod_cstr{0};
        char pad_cstr{0};
        uint16_t subcr_id{0};
        char name_cstr{0};
    };
    struct MapDescription2
    {
        uint32_t total_levels{1};
    };

    struct LevelDescription
    {
        char name_cstr{0};
        char descr_cstr{0};
        char copyright_cstr{0};
        uint32_t src_scale{1};
    };

    struct Tile
    {
        Tile() =default;
        Tile(std::pair<int32_t,int32_t> c1,
             std::pair<int32_t,int32_t> c2,
             uint32_t size) :
            size(size)
        {
            coord[0]=c1.first;
            coord[1]=c1.second;
            coord[2]=c2.first;
            coord[3]=c2.second;
        }
        int32_t coord[4];
        uint16_t width{TILE_SIDE};
        uint16_t height{TILE_SIDE};
        uint32_t size;
        uint32_t offset;
    };
    using TilesVector = std::vector<Tile>;
#pragma pack()

    TilesVector encodeTiles(char* tile_data, uint32_t start_y, uint32_t stop_y);
    Error writeHeader(uint32_t width,
                     uint32_t height,
                     double coord[4]);

    std::pair<int32_t,int32_t> tileCoord(uint32_t x, uint32_t y);

    int32_t lon1, lon2, lat1, lat2;
    double lon1d,lon2d,lat1d,lat2d;
    static constexpr double k_deg2int = 0x7fffffff/180.0;

    double pix_res;
    std::ofstream fout;
    uint32_t tiles_x,tiles_y;
    uint32_t total_tiles;
    uint32_t im_w, im_h;
    std::string map_name{"TestMap"};
    char* hdr_data;
    uint32_t tiles_table_offset;
    cv::Mat map_img;

};

#endif // JNXCONVERTER_H



