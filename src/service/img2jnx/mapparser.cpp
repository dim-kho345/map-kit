#include "mapparser.h"
#include <algorithm>
#include <sstream>

bool  MAPParser::openMAP(std::string filename)
{
    using namespace std;
    char line[256];
    char* opt[20];
    ifstream fin(filename);
    if(!fin.is_open())
        return false;
    fin.getline(line,256);
    for(int i=0;i<2;i++)
    {
    fin.getline(line,256);
   /* char* img_name = strrchr(line,'\\');
                    if (!img_name)
                        img_fname[i].assign(line);
                    else
                        img_fname[i].assign(++img_name);*/
    img_fname[i].assign(line);
    }

    while(fin)
    {
        fin.getline(line,256);
        remove(line,line+strlen(line),' ');
        unsigned idx=0, opt_idx=0;

        // split line into options delimited by comma
        // first option locates at start of line
        opt[opt_idx++]=line;
        while(line[idx] && idx<sizeof(line))
        {
            if(line[idx]==',')
            {
                line[idx]=0;
                opt[opt_idx++]=line+idx+1;
            }
            ++idx;
        }

        if(!strcmp(opt[0],"MMPLL"))
        {
            if(!strcmp(opt[1],"1"))
            {
                lon1 = stod(opt[2]);
                lat1 = stod(opt[3]);
            }
            if(!strcmp(opt[1],"3"))
            {
                lon2 = stod(opt[2]);
                lat2 = stod(opt[3]);
            }
        }
        if(!strcmp(opt[0],"MMPXY"))
        {
            if(!strcmp(opt[1],"3"))
            {
                width = stoul(opt[2]);
                height = stoul(opt[3]);
            }
        }
    }
    return true;

}
