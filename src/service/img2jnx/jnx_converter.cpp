#include <future>
#include <random>
#include <turbojpeg.h>
#include "log.h"
#include "jnx_converter.h"
#include "geowarp.h"


JnxConverter::JnxConverter(GUI* p_gui, Resources res) :
    ServiceImg2Jnx(p_gui, res),
    hdr_data(reinterpret_cast<char*>(operator new (2048)))
{
}

JnxConverter::~JnxConverter()
{
     delete hdr_data;
}

void  JnxConverter::start()
{
    t=std::thread(&JnxConverter::job,this);
    t.detach();
}

void  JnxConverter::abort()
{
    abort_request=true;
    gui->onProgress(function(), model_ctx, -1, -1, "Aborting...");
}



void JnxConverter::job()
{
    using namespace std;
    report(0, "Loading "+map_path_in.substr(map_path_in.rfind('/')));
    Logger::write("Started JNX task ",map_path_in);
    setMapName(location_name);
    try
    {
        auto res = convertFromMap();
        switch(res)
        {
        case AbortRequested:
            finishedVerbose(false, GUI::Warning, "JNX conversion aborted");
            break;
        case OutputFileError:
            finishedVerbose(false, GUI::Warning, "JNX conversion: output file error");
            break;
        case NoError:
            finished(true);
            break;
        default:
            finishedVerbose(false, GUI::Critical, "JNX conversion failed. Error "+std::to_string(res));
            break;
        }
    }
    catch(std::exception& ex)
    {
       finishedVerbose(false, GUI::Critical, std::string("JNX exception:\n")+ex.what());
    }
}


JnxConverter::Error JnxConverter::setMapName(std::string name)
{
    if(name.size()>100)
        return MapNameOversized;
    map_name=name;
    return NoError;
}

std::pair<int32_t, int32_t> JnxConverter::tileCoord(uint32_t x, uint32_t y)
{
    double lat = lat1d -  y * pix_res*TILE_SIDE;
    double lon = lon2d +  x * pix_res*TILE_SIDE;
    return std::make_pair(deg2int(lat),deg2int(lon));
}


JnxConverter::Error JnxConverter::convertFromMap()
{
    using namespace cv;
    TilesVector tile_hdr;
    tile_hdr.reserve(4096);
    GDALRegister_MAP();
    GDALRegister_MEM();
    GDALRegister_BMP();
    GDALRegister_GTiff();
    Logger::write("Registered drivers");
    auto ds_src = GDALDataset::Open(map_path_in.c_str());
    if(ds_src == nullptr)
        return InputFileError;
    Logger::write("Opened dataset");
    Mat warped;//(map_file.height, map_file.width, CV_8UC3);


    double dst_transform[6]
    {
       region.nw.x,
                (region.se.x-region.nw.x)/ds_src->GetRasterXSize(),
                0,
                region.nw.y,
                0,
                (region.se.y-region.nw.y)/ds_src->GetRasterYSize()
    };

    report(20, "Warping... ");
    warp(ds_src,warped,dst_transform,this, 20,70);
    ds_src->~GDALDataset();
    if(abort_request)
    {
        warped.release();
        return AbortRequested;
    }
    Logger::write("Warped");
    uint32_t width(warped.cols);
    uint32_t height(warped.rows);

    fout.open(image_path_out,std::ios_base::binary);
    if(!fout.is_open())
        return OutputFileError;


    memset(hdr_data,0,2048);
    // corners coorinates
    double coord[4] {region.nw.y,region.se.x,region.se.y,region.nw.x};
    auto errc = writeHeader(width,height,coord);
    if(errc)
        return errc;
    Logger::write("Header written");

    report(70, "Resizing... ");
    Mat scaled(im_h,im_w,CV_8UC3,Scalar(0xef,0xff,0xff));
    resize(warped,scaled,Size(im_w,im_h),0,0,INTER_LANCZOS4);
    warped.release();
    Mat img_padded(tiles_y*TILE_SIDE, tiles_x*TILE_SIDE,CV_8UC3);

    // mimicre to GlobalMapper
    rectangle(img_padded,
              Rect(0,0,img_padded.cols,img_padded.rows),
              Scalar(0xaf,0xfe,0xff),
              -1,
              LINE_4,0);

    Mat window(img_padded,Rect(0,0,im_w,im_h));
    scaled.copyTo(window);
    scaled.release();
    window.release();
    map_img=img_padded;
    img_padded.release();

    if(abort_request)
    {
        map_img.release();
        return AbortRequested;
    }

    Logger::write("Scaled");
    std::vector<std::future<TilesVector>> jtask_res;
    std::vector<TilesVector> j_res;
    std::vector<uint32_t> compressed_size;
    std::vector<char*> jtile_storage;

    report(85, "Saving... ");
    auto hw_c = std::thread::hardware_concurrency();

    auto tile_height_total=tiles_y;
    auto tile_height_per_task = std::max(unsigned(1),tiles_y/hw_c);
    unsigned start_tile_y=0;


    while(tile_height_total)
    {
        jtile_storage.push_back(reinterpret_cast<char*>(
                                    operator new(im_w*im_h*3/2/hw_c)));
        auto task_height = std::min(tile_height_total, tile_height_per_task);


        jtask_res.emplace_back(      std::async(std::launch::async,
                                                [this](
                                                char* dst,
                                                unsigned start_y,
                                                unsigned stop_y)->TilesVector
        {
            return this->encodeTiles(dst,start_y,stop_y);
        }, jtile_storage.back(),
        start_tile_y,
        start_tile_y+task_height));
        tile_height_total-=task_height;
        start_tile_y+=task_height;
    }

    for(auto& jr : jtask_res)
    {
        j_res.push_back(jr.get());
    }
    map_img.release();
    uint32_t tiles_offset = tiles_table_offset+total_tiles*sizeof(Tile);
    for(auto&jr : j_res)
    {
        uint32_t jstorage_size=0;
        std::transform(jr.begin(),
                       jr.end(),
                       std::back_inserter(tile_hdr),
                       [&tiles_offset,&jstorage_size](Tile& t)->Tile
        {
            Tile tmp(t);
            tmp.offset=tiles_offset;
            tiles_offset+=tmp.size;
            jstorage_size+=tmp.size;
            return tmp;
        });
        compressed_size.push_back(jstorage_size);
    }
    fout.write(reinterpret_cast<char*>(tile_hdr.data()),
               tile_hdr.size()*sizeof(Tile));

    for(auto i=0;i<jtile_storage.size();i++)
    {
        fout.write(reinterpret_cast<char*>(jtile_storage[i]),
                   compressed_size[i]);
        operator delete(jtile_storage[i]);
    }

    const char sign[]{"BirdsEye"};
    fout.write(sign,8);

    if(!fout.good())
    {
        return OutputFileError;
    }

    fout.close();
    return NoError;
}


JnxConverter::TilesVector JnxConverter::encodeTiles(char *tile_data,
                                                  uint32_t start_y,
                                                  uint32_t stop_y)
{
    TilesVector thdr;
    thdr.reserve(2048);
    int err_c{0};
    // hope for that to be cache aligned :-)
    std::unique_ptr<unsigned char[]> tile(new unsigned char[TILE_SIDE*TILE_SIDE*3]);

    auto jtile = tjAlloc(TILE_SIDE*TILE_SIDE*3);
    unsigned long jsize;

    // padded image size
    uint32_t im_w_pad = tiles_x*TILE_SIDE;

    for(auto y = start_y; y<stop_y; y++)
        for(uint32_t x = 0; x <tiles_x; x++)
        {
            for(auto ty=0;ty<TILE_SIDE;ty++)
                memcpy(tile.get()+ty*TILE_SIDE*3,
                       map_img.ptr(y*TILE_SIDE+ty)+x*TILE_SIDE*3,
                       TILE_SIDE*3);
            auto h = tjInitCompress();
            jsize=TILE_SIDE*TILE_SIDE*3;
            auto err = tjCompress2(h,tile.get(),TILE_SIDE,TILE_SIDE*3,TILE_SIDE,
                        TJPF_BGR,&jtile,&jsize,TJSAMP_420,85,0);
            if(h==NULL || err)
            {
                throw(std::logic_error("TurboJpeg error" + std::to_string(err) +
                                       " handle=" + std::to_string((size_t)h)));
            }
            tjDestroy(h);
            thdr.emplace_back( tileCoord(x+1,y),tileCoord(x,y+1), jsize-2);
            // skip SOF bytes
            memcpy(tile_data, jtile+2,jsize-2);
            tile_data += jsize-2;
        }
    tjFree(jtile);
    return thdr;
}




JnxConverter::Error JnxConverter::writeHeader(uint32_t width, uint32_t height, double *coord)
{
    // angle resolution of pixel
    auto lat_pix_res = (coord[0]-coord[2])/height;
    auto lon_pix_res = (coord[1]-coord[3])/width;

    //std::cout<< lat_pix_res << " : " << lon_pix_res << std::endl;
    // let new pixel resolution be geometric mean
    //auto pix_res = sqrt (lat_pix_res*lon_pix_res);
    pix_res = std::min (lat_pix_res,lon_pix_res);

    // keep image size in some bounds
    if(width*height > 10000*10000)
        pix_res*= sqrt(width*height/(10000.0*10000.0));
    //std::cout<< "New pix res = " <<pix_res << std::endl;

    // new image size considering new pixel(true square) resolution
    im_h = height = static_cast<uint32_t>(round((coord[0]-coord[2])/pix_res));
    im_w = width = static_cast<uint32_t>(round((coord[1]-coord[3])/pix_res));

    tiles_x = width/TILE_SIDE + (width%TILE_SIDE ? 1 : 0);
    tiles_y = height/TILE_SIDE +  + (height%TILE_SIDE ? 1 : 0);


    lat1d = coord[0];
    lat2d = lat1d - double(tiles_y*TILE_SIDE)/height * (coord[0]-coord[2]);
    lon2d = coord[3];
    lon1d = lon2d + double(tiles_x*TILE_SIDE)/width *(coord[1]-coord[3]);

    lat1 = deg2int(lat1d);
    lat2 = deg2int(lat2d);
    lon1 = deg2int(lon1d);
    lon2 = deg2int(lon2d);

    uint32_t pos=0;
    MainHeader* mhdr = new(hdr_data) MainHeader;
    mhdr->coord[0]=lat1;
    mhdr->coord[1]=lon1;
    mhdr->coord[2]=lat2;
    mhdr->coord[3]=lon2;
    pos+=sizeof(MainHeader);

    LevelHeader* lhdr = new(hdr_data+pos) LevelHeader;
    total_tiles = lhdr->total_tiles = tiles_x*tiles_y;
    if (total_tiles>50000)
        return TooMuchTiles;
    lhdr->scale = pix_res>1.51e-5 ? 1194 : 2388;
    pos+=sizeof(LevelHeader);

    MapDescription1* dhdr = new(hdr_data+pos) MapDescription1;

    strcpy((char*)dhdr+offsetof(MapDescription1,guid),generateJnxId().c_str());

    strcpy((char*)dhdr+offsetof(MapDescription1,name_cstr),map_name.c_str());
    pos+=sizeof(MapDescription1)+map_name.size();
    MapDescription2* dhdr2 = new(hdr_data+pos) MapDescription2;
    pos+=sizeof(MapDescription2);
    LevelDescription* ldhdr = new(hdr_data+pos) LevelDescription;
    pos+=1024;
    tiles_table_offset=pos;
    lhdr->tile_table_offset=tiles_table_offset;
    fout.write(hdr_data, tiles_table_offset);
    fout.flush();
    return NoError;
}


std::string JnxConverter::generateJnxId()
{
    using namespace std;

    string res;
    minstd_rand gen(chrono::system_clock::now().time_since_epoch().count());
    uniform_int_distribution<> d(0,15);
    const char hex_char[]="0123456789ABCDEF";
    for (auto i : {8,4,4,4,12})
    {
        for(auto j=0;j<i;j++)
            res.append({hex_char[d(gen)]});
        res.append({'-'});
    }
    return (res.substr(0,36));

}
