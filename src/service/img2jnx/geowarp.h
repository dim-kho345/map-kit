#ifndef GEOWARP_H
#define GEOWARP_H

#include <gdal/gdal_priv.h>
#include <gdal/gdalwarper.h>
#include <opencv2/opencv.hpp>

extern const char* warp_opts_str[];

template <class Caller>
bool warp(GDALDataset* ds_src,
         cv::Mat& warped_holder,
         double geotransform[6],
         Caller* caller,
         int percent_complete_at_start,
         int percent_complete_at_end)
{
    using namespace cv;
    using namespace std;

    int32_t w = ds_src->GetRasterXSize();
    int32_t h = ds_src->GetRasterYSize();
    GDALDriver* mem_drv = GetGDALDriverManager()->GetDriverByName("MEM");
    unique_ptr<char[]> warped_data(new char[w*h*3]);
    std::vector<Mat> warped_ch {{h,w,CV_8UC1,warped_data.get()},
                            {h,w,CV_8UC1,warped_data.get()+h*w},
                            {h,w,CV_8UC1,warped_data.get()+h*w*2}};


    string ds_dst_name
    {
        "MEM:::DATAPOINTER=" +
          to_string(reinterpret_cast<size_t>(nullptr/*warped_data*/)) +
                ",PIXELS=" + to_string(1) +
                ",LINES=" + to_string(1) +
                ",BANDS=3,DATATYPE=Byte,PIXELOFFSET=1,LINEOFFSET=" +
                to_string(1) + ",BANDOFFSET=" +
                  to_string(w*h)
    };

    auto ds_dst = mem_drv->Create(ds_dst_name.c_str(),1,1,3,GDT_Byte,nullptr);

    //auto ds_dst = tiff_drv->Create("j:/LIZA/out.tif",w,h,3,GDT_Byte,nullptr);
    //ds_dst->Open("j:/LIZA/out.tif",GA_Update);

    int src_bands[3]{3,2,1};
    int dst_bands[3]{1,2,3};
    GDALWarpOptions* warp_opts = GDALCreateWarpOptions();

    OGRSpatialReference dst_crs;
    dst_crs.importFromEPSG(4326);
    char* dst_proj_wkt;
    dst_crs.exportToWkt(&dst_proj_wkt);
    ds_dst->SetProjection(dst_proj_wkt);
    CPLFree(dst_proj_wkt);

    struct CallerSpecs
    {
        Caller* caller_obj;
        int perc_at_start;
        int perc_at_end;
    } caller_specs {caller, percent_complete_at_start, percent_complete_at_end};

    GDALProgressFunc prog_func = [](double dfComplete, const char *pszMessage, void *pProgressArg)->int
    {
        CallerSpecs* cs   = static_cast<CallerSpecs*>(pProgressArg);
        cs->caller_obj->report(cs->perc_at_start + dfComplete*(cs->perc_at_end - cs->perc_at_start),"Warping...");
        if(cs->caller_obj->abortRequested())
            return false;
        else
            return true;
    };

    double dst_geotransform[6];
    if(geotransform!=nullptr)
        std::copy_n(geotransform,6,dst_geotransform);
    else
        ds_dst->GetGeoTransform(dst_geotransform);

    ds_dst->SetGeoTransform(dst_geotransform);

    warp_opts->hDstDS = nullptr;//GDALDataset::ToHandle(ds_dst);
    warp_opts->hSrcDS = GDALDataset::ToHandle(ds_src);
    warp_opts->nBandCount = 3;
    warp_opts->eResampleAlg = GRA_Cubic;
    warp_opts->eWorkingDataType = GDT_Byte;
    warp_opts->panSrcBands=src_bands;
    warp_opts->panDstBands=dst_bands;
    warp_opts->pTransformerArg =
             GDALCreateGenImgProjTransformer2(ds_src, ds_dst,nullptr);
    warp_opts->pfnTransformer = GDALGenImgProjTransform;
    warp_opts->pfnProgress=prog_func;
    warp_opts->pProgressArg = &caller_specs;


    warp_opts->papszWarpOptions=const_cast<char**>(warp_opts_str);

    GDALWarpOperation warp_operation;
    warp_operation.Initialize(warp_opts);


    warp_operation.WarpRegionToBuffer( 0,0,w,h,warped_data.get(),GDT_Byte);
    Mat warped(h,w,CV_8UC3);
    merge(warped_ch,warped);
    for(auto& ch : warped_ch)
        ch.release();
    warped_holder = warped;
    warped.release();

    GDALDestroyGenImgProjTransformer(warp_opts->pTransformerArg);
    delete warp_opts;
    delete ds_dst;//->~GDALDataset();
    return true;
}

#endif // GEOWARP_H
