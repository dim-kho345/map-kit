#ifndef MAPPARSRER_H
#define MAPPARSRER_H

#include <fstream>

struct MAPParser
{
    bool openMAP(std::string filename);
    std::string img_fname[2];
    double lat1, lat2, lon1, lon2;
    unsigned width, height;
};

#endif // MAPPARSRER_H
