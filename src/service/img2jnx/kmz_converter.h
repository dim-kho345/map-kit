#ifndef KMZCONVERTER_H
#define KMZCONVERTER_H

#include <thread>
#include <atomic>
#include <sstream>
#include "service.h"

class KmzConverter : public ServiceImg2Kmz
{
public:
    KmzConverter(GUI* p_gui, Resources res=Memory);
    ~KmzConverter();
    void start() override;
    void abort() override;
    using ServiceBase::report;
    bool abortRequested() {
        return abort_request;
    }
private:
      enum Error{
          NoError=0,
          AbortRequested,
          OutputFileError,
          TooMuchTiles,
          MapNameOversized,
          InputFileError
      };

      Error convertFromMap();
  private:

      double cont_lon_span, cont_lat_span;

      void writeKMLHeader(std::string &name, double w, double e, double n, double s);
      void writeKMLTile(std::string name, double w, double e, double n, double s);
      void finishKML();

      std::vector<std::string> tile_filename;

      unsigned tile_width, tile_height;
      struct KMZRatio
      {
          KMZRatio(unsigned x,unsigned y,unsigned total,double r) :
              x(x),y(y),total(total),ratio(r) {}
          // relative x length
          const unsigned x;
          // relative y length
          const unsigned y;
          // maximum megapixels for current ratio
          const unsigned total;
          const double ratio;
      };
      std::vector<KMZRatio> kmz_ratio;
      void initTilesRatio();
      std::ostringstream kml_doc;

    void job();
    std::atomic<int> abort_request{false};
    std::thread t;
};

#endif // KMZCONVERTER_H



