#include <future>
#include <random>
#include <turbojpeg.h>
#include <gdal/gdal_priv.h>
#include <opencv2/opencv.hpp>
#include <zip.h>
#include <iomanip>
#include "log.h"
#include "kmz_converter.h"
#include "geowarp.h"
#include <iostream>



KmzConverter::KmzConverter(GUI* p_gui, Resources res) :
    ServiceImg2Kmz(p_gui, res)
{

}

KmzConverter::~KmzConverter()
{

}

void  KmzConverter::start()
{
    t=std::thread(&KmzConverter::job,this);
    t.detach();
}

void  KmzConverter::abort()
{
    abort_request=true;
    gui->onProgress(function(), model_ctx, -1, -1, "Aborting...");
}



void KmzConverter::job()
{
    using namespace std;
    Logger::write("Started KMZ task ",map_path_in);
    initTilesRatio();

    auto res = convertFromMap();
    switch(res)
    {
    case AbortRequested:
        finishedVerbose(false, GUI::Warning, "KMZ conversion aborted");
        break;
    case NoError:
        finished(true);
        break;
    default:
        finishedVerbose(false, GUI::Warning, "JNZ conversion failed. Error "+std::to_string(res));
        break;
    }
}

void KmzConverter::writeKMLHeader(std::string& name, double w, double e, double n, double s)
{
    kml_doc << "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
    kml_doc << "<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\">\n";
    kml_doc << "<Document>\n<name>" << name <<"</name>\n";
    kml_doc << "<Region>\n  <LatLonAltBox>\n";
    kml_doc << "    <north>" << n << "</north>\n";
    kml_doc << "    <south>" << s << "</south>\n";
    kml_doc << "    <east>" << e << "</east>\n";
    kml_doc << "    <west>" << w << "</west>\n";
    kml_doc << "  </LatLonAltBox>\n</Region>\n";

}

void KmzConverter::writeKMLTile(std::string name, double w, double e, double n, double s)
{
    kml_doc << "<GroundOverlay>\n  <name>" << name << "</name>\n";
    kml_doc << "  <drawOrder>0</drawOrder>\n  <gx:altitudeMode>clampToSeaFloor</gx:altitudeMode>\n";
    kml_doc << "  <Icon>\n    <href>" << name <<".jpg</href>\n  </Icon>\n";
    kml_doc << "  <LatLonBox>\n";
    kml_doc << "    <north>" << n << "</north>\n";
    kml_doc << "    <south>" << s << "</south>\n";
    kml_doc << "    <east>" << e << "</east>\n";
    kml_doc << "    <west>" << w << "</west>\n";
    kml_doc << "  </LatLonBox>\n</GroundOverlay>\n";

}

void KmzConverter::finishKML()
{
    kml_doc << "</Document>\n</kml>\n";

}

void KmzConverter::initTilesRatio()
{
    kmz_ratio.clear();
    for(unsigned x=4;x<25;x++)
        kmz_ratio.emplace_back(x,100/x,x*(100/x)*1024*1024,
                               static_cast<double>(x)/(100/x));
    for(unsigned x=4;x<21;x++)
        kmz_ratio.emplace_back(x,85/x,x*(85/x)*1024*1024,
                               static_cast<double>(x)/(85/x));
}


KmzConverter::Error KmzConverter::convertFromMap()
{
    using namespace cv;
    report(0, "Loading "+map_path_in.substr(map_path_in.rfind('/')));
    tile_filename.clear();
    kml_doc.str().clear();
    kml_doc.precision(10);
    GDALRegister_MAP();
    GDALRegister_MEM();
    GDALRegister_BMP();
    GDALRegister_GTiff();
    Logger::write("Registered drivers");
    report(0,"Opening " + map_path_in);
    auto ds_src = GDALDataset::Open(map_path_in.c_str());
    if(ds_src == nullptr)
        return InputFileError;
    Logger::write("Opened dataset");
    Mat warped;

    LLPoint nw(region.nw);
    LLPoint se(region.se);
    auto lon_span = cont_lon_span = se.lon-nw.lon;
    auto lat_span = cont_lat_span = nw.lat-se.lat;

    // transformation matrix image<->crs coordinates
    double dst_transform[6]
    {
        nw.lon,
                lon_span/ds_src->GetRasterXSize(),
                0,
                nw.lat,
                0,
                -lat_span/ds_src->GetRasterYSize()};


    // angle resolution of source image (deg/pixel)
    double angle_res = std::min(dst_transform[1],-dst_transform[5]);

    if(abort_request)
        return AbortRequested;

    // transform to geographic lon/lat projection
    warp(ds_src,warped,dst_transform,this, 20, 70);
    ds_src->~GDALDataset();

    if(abort_request)
        return AbortRequested;

    // find most fit width/height ratio for container image
    decltype (kmz_ratio)::const_iterator dimentions;
    double lonlat_ratio_delta = 100.0;
    double lonlat_ratio = cont_lon_span/cont_lat_span;
    for(auto it = kmz_ratio.cbegin();it!=kmz_ratio.cend();it++)
        if(lonlat_ratio_delta > std::abs(it->ratio-lonlat_ratio))
        {
            lonlat_ratio_delta = std::abs(it->ratio-lonlat_ratio);
            dimentions = it;
        }

    // find image size for current container and angle resolution

    // 1. Find geospan of container image, using
    // lon or lat expanding
    if(lonlat_ratio > dimentions->ratio)
        cont_lat_span=cont_lon_span / dimentions->ratio;
    else
        cont_lon_span=cont_lat_span * dimentions->ratio;

    // angle resolution for container image (deg/pixel)
    double cont_angle_res = angle_res;
    // container dimentions
    unsigned cont_w, cont_h;

    // fit constrains
    for(;;)
    {
        // container size in pixels
        cont_w = std::round(cont_lon_span/cont_angle_res);
        cont_h = std::round(cont_lat_span/cont_angle_res);

        // pad container size to integer multiple of it's size dimentions
        // and adjust geoscale accordingly
        if((cont_w % dimentions->x) != 0)
            cont_w += dimentions->x - cont_w % dimentions->x;
        if((cont_h % dimentions->y) != 0)
            cont_h += dimentions->y - cont_h % dimentions->y;

        // if container exceeds allowed megapixels limit, decrease angle
        // resolution (increase numerically deg/pix)
        if(cont_w * cont_h > dimentions->total)
        {
            // leave a little margin from required
            cont_angle_res *= std::sqrt(static_cast<double>(cont_w*cont_h)/
                                        dimentions->total)*1.001;
        }
        else
            break;
    }
    cont_lon_span = cont_w * cont_angle_res;
    cont_lat_span = cont_h * cont_angle_res;
    report(70, "Scaling");
    Mat warped_scaled(std::round(lat_span/cont_angle_res),
                      std::round(lon_span/cont_angle_res),
                      CV_8UC3);
    resize(warped,warped_scaled,
           Size(warped_scaled.cols,warped_scaled.rows),
           0,0,INTER_LANCZOS4);
    Logger::write("Scaled");
    warped.release();
    Mat container(cont_h,cont_w,CV_8UC3);
    rectangle(container,
              Rect(0,0,container.cols,container.rows),
              Scalar(0xaf,0xfe,0xff),
              -1,
              LINE_4,0);
    warped_scaled.copyTo(
                Mat(container,Rect(0,0,warped_scaled.cols,warped_scaled.rows)) );
    warped_scaled.release();
    tile_width = cont_w/dimentions->x;
    tile_height = cont_h/dimentions->y;
    // use single task this time

    if(abort_request)
        return AbortRequested;

    report(85, "Saving");
    // open zip file
    int zip_err;
    auto zf = zip_open(kmz_path_out.c_str(),ZIP_CREATE,&zip_err);
    if(zf==NULL)
        return OutputFileError;

    writeKMLHeader(location_name,nw.lon, nw.lon+cont_lon_span,
                   nw.lat, nw.lat-cont_lat_span);

    using namespace std;

    Mat tile;
    tile.create(tile_height,tile_width,CV_8UC3);
    auto jtile = tjAlloc(tile_width*tile_height*3);
    unsigned long jsize;
    for(unsigned y=0;y<cont_h/tile_height;y++)
        for(unsigned x=0;x<cont_w/tile_width;x++)
        {
            char* jtile_copy = new char[tile_width*tile_height*3];
            Mat window(container,Rect(tile_width*x,    tile_height*y,
                                      tile_width,tile_height));
            window.copyTo(tile);
            auto h = tjInitCompress();
            jsize = tile_width*tile_height*3;
            auto err = tjCompress2(h,tile.ptr(),tile_width,tile_width*3,
                                   tile_height,TJPF_BGR,&jtile,&jsize,
                                   TJSAMP_420,85,0);
            if(err)
                throw(std::logic_error("KMZ error: turbojpeg"));
            tjDestroy(h);
            memcpy(jtile_copy,jtile,jsize);
            ostringstream tname;
            tname << "kml_image_" << std::setw(3) << std::setfill('0') <<
                     y << '_' << std::setw(3) << x;

            writeKMLTile(tname.str(),
                         nw.lon+x*cont_lon_span/dimentions->x,
                         nw.lon+(x+1)*cont_lon_span/dimentions->x,
                         nw.lat-y*cont_lat_span/dimentions->y,
                         nw.lat-(y+1)*cont_lat_span/dimentions->y);
            auto zs = zip_source_buffer(zf,jtile_copy,jsize,1);
            if(zs==nullptr)
                throw(std::logic_error("KMZ error: init zip source buffer"));
            if(zip_file_add(zf, (tname.str()+".jpg").c_str(),zs,ZIP_FL_OVERWRITE)==-1)
                throw(std::logic_error(std::string("KMZ error: adding tile: ")+
                     zip_strerror(zf)));
        }
    finishKML();
    container.release();
    auto kml_size = kml_doc.str().size();
    char* kml_doc_copy = new char[kml_size];
    memcpy(kml_doc_copy,kml_doc.str().c_str(),kml_size);
    auto zs = zip_source_buffer(zf,kml_doc_copy,kml_size,1);
    if(zip_file_add(zf, "doc.kml",zs,ZIP_FL_OVERWRITE)==-1)
        throw(std::logic_error(std::string("KMZ error: adding KML: ")+
             zip_strerror(zf)));
    zip_close(zf);
    tjFree(jtile);
    return NoError;
}
