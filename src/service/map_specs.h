#ifndef MAPSPECS_H
#define MAPSPECS_H
#include <QProcess>
#include <mutex>
#include "map_source.h"


class MapSourceRemote : public MapSource
{
public:
    MapSourceRemote(const std::string map_name,
              const std::vector<int> zoom,
              const bool topo,
              const std::string crs_name,
              const std::string cache_name,
              const std::string content_type,
              QProcess* proc) :
        MapSource(map_name, zoom, topo, crs_name, cache_name, content_type),
        map_proc(proc) {}
    std::string tileUrl(int x, int y, int zoom) const override ;
    std::string tilePath(int x, int y, int zoom) const override;
 private:
    QProcess* map_proc;

};

#endif // MAPSPECS_H
