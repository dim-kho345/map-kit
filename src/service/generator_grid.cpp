#include <QStringList>
#include <QDateTime>
#include <QTimeZone>
#include "generator_grid.h"
#include "util.h"


bool WptFile::writeHeader()
{
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return false;
    }

    points_opened=true;
    str.setDevice(&file);
    str.setRealNumberPrecision(8);
    str << "OziExplorer Waypoint File Version 1.1" << endl;
    str << "WGS 84" << endl;
    str << "Reserved 2" << endl;
    str << "garmin" << endl;
    num=1;
    return true;
}

bool WptFile::startTrack(const QString& name,int num_points)
{
    if (!trk_file.open(QFile::WriteOnly | QFile::Text))
        return false;

    track_opened=true;
    tr_str.setDevice(&trk_file);
    tr_str.setRealNumberPrecision(8);
    tr_str << "OziExplorer Track Point File Version 2.1" << endl;
    tr_str << "WGS 84" << endl;
    tr_str << "Altitude is in Feet" << endl;
    tr_str << "Reserved 3" << endl;
    tr_str << "0,2,255," << name << ",0,0,2,8421376,-1,0" << endl;
    tr_str << num_points << endl;
    return true;
}
void WptFile::writePoint(GeoPoint p,QString& name)
{
    auto& lon = p.x, lat=p.y;
    str << num++ << "," << name << ", ";
    str << lat << ", " << lon << ", ";
    str << delphiDate(unix_time) << ", 0, 1, 3,         0,     65535,, 0, 0,0,   -777, 6, 0,17,0,10.0,2,,,," << endl;
}

void WptFile::writeTrackPoint(GeoPoint p)
{
    auto& lon = p.x, lat=p.y;
    tr_str << lat << ", " << lon << ",0, -777.0,";
    tr_str << delphiDate(unix_time) << ",," << endl;
}


void WptFile::close()
{
    if(points_opened)
        file.close();
    if(track_opened)
        trk_file.close();
}


bool GPXFile::writeHeader()
{
    if (!file.open(QFile::WriteOnly | QFile::Text))
        return false;
    date_time=QDateTime::currentDateTime().toString("yyyy-MM-ddTh:mm:ssZ");
    xml.setDevice(&file);
    xml.setCodec("UTF-8");
    xml.writeStartDocument();
    xml.writeStartElement("gpx");
    xml.writeDefaultNamespace("http://www.topografix.com/GPX/1/1");
    xml.writeAttribute("version","1.1");
    return true;
}

bool GPXFile::startTrack(const QString& name, int)
{
    xml.writeStartElement("trk");
    xml.writeStartElement("name");
    xml.writeCharacters(name);
    xml.writeEndElement();
    xml.writeStartElement("desc");
    xml.writeEndElement();
    xml.writeStartElement("trkseg");
    return true;
}

void GPXFile::writePoint(GeoPoint p,QString &name)
{
    auto& lon = p.x, lat=p.y;
    xml.writeStartElement("wpt");
    xml.writeAttribute("lat",QString("%1").arg(lat,0,'f',8));
    xml.writeAttribute("lon",QString("%1").arg(lon,0,'f',8));
    xml.writeStartElement("time");
    xml.writeCharacters(date_time);
    xml.writeEndElement();
    xml.writeStartElement("name");
    xml.writeCharacters(name);
    xml.writeEndElement();
    xml.writeEndElement();
}

void GPXFile::writeTrackPoint(GeoPoint p)
{
    auto& lon = p.x, lat=p.y;
    xml.writeStartElement("trkpt");
    xml.writeAttribute("lat",QString("%1").arg(lat,0,'f',7));
    xml.writeAttribute("lon",QString("%1").arg(lon,0,'f',7));
    xml.writeStartElement("time");
    xml.writeCharacters(date_time);
    xml.writeEndElement();
    xml.writeEndElement();
}

void GPXFile::close()
{
    xml.writeEndDocument();
    file.close();
}


bool DMXFile::writeHeader()
{
    if (!file.open(QFile::WriteOnly | QFile::Text))
        return false;
    str.setDevice(&file);
    str.setRealNumberPrecision(5);
    str << "Datum,WGS 84" << endl;
    return true;
}

void DMXFile::writePoint(GeoPoint p,QString& name)
{
    auto& lon = p.x, lat=p.y;
    QDateTime dt(QDateTime::currentDateTime());
    int lat_deg,lon_deg;
    double lat_min,lon_min;
    GeoCalc::toDegMin(lat,lat_deg,lat_min);
    GeoCalc::toDegMin(lon,lon_deg,lon_min);
    str << "WP,DMX," << name << ", ";
    str << lat_deg << ',' << lat_min << ", ";
    str << lon_deg << ',' << lon_min;
    str << dt.toString(", MM/dd/yyyy,h:mm:ss");
    str << ",,A,N,-9999" << endl;
}


void DMXFile::close()
{
    file.close();
}



bool KMLFile::writeHeader()
{
    if (!file.open(QFile::WriteOnly | QFile::Text))
        return false;
    date_time=QDateTime::currentDateTime().toString("yyyy-MM-ddTh:mm:ssZ");
    xml.setDevice(&file);
    xml.setCodec("UTF-8");
    xml.writeStartDocument();
    xml.writeStartElement("kml");
    xml.writeDefaultNamespace("http://earth.google.com/kml/2.0");
    xml.writeStartElement("Document");
    xml.writeStartElement("name");
    xml.writeCharacters("OziExplorer Data");
    xml.writeEndElement();
    xml.writeStartElement("visibility");
    xml.writeCharacters("1");
    xml.writeEndElement();
    xml.writeStartElement("Folder");
    xml.writeStartElement("name");
    xml.writeCharacters("Waypoints");
    xml.writeEndElement();
    return true;
}

/*
<Placemark>
  <name>G7</name>
  <description></description>
  <styleUrl>#waypoint</styleUrl>
    <Point>
      <coordinates>34.65393,57.726829</coordinates>
    </Point>
    <end>
    <TimeInstant><timePosition><time>2020-02-14T19:02:00Z</time></timePosition></TimeInstant>
    </end>
  </Placemark>
*/

void KMLFile::writePoint(GeoPoint p,QString& name)
{
    xml.writeStartElement("Placemark");
    xml.writeStartElement("name");
    xml.writeCharacters(name);
    xml.writeEndElement();
    xml.writeStartElement("Point");
    xml.writeStartElement("coordinates");
    xml.writeCharacters(QString("%1,%2").arg(p.x).arg(p.y));
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeEndElement();
}

void KMLFile::finishPoints()
{
    xml.writeEndElement(); //</Folder>
}

bool KMLFile::startTrack(const QString& name, int)
{
    xml.writeStartElement("Style");
    xml.writeAttribute("id","route");

    xml.writeStartElement("icon");
    xml.writeStartElement("href");
    xml.writeCharacters("root://icons/palette-4.png?x=160&amp;y=0&amp;w=32&amp;h=32");
    xml.writeEndElement();
    xml.writeEndElement();

    xml.writeStartElement("LineStyle");
    xml.writeStartElement("color");
    xml.writeCharacters("FF0000FF");
    xml.writeEndElement();
    xml.writeStartElement("width");
    xml.writeCharacters("2.0");
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeEndElement();

    xml.writeStartElement("Placemark");
    xml.writeStartElement("styleUrl");
    xml.writeCharacters("#route");
    xml.writeEndElement();
    xml.writeStartElement("name");
    xml.writeCharacters("Grid_Line");
    xml.writeEndElement();
    xml.writeStartElement("LineString");
    xml.writeStartElement("coordinates");
    return true;
}

void KMLFile::writeTrackPoint(GeoPoint p)
{
    xml.writeCharacters(QString("%1,%2 ").arg(p.x,0,'g',7)
                        .arg(p.y,0,'g',7));
}


void KMLFile::close()
{
    /*xml.writeEndElement();
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeEndElement();*/
    xml.writeEndDocument();
    file.close();
}


GeoPoint GeneratorGrid::cvtUTM2EN(int x, int y, int zone)
{
    auto p = CRS::convert(CRS::CoordRefSys(CRS::EPSG_UTM+zone), CRS::EPSG4326,
                          static_cast<double>(x),
                          static_cast<double>(y));
    return {p.first, p.second};
}

void GeneratorGrid::start()
{
    generator.emplace_back(new GPXFile(gpx_path));
    generator.emplace_back(new WptFile(wpt_path, kit_date, plt_path));
    generator.emplace_back(new DMXFile(dmx_path));
    generator.emplace_back(new KMLFile(kml_path, kit_date));

    using std::lround;
    GeoPoint point;


    // grid UTM coords settings
    int32_t x1(lround(region.nw.x)),x2(lround(region.se.x));
    int32_t y1(lround(region.nw.y)),y2(lround(region.se.y));
    auto zone = region.nw.zone;
    QString point_name;

    int32_t x,y,row=offset_y+1,col=0;

    for(auto& g : generator)
        if(!g->writeHeader())
        {
            finishedVerbose(false,GUI::Critical, "Coudn't create grid files");
            return;
        }

    grid.clear();
    for(y=y1;y>=y2;y-=step)
    {
        col=offset_x;
        for(x=x1;x<=x2;x+=step)
        {
            point=cvtUTM2EN(x, y, zone);
            auto& lon = point.x, lat=point.y;
            point_name.clear();
            int pref_idx=col/26;
            if(pref_idx)
                point_name=QChar('A'+pref_idx-1);
            point_name.append(QChar('A'+col%26));
            point_name.append(QString("%1").arg(row));
            for(auto& g : generator)
                g->writePoint(point,point_name);
            grid.push_back({point_name.toStdString(),3, {lon, lat}});
            ++col;
        }
        ++row;
    }

    for(auto& g : generator)
        g->finishPoints();

    for(auto& g : generator)
        if(!g->startTrack("Grid_Line",((x2-x1)/step+1)*2+
                          ((y1-y2)/step+1)*2-1))
        {
            finished(false);
            return;
            }

    // пройдем змейкой по горизонтали
    x=x1;y=y1;
    while(x<=x2)
    {
        point=cvtUTM2EN(x, y, zone);
        for(auto& g : generator)
            g->writeTrackPoint(point);
        y=(y==y1) ? y2 : y1;
        point=cvtUTM2EN(x, y, zone);
        for(auto& g : generator)
            g->writeTrackPoint(point);
        x+=step;
    }
    x=x1;
    point=cvtUTM2EN(x, y, zone);
    for(auto& g : generator)
        g->writeTrackPoint(point);

    // пройдем змейкой во вертикали
    int32_t dy=(y==y1) ? -step : step;
    y+=dy;

    for (int i=0;i<(y1-y2)/step;i++)
    {
        point=cvtUTM2EN(x, y, zone);
        for(auto& g : generator)
            g->writeTrackPoint(point);
        x=(x==x1) ? x2 : x1;
        point=cvtUTM2EN(x, y, zone);
        for(auto& g : generator)
            g->writeTrackPoint(point);
        y+=dy;
    }

    for(auto& g : generator)
        g->close();

    finished(true);
}
