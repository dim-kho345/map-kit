#include <iostream>
#include "map_source.h"
#include "settings.h"

std::string MapSource::tilePath(int x, int y, int zoom, CacheType cache_type) const
{
    char str[160];
    std::string path;
    if(cache_type==CacheType::SASPlanet)
    {
        snprintf(str, sizeof(str),
                 "z%i/%i/x%i/%i/y%i",
                 zoom+1,
                 x/1024,
                 x,
                 y/1024,
                 y);
        path=str;
        path.append('.' + content_type);
    }

    if(cache_type==CacheType::GlobalMapper)
    {
        snprintf(str, sizeof(str),
                 "z%i/%i/%i",
                 zoom,
                 y,
                 x);
        path=str;
        if(!is_topo)
            path.append(".jpg");
        else
            path.append(".png");
    }
    return path;
}


