#include <QSettings>
#include <QCoreApplication>
#include <QTextCodec>
#include <QTextStream>
#include <QFile>
#include <QDate>
#include <QString>
#include <QSettings>
#include <QProcess>
#include <fstream>
#include "wiki2wpt.h"
#include "ozi.h"
#include "util.h"
#include "generator_grid.h"

void Wiki2WptExecutor::start()
{
    setConfig();
    setGrid(grid_step);
    setDate(delphiDate(kit_date));
    setPlace(location);
    setConfig();

    proc.start(QCoreApplication::applicationDirPath()+"/Wiki2Wpt.exe");
    proc.waitForFinished(-1);

    if (!QFile::exists(QString::fromStdString(pnt_path_sat)))
        return;
    OziWptLoader w2w_points;
    filterWpt(pnt_path_sat,
                        QCoreApplication::applicationDirPath().toStdString()+
                        "/nono_list.txt");
    // а разве топо отфильтровать не надо?
    fix100(pnt_path_sat);
    fix100(pnt_path_topo);
    if(w2w_points.load(pnt_path_sat))
    {
        poi = w2w_points.points();

        //gpsbabel.exe -i ozi,codec=windows-1251 -f input.wpt -o garmin_gpi,writecodec=windows-1251 -F output.gpi
        auto l_str = QCoreApplication::applicationDirPath()+"/gpsbabel.exe " +
                QString("-i ozi,codec=windows-1251 -f %1 "
                        "-o garmin_gpi,writecodec=windows-1251 -F %2").arg(QString::fromStdString(pnt_path_sat))
                                                                        .arg(QString::fromStdString(pnt_path_gpi));

        proc.start(l_str);
        if(proc.waitForFinished(3000)==false)
        {
            gui->onUserMessage(GUI::Warning, "Конвертация GPI не удалась");
        }
        GPXFile gpx(pnt_path_gpx);
        gpx.writeHeader();
        for(auto p : poi)
        {
            auto name_ref(QString::fromStdString(p.name));
            gpx.writePoint(p, name_ref);
        }
        gpx.close();
    }
}


bool Wiki2WptExecutor::setGrid(int grid_step)
{
    QSettings s("HKEY_CURRENT_USER\\Software\\LA\\Cartography\\Wiki2Wpt\\TMainForm\\",QSettings::NativeFormat);
    if(s.value("cbMapGrid_Text").isValid())
    {
            s.setValue("cbMapGrid_Text",QString("%1m").arg(grid_step));
            return true;
    }
    else
        return false;
}

bool Wiki2WptExecutor::setDate(float date)
{
    QSettings s("HKEY_CURRENT_USER\\Software\\LA\\Cartography\\Wiki2Wpt\\TMainForm\\",QSettings::NativeFormat);
    if(s.value("cbMapDate_Date").isValid())
    {
        s.setValue("cbMapDate_Date",QString("%1").arg(date,0,'f',4));
        return true;
    }
    return false;
}
bool Wiki2WptExecutor::setPlace(const std::string& place)
{
    QSettings s("HKEY_CURRENT_USER\\Software\\LA\\Cartography\\Wiki2Wpt\\TMainForm\\",QSettings::NativeFormat);
    if(s.value("edMapPlace_Text").isValid())
    {
        s.setValue("edMapPlace_Text",QString::fromStdString(place));
        return true;
    }
    return false;
}

void Wiki2WptExecutor::setConfig()
{
    QSettings s("HKEY_CURRENT_USER\\Software\\LA\\Cartography\\Wiki2Wpt\\TMainForm\\",QSettings::NativeFormat);
    s.setValue("cbGridImport_Checked","false");
    s.setValue("cbZoom1_ItemIndex","8");
    s.setValue("cbZoom2_ItemIndex","8");
}


void Wiki2WptExecutor::filterWpt(std::string in_filename, std::string nono_filename)
{
    QString line;
    QFile in(QString::fromStdString(in_filename));
    QFile out("nono_tmp.txt");
    QFile nono(QString::fromStdString(nono_filename));
    QTextStream in_str(&nono);
    QTextStream out_str(&out);
    out_str.setCodec(QTextCodec::codecForName("Windows-1251"));
    QStringList nono_list;
    if(nono.open(QFile::ReadOnly))
        while(!in_str.atEnd())
            nono_list.append(in_str.readLine());
    nono.close();
    nono.setFileName(":/nono_list.txt");
    if(nono.open(QFile::ReadOnly))
        while(!in_str.atEnd())
            nono_list.append(in_str.readLine());
    nono.close();
    in_str.setDevice(&in);
    if(!out.open(QFile::WriteOnly))
        return;
    if(!in.open(QFile::ReadOnly))
        return;
    for(int i=0;i<4;i++) out_str << in_str.readLine() << endl;
    int point_num=1;
    while(!in_str.atEnd())
    {
        int num;
        bool include=true;
        in_str >> num;
        line=in_str.readLine();
        for(auto&s : nono_list)
            if(!s.isEmpty() && line.contains(s,Qt::CaseInsensitive))
            {
                include=false;
                break;
            }
        if(include)
            out_str << point_num++ << line << endl;
    }
    in.close();
    out.close();
    in.remove();
    out.rename(QString::fromStdString(in_filename));
}


bool Wiki2WptExecutor::fix100(const std::string& path)
{

    using namespace std;

    struct OFStreamWrapper
    {
        OFStreamWrapper(ofstream& of, string path) :
            fstr(of), path(path)
        {}
        ~OFStreamWrapper() {
            if(fstr.is_open())
                fstr.close();
            QFile::remove(path.c_str());
        }
        ofstream& fstr;
        string path;
    };


    char header[4][512];
    char line[512];

    ifstream wp(path);
    string temp_path = path+".tmp";
    ofstream temp(temp_path);


    OFStreamWrapper temp_wrapper(temp, temp_path);



    temp.precision(10);
    if(!wp.is_open())
        return false;
    int i, line_num{1};
    for(i=0;i<4;i++)
    {
        wp.getline(header[i],sizeof(header[0]));
        if(wp.bad())
            return false;
    }

    for(i=0;i<4;i++)
        temp << header[i] << '\n';


    while(!wp.eof() && !temp.bad())
    {
        // позиции запятых, окружающих долготу
        string::size_type pos1, pos2;
        double lon;
        wp.getline(line,sizeof(line));
        const string str(line);
        for(i=0, pos1=0;i<3;i++)
        {
            pos1 = str.find(',',pos1+1);
            if(pos1==string::npos)
                break;
        }
        if(pos1==string::npos)
            break;
        else
            ++pos1;
        pos2 = str.find(',',pos1);
        if(pos2==string::npos)
            break;
        while(str.at(pos1)==' ')
            ++pos1;
        if(pos1==pos2)
            break;

        try {
            lon = stod(str.substr(pos1, pos2));
        } catch (logic_error& ex) {
            throw logic_error("wikipnt: lontitude stod error pnt# "+to_string(line_num));
        }
        if(lon<19)
        {
            lon*=10;
            temp << str.substr(0,pos1)  << lon << str.substr(pos2) << '\n';
        }
        else
            temp << str << '\n';
    }
    temp.close();
    wp.close();

    QFile::remove(path.c_str());
    QFile::copy(temp_path.c_str(), path.c_str());

    return true;

}
