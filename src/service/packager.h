#ifndef PACKAGER_H
#define PACKAGER_H

#include <atomic>
#include <thread>
#include "service.h"



class Packager : public ServiceZip
{
public:
    Packager(GUI* p_gui, Resources res=None);
    void start() override;
    void abort() override {}
    using ServiceBase::report;

private:
    void job();

    std::vector<std::string> dir;

    bool copyFiles();
    void zip();
    std::vector<std::string> error_text;
    std::atomic<int> abort_request{false};
    std::thread t;
};

#endif // PACKAGER_H
