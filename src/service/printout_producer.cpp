#include "printout_producer.h"
#include <QMargins>
#include <QImage>
#include <QPdfWriter>
#include <QPrinterInfo>
#include "printout_producer.h"
#include "settings.h"
#include "log.h"
#include "util.h"


Point imgPointFromUTM(ImageCRS& im_c, UTMPoint p)
{
    GeoPoint t = CRS::convert(CRS::EPSG_UTM + p.zone, im_c.epsg, p.x, p.y);
    return im_c.imgPoint(t);
}

PrintoutProducer::PrintoutProducer(GUI* p_gui, Resources res):
    ServicePrintout(p_gui, res)
{
    calcSuitCases();
}

std::unordered_map<QPageSize::PageSizeId,QString> size_suffix {
    {QPageSize::A0,"_A0"},
    {QPageSize::A1,"_A1"},
    {QPageSize::A2,"_A2"},
    {QPageSize::A3,"_A3"},
};

const std::unordered_map<PrintFormat, QPageSize::PageSizeId> PrintoutProducer::page_format{
    {PrintFormat::A0, QPageSize::A0},
    {PrintFormat::A1, QPageSize::A1},
    {PrintFormat::A2, QPageSize::A2},
    {PrintFormat::A3, QPageSize::A3},
    {PrintFormat::A4, QPageSize::A4}
};

// большее число означает меньший размер
const std::unordered_map<QPageSize::PageSizeId,double> PrintoutProducer::label_scale {
    {QPageSize::A4,2.6},
    {QPageSize::A3,2.2},
    {QPageSize::A2,2.2},
    {QPageSize::A1,2},
    {QPageSize::A0,1.9},
};

#define PI 3.1415926

constexpr int dpi=600;
// dots per centimeter
constexpr int dpc=dpi/2.54;
// page margin mm
constexpr double margin=1;
constexpr double margin_split=5;
/*
std::unordered_map<QPageSize::PageSizeId,int> dpi {
    {QPageSize::A0,600},
    {QPageSize::A1,600},
    {QPageSize::A2,600},
    {QPageSize::A3,600},
};
*/

const PrintoutProducer::SuitCaseArr PrintoutProducer::suit_a3_src {
    {false,2,1},
    {false,3,1},
    {true,2,1}
};

const PrintoutProducer::SuitCaseArr PrintoutProducer::suit_a2_src {
    {false,2,2},
    {false,3,3},
    {true,2,2},
    {true,2,3}
};

const PrintoutProducer::SuitCaseArr PrintoutProducer::suit_a1_src {
    {false,3,3},
    {false,4,2},
    {false,5,2},
    {false,4,3},
    {true,3,3},
    {true,3,4},
    {true,4,3}
};

const PrintoutProducer::SuitCaseArr PrintoutProducer::suit_a0_src {
    {false,5,3},
    {false,4,4},
    {false,4,3},
    {true,4,4},
    {true,3,4},
    {true,3,5}
};



PrintoutProducer::SuitCaseMap PrintoutProducer::suit_a3;
PrintoutProducer::SuitCaseMap PrintoutProducer::suit_a2;
PrintoutProducer::SuitCaseMap PrintoutProducer::suit_a1;
PrintoutProducer::SuitCaseMap PrintoutProducer::suit_a0;


void PrintoutProducer::start()
{
    t=std::thread(&PrintoutProducer::job,this);
    t.detach();
    //    job();
}

void PrintoutProducer::job()
{
    Logger::write("Started printout thread for ", print_path);
    try
    {
        if(prn_type != PrintType::Splitted)
        {
            if(!printWhole())
                finishedVerbose(true, GUI::Info, print_path + " - ошибка печати");
        }
        else
        {
            if(!printSplitted())
                finishedVerbose(true, GUI::Info, print_path + " - ошибка печати");
        }
        finishedVerbose(true, GUI::Info, print_path + " отправлено на печать");
    }
    catch(std::exception& ex)
    {
        Logger::write(print_path + ' ' + ex.what());
        finishedVerbose(false, GUI::Critical,
                        "Ошибка печати " + print_path + ": "+ ex.what());
    }
}




void PrintoutProducer::calcSuitCases()
{
    QSizeF size = QPageSize(QPageSize::A4).size(QPageSize::Unit::Millimeter);
    double h=size.height()-2*margin;
    double w=size.width()-2*margin;
    for(auto& s : suit_a3_src)
        suit_a3.emplace(s.landscape ? (h*s.cols/w/s.rows) :
                                      w*s.cols/h/s.rows, s);
    for(auto& s : suit_a2_src)
        suit_a2.emplace(s.landscape ? (h*s.cols/w/s.rows) :
                                      w*s.cols/h/s.rows, s);
    for(auto& s : suit_a1_src)
        suit_a1.emplace(s.landscape ? (h*s.cols/w/s.rows) :
                                      w*s.cols/h/s.rows, s);
    for(auto& s : suit_a0_src)
        suit_a0.emplace(s.landscape ? (h*s.cols/w/s.rows) :
                                      w*s.cols/h/s.rows, s);
}

bool PrintoutProducer::printSplitted()
{
    QPrinter pdf;

    pdf.setPrinterName(QString::fromStdString(printer_name));
    pdf.setCreator("KitAssembler");
    pdf.setDocName(QString::fromStdString(print_path));
    pdf.setResolution(dpi);
    if(!pdf.isValid())
        return false;

    pdf.setPageSize(QPageSize(QPageSize::A4));
    pdf.setPageMargins(QMarginsF(margin_split,margin_split,margin_split,margin_split),
                       QPageLayout::Millimeter);

    QImage im_src(QString::fromStdString(image_path));
    if(im_src.isNull())
        throw(std::runtime_error("failed to open " + image_path));
    ImageCRS im_c(region, CRS::EPSG4326, img_width, img_height, epsg);

    // находим угловые точки области на изображении
    Point im_nw = im_c.imgPoint(prn_region.nw, CRS::EPSG4326);
    Point im_se = im_c.imgPoint(prn_region.se, CRS::EPSG4326);
    double aspect=(im_se.x-im_nw.x)/(im_se.y-im_nw.y);
    SuitCaseMap* suit;

    switch(paper_size)
    {
    case PrintFormat::A0:
        suit=&suit_a0;
        break;
    case PrintFormat::A1:
        suit=&suit_a1;
        break;
    case PrintFormat::A2:
        suit=&suit_a2;
        break;
    case PrintFormat::A3:
        suit=&suit_a3;
        break;
    default:
        return false;
    }

    double min_aspect_delta=10,min_suit_key;
    for (auto s : *suit)
        // находим минимальную разницу соотношения сторон изображения
        // карты и склейки, запоминаем aspect как ключ контейнера
        if( fabs(aspect-s.first) < fabs(min_aspect_delta) )
        {
            min_aspect_delta=aspect-s.first;
            min_suit_key=s.first;
        }


    QSizeF a4_size=pdf.pageSizeMM()-QSizeF(2*margin_split,2*margin_split);
    double a4_width=a4_size.width();
    double a4_height=a4_size.height();
    SuitCase sc=suit->at(min_suit_key);
    double w=sc.cols*(sc.landscape ? a4_height : a4_width);
    double h=sc.rows*(sc.landscape ? a4_width : a4_height);

    // где будет минимальное соотношение, та сторона проскейлится
    // во весь размер, другая нет
    double scale_w = dpc*0.1*w/(im_se.x-im_nw.x);
    double scale_h = dpc*0.1*h/(im_se.y-im_nw.y);

    // приращения шагов в пикселях
    double dx,dy;

    // если влезаем в упор по ширине
    if(scale_w < scale_h)
    {
        dx=(im_se.x-im_nw.x)/sc.cols;
        dy=dx * ((sc.landscape) ? a4_width/a4_height : a4_height/a4_width);
    }
    // а если по высоте
    else
    {
        dy=(im_se.y-im_nw.y)/sc.rows;
        dx=dy * ((sc.landscape) ? a4_height/a4_width : a4_width/a4_height);
    }

    pdf.setPageOrientation(sc.landscape ? QPageLayout::Landscape : QPageLayout::Portrait);

    for(int row=0;row<sc.rows;row++)
        for(int col=0;col<sc.cols;col++)
        {
            QRectF win(QPointF(im_nw.x+dx*col,im_nw.y+dy*row).toPoint(),
                       QPointF(std::min(im_nw.x+dx*(col+1),im_se.x),
                               std::min(im_nw.y+dy*(row+1),im_se.y)));
            //            qDebug() << QRect(win.topLeft().toPoint(),
            //                       win.bottomRight().toPoint());
            QImage im_win=im_src.copy(QRect(win.topLeft().toPoint(),
                                            win.bottomRight().toPoint()));
            printArea(&im_win,
            {im_c.geoPoint(win.topLeft().x(), win.topLeft().y(), CRS::EPSG4326),
             im_c.geoPoint(win.bottomRight().x(), win.bottomRight().y(), CRS::EPSG4326)},
                      &pdf,
                      (std::min(scale_h,scale_w)),
                      label_scale.at(page_format.at(paper_size)),
                      GridOn);
        }
    paint.end();
    return true;
}


bool PrintoutProducer::printWhole()
{

    std::shared_ptr<QPagedPaintDevice> pdf;
    if(prn_type == Map4Group)
    {
        pdf.reset(new QPdfWriter(QString::fromStdString(print_path)));
        static_cast<QPdfWriter*>(pdf.get())->setResolution(dpi);
    }
    else
    {
        pdf.reset(new QPrinter);
        QPrinter& pr = *static_cast<QPrinter*>(pdf.get());
        pr.setPrinterName(QString::fromStdString(printer_name));
        pr.setCreator("KitAssembler");
        pr.setDocName(QString::fromStdString(print_path));
        pr.setResolution(dpi);
        if(!pr.isValid())
            return false;
    }

    pdf->setPageSize(QPageSize(page_format.at(paper_size)));
    pdf->setPageMargins(QMarginsF(margin,margin,margin,margin),
                        QPageLayout::Millimeter);

    QImage im_src(QString::fromStdString(image_path));
    if(im_src.isNull())
        throw(std::runtime_error("failed to open " + image_path));
    ImageCRS im_c(region, CRS::EPSG4326, img_width, img_height, epsg);

    // находим угловые точки области на изображении
    Point im_nw = im_c.imgPoint(prn_region.nw, CRS::EPSG4326);
    Point im_se = im_c.imgPoint(prn_region.se, CRS::EPSG4326);
    QPoint p1 = QPoint(im_nw.x, im_nw.y);
    QPoint p2 = QPoint(im_se.x, im_se.y);

    QImage im_win = im_src.copy(QRect(p1, p2));

    ///@todo generalize
    bool is_portrait = im_win.width()<im_win.height();

    pdf->setPageOrientation(is_portrait ? QPageLayout::Portrait :
                                          QPageLayout::Landscape);

    double scale=fitImage2Paper(pdf->pageSizeMM()-QSizeF(2*margin,2*margin),
                                !is_portrait,
                                im_win.width(),
                                im_win.height());

    auto lab_scale = label_scale.at(page_format.at(paper_size));
    printArea(&im_win,
              prn_region,
              pdf.get(),
              scale,
              lab_scale,
              GridOn);

    paint.end();
    return true;
}


void PrintoutProducer::printPoint(const ImageCRS &im_c, const POIPoint &p, double scale, double label_scale)
{
    auto img_pnt=im_c.imgPoint(p, CRS::EPSG4326);
    QPointF xy(img_pnt.x, img_pnt.y);
    double dpcf=static_cast<double>(dpc);
    QTransform tr_temp;
    QString p_name(QString::fromStdString(p.name));
    tr_temp=paint.transform();
    QPointF z(0,0);
    paint.translate(xy);
    // компенсируем переменное увеличение изображения
    // + фиксированный scale для меток
    paint.scale(1.0/label_scale/scale,1.0/label_scale/scale);
    paint.setFont(QFont("sans serif",16));
    // если это точка со значком (по нумерации OziExplorer)
    if(p.type==3)
    {
        // рисуем мишеньку
        paint.setPen(QPen(QBrush(Qt::black),dpcf*0.04));
        paint.setBrush(QBrush(QColor(Qt::white)));
        paint.drawEllipse(QPointF(0,0),dpcf*0.2,dpcf*0.2);

        paint.drawLine(QLineF(QPointF(-dpcf*0.1,0),
                              QPointF(dpcf*0.1,0)));
        paint.drawLine(QLineF(QPointF(0,-dpcf*0.1),
                              QPointF(0,dpcf*0.1)));
        // квадрат с именем точки
        paint.setBrush(QBrush(QColor(Qt::yellow)));
        paint.drawRect(QRectF(QPointF(-dpcf*0.2*p_name.size(),-dpc*1.1),
                              QSizeF(dpcf*0.4*p_name.size(),dpc*0.75)));
        paint.drawText(z-QPointF(p.name.size()*dpcf*0.16,dpcf*0.5),p_name);
    }

    // если это просто метка(на ММБ картах не печатаем
    if(p.type==1 && !skip_poi)
    {
        paint.setPen(QPen(QBrush(Qt::black),dpcf*0.04/scale));
        if(!is_topo)
        {
            //по горизонтали/вертикали
            //for(float an=0;an<2*PI;an+=PI/4)
            //   paint.drawText(z-QPointF(p_name.size()*dpcf*0.16+dpcf*0.04*cos(an),dpcf*0.2+dpcf*0.04*sin(an)),p_name);
            //for(float an=0;an<2*PI;an+=PI/6)
            //    paint.drawText(z-QPointF(p_name.size()*dpcf*0.16+dpcf*0.08*cos(an),dpcf*0.2+dpcf*0.08*sin(an)),p_name);

            paint.setPen(QPen(QBrush(Qt::white),dpcf*0.04/scale));
            paint.drawText(z-QPointF(p_name.size()*dpcf*0.16,dpcf*0.2),p_name);
        }
        else
            paint.drawText(z-QPointF(p_name.size()*dpcf*0.16,dpcf*0.2),p_name);
    }

    paint.setTransform(tr_temp);
}


void PrintoutProducer::printArea(QImage* im, GeoRegion reg,
                                 QPagedPaintDevice *pdf, double scale,
                                 double label_scale,bool print_grid)
{
    ImageCRS im_c(reg, CRS::EPSG4326, im->width(), im->height(), epsg);
    double dpcf=static_cast<double>(dpc);
    double x1,y1,x2,y2,x_len,y_len;
    int zone;
    pdf->newPage();
    if(!paint.isActive())
        paint.begin(pdf);
    paint.resetTransform();
    paint.scale(scale,scale);
    paint.drawImage(QPointF(0,0),*im);
    paint.setClipping(true);
    paint.setClipRect(QRectF(0,0,im->width()-1,im->height()-1));

    // печатаем точки POI


    for(auto& p : wiki_pnt)
    {
        if(p.x < reg.nw.x || p.y > reg.nw.y ||
                p.x > reg.se.x || p.y < reg.se.y)
            continue;
        printPoint(im_c,p,scale,label_scale*1.15);
    }


    // Рисуем сетку
    if(print_grid)
    {
        if(!is_topo)
            paint.setPen(QPen(QBrush(Qt::white),dpcf*0.012/scale));
        else
            paint.setPen(QPen(QBrush(Qt::red),dpcf*0.012/scale));
        UTMPoint u1(reg.nw.toUTM(grid.nw.zone));
        UTMPoint u2(reg.se.toUTM(u1.zone));

        zone=u1.zone;
        x1=u1.x;
        y1=u1.y;
        x2=u2.x;
        y2=u2.y;

        x_len=x2-x1+3*grid_step;
        y_len=y1-y2+2*grid_step;

        x1 -= fmod(x1,grid_step) + grid_step;
        y1 -= fmod(y1,grid_step);
        y1 += grid_step*2;


        for(double x=x1;x<(x1+x_len);x+=grid_step)
        {
            auto p1 = imgPointFromUTM(im_c, {x,y1,zone});
            auto p2 = imgPointFromUTM(im_c, {x,y1-y_len,zone});
            paint.drawLine(QLineF({p1.x,p1.y}, {p2.x, p2.y}));
        }

        for(double y=y1;y>(y1-y_len);y-=grid_step)
        {
            auto p1 = imgPointFromUTM(im_c, {x1,y,zone});
            auto p2 = imgPointFromUTM(im_c, {x1+x_len,y,zone});
            paint.drawLine(QLineF({p1.x,p1.y}, {p2.x, p2.y}));
        }
    }

    // печатаем точки сетки
    for(auto& g : grid_pnt)
    {
        if(g.x < reg.nw.x || g.y > reg.nw.y ||
                g.x > reg.se.x || g.y < reg.se.y)
            continue;
        printPoint(im_c,g,scale,label_scale);
    }

}


float PrintoutProducer::imageAspect()
{
    return static_cast<float>(img_width) /
            static_cast<float>(img_height);
}

double PrintoutProducer::fitImage2Paper(QSizeF paper_size,bool landscape,int32_t width,int32_t height)
{
    double w,h;
    if(!landscape)
    {w=paper_size.width();h=paper_size.height();}
    else
    {w=paper_size.height();h=paper_size.width();}
    w*=dpc/10.0;h*=dpc/10.0;
    double scale=std::min(h/height,
                          w/width);
    return scale;
}

