#include "guru_converter.h"
#include "log.h"
#include "settings.h"
#include <turbojpeg.h>
#include <windows.h>
#include <QDebug>

GuruConverter::GuruConverter(GUI* p_gui, Resources res) :
    ServiceImg2Guru(p_gui, res)
{
}

void GuruConverter::start()
{
    if(GetFileAttributesA(guru_path_out.c_str()) != INVALID_FILE_ATTRIBUTES)
        DeleteFileA(guru_path_out.c_str());

    // если файл по каким то причинам не удалось стереть
    if(GetFileAttributesA(guru_path_out.c_str()) != INVALID_FILE_ATTRIBUTES)
    {
        finishedVerbose(false, GUI::Critical, "Не удалось стереть файл GuruMaps в папке Sources");
        return;
    }

    t=std::thread(&GuruConverter::job,this);
    t.detach();
}

void  GuruConverter::abort()
{
    abort_request=true;
    gui->onProgress(function(), model_ctx, GUI::NoJob, GUI::NoProgress, "Aborting...");
}


/*
  - определить верхний левый тайл в EPSG3785
  - по размеру изображения определить количество тайлов по вертикали и горизонтали
  - создать четное тайлам изображение (канвас) с серым фоном
  - впечатать в канвас изображение карты
  - пройтись по x (внутренний цикл) и y (внешний цикл), наполняя базу данных
*/
void GuruConverter::job()
{
    struct DBWrapper
    {
        sqlite3* h;
        DBWrapper(sqlite3* handle):
            h(handle) {}
        ~DBWrapper()
        {sqlite3_close(h);}
    };

    using namespace cv;
    report(0, "Loading "+image_path_in.substr(image_path_in.rfind('/')));
    Logger::write("Started GuruMaps task ",image_path_in);
    auto im_src = imread(image_path_in, IMREAD_COLOR);
    if(im_src.empty())
    {
        finishedVerbose(false, GUI::Critical, "Не удалось прочитать "+image_path_in);
        return;
    }

    if(zoom>17)
    {
        finishedVerbose(false, GUI::Warning, "Увеличение слоя более z18 недопустимо для sqlitedb формата");
        return;
    }

    sqlite3* db;
    if (sqlite3_open(guru_path_out.c_str(), &db) != SQLITE_OK)
    {
        finishedVerbose(false, GUI::Warning, sqlite3_errmsg(db));
        return;
    }

    report(10, "Creating "+guru_path_out.substr(image_path_in.rfind('/')));
    DBWrapper db_wrapper(db);
    sqlite3_exec(db,"CREATE TABLE info (maxzoom Int, minzoom Int)",nullptr,nullptr,nullptr);
    sqlite3_exec(db,"CREATE TABLE tiles (x int, y int, z int, s int, image blob, PRIMARY KEY (x,y,z,s))",nullptr,nullptr,nullptr);
    sqlite3_exec(db,
                 ("INSERT INTO info (maxzoom,minzoom) VALUES (" +
                  std::to_string(zoomToGuru(zoom-n_layers+1))+ ", " + std::to_string(zoomToGuru(zoom))+
                  ")").c_str(),
                 nullptr,
                 nullptr,
                 nullptr);

    sqlite3_exec(db, "BEGIN", nullptr, nullptr, nullptr);

    Mat im_curr_layer;
    im_src.copyTo(im_curr_layer);
    double scale = 0.5;

    for(int i = 0; i<n_layers; i++)
    {
        report(10 + i*20, "Creating layer "+std::to_string(i));
        auto res = createLayer(im_curr_layer, db, zoomToGuru(zoom-i), i>0 && map_source->is_topo ? true : false);
        if(res.first == false)
        {
            im_src.release();
            im_curr_layer.release();
            finishedVerbose(false, GUI::Warning, res.second);
            return;
        }
        if(!map_source->is_topo)
        {
            im_curr_layer.release();
            resize(im_src, im_curr_layer, Size(), scale, scale,INTER_CUBIC);
            scale *=0.5;
        }
    }

    sqlite3_exec(db, "COMMIT", nullptr, nullptr, nullptr);
    finished(true);
}

std::pair<bool, std::string> GuruConverter::createLayer(cv::Mat &im_src, sqlite3* db, int db_zoom, bool stich_from_tiles)
{
    // заменить эту херню unique_ptr-ом
    struct StatementWrapper
    {
        sqlite3_stmt* h;
        StatementWrapper(sqlite3_stmt* handle):
            h(handle) {}
        ~StatementWrapper()
        {sqlite3_finalize(h);}
    };

    auto map_zoom = zoomFromGuru(db_zoom);
    using namespace cv;
    auto src_width=im_src.cols;
    auto src_height=im_src.rows;
    // DEPRECATED независимо от СК оригинала, СК gurumaps - EPSG3785
    auto map_crs = CRS::crsFromString(map_source->crs);
    Tile tile_nw(region.nw, map_zoom, map_crs);
    Tile tile_se(region.se, map_zoom, map_crs);

    auto n_tiles_y = tile_se.y-tile_nw.y+1;
    auto n_tiles_x = tile_se.x-tile_nw.x+1;
    Mat im_aligned(n_tiles_y*TILE_SIDE,n_tiles_x*TILE_SIDE,CV_8UC3,Scalar(0x80,0x80,0x80));

    std::vector<Tile> border;
    if(stich_from_tiles)
    {
        for(auto x = tile_nw.x; x<= tile_se.x; x++)
            for(auto y = tile_nw.y; y<= tile_se.y; y++)
                border.emplace_back(x, y);
    }
    else
    {
        for(auto x = tile_nw.x; x<= tile_se.x; x++)
        {
            border.emplace_back(x, tile_nw.y);
            border.emplace_back(x, tile_se.y);
        }
        for(auto y = tile_nw.y+1; y <= tile_se.y-1; y++)
        {
            border.emplace_back(tile_nw.x, y);
            border.emplace_back(tile_se.x, y);
        }
    }
    std::string base_path(settings["SASPath"] + '/');
    Mat mtile;
    for(auto& t : border)
    {
        auto path = base_path + map_source->tilePath(t.x, t.y, map_zoom);
        mtile=imread(path, IMREAD_COLOR);
        if(mtile.empty())
            continue;
        Mat win(im_aligned,  { static_cast<int>((t.x-tile_nw.x)*TILE_SIDE),
                               static_cast<int>((t.y-tile_nw.y)*TILE_SIDE),
                               TILE_SIDE,TILE_SIDE });
        mtile.copyTo(win);
        mtile.release();
    }

    if(!stich_from_tiles)
    {
        Mat window(im_aligned,Rect(tile_nw.off_x, tile_nw.off_y,src_width,src_height));
        im_src.copyTo(window);
        window.release();
    }

    Mat im_layer;

    /* если СК не совпадает с EPSG3785, нужно сделать шаблон для новой СК и
       перепроецировать (в данном случае линейно перенести) im_aligned в этот
       шаблон */
    if(map_crs != CRS::EPSG3785)
    {
        TileRegion treg_webm { {region.nw, map_zoom, CRS::EPSG3785},
                            {region.se, map_zoom, CRS::EPSG3785}};
        // тайловая область в СК исходной карты
        TileRegion treg_native {tile_nw,tile_se};

        auto lay_size = Size(treg_webm.imageWidthAligned(), treg_webm.imageHeightAligned());
        n_tiles_x = lay_size.width/TILE_SIDE;
        n_tiles_y = lay_size.height/TILE_SIDE;

        int off_x = static_cast<int>(treg_webm.nw.off_x) - static_cast<int>(treg_native.nw.off_x);
        int off_y = static_cast<int>(treg_webm.nw.off_y) - static_cast<int>(treg_native.nw.off_y);

        auto webm_p1 = Tile(treg_webm.nw.x, treg_webm.nw.y).toGeoPoint(map_zoom, CRS::EPSG3785);
        auto webm_p2 = Tile(treg_webm.se.x+1, treg_webm.se.y+1).toGeoPoint(map_zoom, CRS::EPSG3785);

        auto native_p1 = Tile(treg_webm.nw.x, treg_webm.nw.y).toGeoPoint(map_zoom, CRS::EPSG3785, map_crs);
        auto native_p2 = Tile(treg_webm.se.x+1, treg_webm.se.y+1).toGeoPoint(map_zoom, CRS::EPSG3785, map_crs);

        auto scale_x = (webm_p2.x - webm_p1.x) / (native_p2.x - native_p1.x);
        auto scale_y = (webm_p2.y - webm_p1.y) / (native_p2.y - native_p1.y);

        Mat scaled;
        resize(im_aligned,scaled, Size(), scale_x ,scale_y, INTER_CUBIC);
        im_aligned.release();
        scaled.copyTo(im_aligned);
        scaled.release();
        auto align_size = im_aligned.size();

        Rect roi_aligned, roi_reproj;
        if(off_x >= 0)
        {
            roi_aligned.x = 0;
            roi_reproj.x = off_x;
            roi_aligned.width = std::min<int>(treg_webm.imageWidthAligned()-off_x, align_size.width);
        }
        else
        {
            roi_aligned.x = -off_x;
            roi_reproj.x = 0;
            roi_aligned.width = std::min<int>(align_size.width+off_x, treg_webm.imageWidthAligned());
        }
        roi_reproj.width = roi_aligned.width;

        if(off_y > 0)
        {
            roi_aligned.y = 0;
            roi_reproj.y = off_y;
            roi_aligned.height = std::min<int>(treg_webm.imageHeightAligned()-off_y, align_size.height);
        }
        else
        {
            roi_aligned.y = -off_y;
            roi_reproj.y = 0;
            roi_aligned.height = std::min<int>(align_size.height+off_y, treg_webm.imageHeightAligned());
        }
        roi_reproj.height = roi_aligned.height;

        im_layer.create(lay_size, CV_8UC3);
        im_layer = Scalar(0x80,0x80,0x80);
        Mat win_lay(im_layer, roi_reproj);
        Mat win_aligned(im_aligned, roi_aligned);
        win_aligned.copyTo(win_lay);
        tile_nw = treg_webm.nw;
        tile_se = treg_webm.se;

    }
    else
    {
        im_aligned.copyTo(im_layer);
        im_aligned.release();
    }

    std::unique_ptr<unsigned char[]> tile(new unsigned char[TILE_SIDE*TILE_SIDE*3]);

    int sqlite_err;
    sqlite3_stmt* st;
    if(sqlite3_prepare_v2(db, "INSERT into tiles(x, y, z, s, image) VALUES (?1, ?2, ?3, 0, ?4)", -1, &st, nullptr) != SQLITE_OK)
        return {false, sqlite3_errmsg(db)};
    StatementWrapper stmt_wrapper(st);

    std::unique_ptr<unsigned char[]> compressed_tile(new unsigned char[TILE_SIDE*TILE_SIDE*3*2]);
    auto ztile = compressed_tile.get();
    unsigned long compressed_size;

    for(uint32_t y = 0; y<n_tiles_y; y++)
        for(uint32_t x = 0; x <n_tiles_x; x++)
        {
            for(auto ty=0;ty<TILE_SIDE;ty++)
                memcpy(tile.get()+ty*TILE_SIDE*3,
                       im_layer.ptr(y*TILE_SIDE+ty)+x*TILE_SIDE*3,
                       TILE_SIDE*3);

            auto h = tjInitCompress();
            compressed_size=TILE_SIDE*TILE_SIDE*3;
            auto err = tjCompress2(h,tile.get(),
                                   TILE_SIDE,TILE_SIDE*3,
                                   TILE_SIDE,
                                   TJPF_BGR,
                                   &ztile,
                                   &compressed_size,
                                   TJSAMP_420,
                                   85,
                                   TJFLAG_NOREALLOC);
            if(h==NULL || err)
            {
                return {false, "TurboJpeg error" + std::to_string(err) +
                            " handle=" + std::to_string((size_t)h)};
            }
            tjDestroy(h);

            sqlite_err = sqlite3_bind_int(st, 1, tile_nw.x + x);
            sqlite_err = sqlite3_bind_int(st, 2, tile_nw.y + y);
            sqlite_err = sqlite3_bind_int(st, 3, db_zoom);
            sqlite_err = sqlite3_bind_blob(st, 4, ztile, compressed_size, SQLITE_TRANSIENT);
            if ((sqlite_err = sqlite3_step(st)) != SQLITE_DONE)
            {
                return{false, "SQLITE3 error " + std::to_string(sqlite_err) + ": " + sqlite3_errmsg(db)};
            }
            sqlite3_reset(st);
            if(abortRequested())
            {
                return{false,"Creating sqlitedb aborted"};
            }
        }
    return {true,""};
}
