#ifndef IMAGEENHANCER_H
#define IMAGEENHANCER_H

#include <atomic>
#include <thread>
#include "service.h"

class ImageEnhancer : public ServiceEnhancer
{
public:
    ImageEnhancer(GUI* p_gui, Resources res=Memory);
    void start() override {}
    void abort() override {}
    using ServiceBase::report;
private:
    void job();
    std::atomic<int> abort_request{false};
    std::thread t;

};



#endif // IMAGEENHANCER_H
