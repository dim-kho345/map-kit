#include <QString>
#include <QDir>
#include <set>
#include "download.h"
#include "settings.h"
#include "log.h"


void TileDownloader::abort()
{
    queue.clear();
    abort_req=true;

    // это должен сделать последний worker
    //gui->onProgress(ServiceFunction::DownloadMap, model_ctx, -1, -1, "aborted");
    //finished(false);
}

void TileDownloader::start()
{
    gui->onProgress(function(), model_ctx, queue_ctx, 0, "downloading");

    Tile nw, se;
    Logger::write("Download: obtaining tile coordinates for ",map_source->name);
    try{
        nw = Tile(region.nw, zoom, CRS::crsFromString(map_source->crs));
        se = Tile(region.se, zoom, CRS::crsFromString(map_source->crs));
    }
    catch (std::exception& e){
        Logger::write(e.what());
        gui->onUserMessage(GUI::Critical, src_name + ": " + e.what());
        finished(false);
        return;
    }

    qDebug() << QSslSocket::supportsSsl();

    std::set<QString> tile_folder;


    // строим план скачивания основного увеличения. для СК отличных от ESP3785 скачиваем
    // дополнительный внешний периметр в целях красивого оформления (без серых краев) карт sqlitedb формата
    bool expandBounds = CRS::crsFromString(map_source->crs)!=CRS::EPSG3785;
    // пока отключим эту фичу
    expandBounds = false;

    if(expandBounds)
    {
        nw.x--; nw.y--;
        se.x++; se.y++;
    }
    for(auto x = nw.x; x <= se.x; x++)
        for(auto y = nw.y; y <= se.y; y++)
            queue.emplace_back(x, y, zoom);

    // скачиваем внешний периметр зоны для мЕньшего увеличения, для формирования sqlitedb(GuruMaps)
    if(guru_maps)
    {
        // спутнику нужен только периметр
        if(!map_source->is_topo)
            for(int i=1; i< SQLITEDB_LAYERS; i++)
            {
                auto z = zoom-i;
                nw = Tile(region.nw, z, CRS::crsFromString(map_source->crs));
                se = Tile(region.se, z, CRS::crsFromString(map_source->crs));
                for(auto x = nw.x; x <=se.x; x++)
                {
                    queue.emplace_back(x, nw.y, z);
                    queue.emplace_back(x, se.y, z);
                }
                for(auto y = nw.y+1; y <=se.y-1; y++)
                {
                    queue.emplace_back(nw.x, y, z);
                    queue.emplace_back(se.x, y, z);
                }
            }
        // а вот топо - нижние слои полностью, из-за генерализации
        else
        {
            for(int i=1; i< SQLITEDB_LAYERS; i++)
            {
                auto z = zoom-i;
                nw = Tile(region.nw, z, CRS::crsFromString(map_source->crs));
                se = Tile(region.se, z, CRS::crsFromString(map_source->crs));
                for(auto x = nw.x; x <= se.x; x++)
                    for(auto y = nw.y; y <= se.y; y++)
                        queue.emplace_back(x, y, z);
            }
        }

        /*
        if(expandBounds)
        {
            for(auto x = nw.x; x <=se.x; x++)
            {
                queue.emplace_back(x, nw.y-1, z);
                queue.emplace_back(x, se.y+1, z);
            }
            т.к. по факту СК EPSG3785 и EPSG3395 смещены друг относительно друга только
              по широте, вертикальную часть внешнего периметра качать не надо
              Если подгонят новую СК со смещением и по долготе, включим и этот кусок

            // захватываем углы периметра в т.ч.
            for(auto y = nw.y-1; y <=se.y+1; y++)
            {
                queue.emplace_back(nw.x-1, y, z);
                queue.emplace_back(se.x+1, y, z);
            }
        }*/
    }


    // каталоги для кэша
    for(const auto& t : queue)
    {
        QString f(QString::fromStdString(map_source->tilePath(t.x,t.y,t.zoom)));
        f.truncate(f.lastIndexOf(QChar('/')));
        tile_folder.emplace(f);
    }

    Logger::write("Download: creating paths");
    QDir cache(QString::fromStdString(settings["SASPath"]));
    for(auto&f : tile_folder)
    {
        if(!cache.mkpath(f))
        {
            finishedVerbose(false, GUI::Critical, "Ошибка создания кэша.\nПроверьте SASPath в settings.ini");
            return;
        }
    }
    tile_folder.clear();

    if(queue.empty())
    {
        finished(false);
        return;
    }
    total_tiles = queue.size();
    Logger::write("Download: starting");

    // https handshake, если необходимо
    auto url = map_source->tileUrl(0,0,zoom);
    if(url.at(4) == 's')
    {
        auto pos = url.find('/',8);
        auto str = QString::fromStdString(url.substr(0,pos));
        net_mgr->connectToHostEncrypted(str);
    }

    for(auto i=0; i<4; i++)
    {
        worker[i] = new DownloadWorker(*this);
        ++active_workers;
        // инициируем начало загрузки имитацией ответа пакета
        worker[i]->start();
    }
}

void DownloadWorker::error(QNetworkReply::NetworkError err)
{
    Logger::printf("Download network error: %i, tile x=%i y=%i z=%i ttl %i", err, tile.x, tile.y, tile.zoom, tile.ttl);
    //if(err==QNetworkReply::HostNotFoundError)
    //   fail_code=HostNotFound;
    // попытаться скачать снова
    if(--tile.ttl && !mgr.abort_req)
    {
        mgr.queue.push_back(tile);
    }
    else
    {
        ++mgr.n_missed_tiles;
        mgr.error_code.insert(MissedTile);
    }
    ++mgr.error_count;
}

void DownloadWorker::reqReply()
{
    // обрабатываем ответ, проставляем статусы
    if(reply)
    {
        // если тайл принят нормально, записываем в файл
        if(reply->error()==QNetworkReply::NoError)
        {
            qDebug() << "Recv " << reply->url().toString();
            qDebug() << reply->rawHeader("Content-Type");
            QFile f(path);
            if(!f.open(QFile::WriteOnly))
            {
                mgr.fail_code = WriteTileFiled;
            }
            else
            {
                if(f.write(reply->readAll())==-1)
                {
                    mgr.fail_code=WriteTileFiled;
                }
            }
        }
        reply->close();
        reply->deleteLater();
        reply=nullptr;
    }

    if(mgr.abort_req)
    {
        if(!--mgr.active_workers)
            mgr.finished(false);
        return;
    }

    bool tile_pending=false;
    if(!mgr.fail_code)
    {
        if(mgr.error_count >= mgr.error_limit)
            mgr.fail_code = TooMuchErrors;
        else
        {

            // check queue for missing tiles
            while(!mgr.queue.empty())
            {

                tile=mgr.queue.front();
                mgr.queue.pop_front();
                path =QString::fromStdString(mgr.map_source->tilePath(tile.x,tile.y,tile.zoom));
                path.prepend(QString::fromStdString(settings["SASPath"])+'/');
                if(!QFile::exists(path))
                {
                    tile_pending=true;
                    break;
                }
            }
        }
    }

    mgr.gui->onProgress(ServiceFunction::DownloadMap,
                        mgr.model_ctx,
                        mgr.queue_ctx,
                        100-(mgr.queue.size()*100)/mgr.total_tiles,
                        "downloading");

    // in case of critial error
    if(mgr.fail_code)
    {
        // if this worker is the last active one
        if(!--mgr.active_workers)
            switch (mgr.fail_code)
            {
            case WriteTileFiled:
                mgr.finishedVerbose(false,
                                    GUI::Critical,
                                    "Запись тайла не удалась. Проверьте место на диске");
                break;
            case TooMuchErrors:
                mgr.finishedVerbose(false,
                                    GUI::Warning,
                                    "Превышено кол-во ошибок. Проверьте сеть/доступность/полноту источника карт "+
                                    mgr.src_name);
                break;
            case HostNotFound:
                mgr.finishedVerbose(false,
                                    GUI::Warning,
                                    "Сервер карт для "+ mgr.src_name + " не найден. Проверьте сеть");
                break;
            }
        return;
    }

    // no more tiles left
    if(!tile_pending)
    {

        if(--mgr.active_workers == 0)
        {
            std::vector<std::string> error_text;
            for(auto e : mgr.error_code)
            {
                switch(e)
                {
                case MissedTile:
                    error_text.emplace_back("Не скачано " + std::to_string(mgr.n_missed_tiles) + " тайлов из " +
                                            mgr.src_name);
                }
            }
            std::string res;
            res.reserve(1024);
            for(auto& s : error_text)
                res.append(s + '\n');
            if(res.empty())
                mgr.finished(true);
            else
            {
                // удаляем последний перевод строки
                res.pop_back();
                mgr.finishedVerbose(true, GUI::Warning, res);
            }
        }
    }
    else
    {
        auto url = mgr.map_source->tileUrl(tile.x,tile.y,tile.zoom);
        ///@todo обработать ошибку соединения с rmaps0.exe
        QNetworkRequest req;
        req.setUrl(QString::fromStdString(url));
        req.setHeader(QNetworkRequest::UserAgentHeader,
                      QString("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)"));
        req.setRawHeader(QByteArray("Accept"), QByteArray("*/*"));
        req.setRawHeader(QByteArray("Accept-Language"), QByteArray("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"));
        reply=mgr.net_mgr->get(req);
        connect(reply,&QNetworkReply::finished,this,&DownloadWorker::reqReply);
        connect(reply,QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),
                this, &DownloadWorker::error);
    }
}

std::shared_ptr<QNetworkAccessManager> TileDownloader::net_mgr;

