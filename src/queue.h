﻿#ifndef QUEUE
#define QUEUE
#include <memory>
#include <map>
#include "service.h"
#include "serv_func.h"
#include "model/model.h"
#include "service/factory.h"


/**
 @page Очередь сервисов
 TODO
 Сейчас не ставим сервис в очередь, если выполняется севрис над той же картой например, или над той же папкой
 Но это не правильно. Если в запросе та же карта, нужно отменить running сервис. Вообще нужно как то уйти от
 рекурсивных постановок задач в очередь. Нужно уйти от инвалидации итераторов, которые получаются, когда доступ
 к очереди получается из цикла. Можно так: пройти очеред, но не запускать ничего, а найти подходящую задачу
 для запуска. выйдя из цикла, запустить ее. Если сама задача запускает просмотр очереди/выбор кандидата на
 исполнение, итераторы не должны инвалидироваться во время прохода цикла.
 А обращение к сервисам из цикла не должно менять очередь. Например, abort во время прохода по циклу. abort,
 как и finished, не должна вызывать диспетчер, это не ее задача и проблема. Выполнили проход, а потом только
 Чувстую будет головная боль.
*/



class ServiceQueue
{
public:
    ServiceQueue(ModelReactor* model, GUI* gui) :
        model(model), gui(gui) {}

    enum AbortFilterId{ZipMap, ZipAll, PrintoutMap, PrintoutAll, ZipGarmin, ConvMap, EnhMap};

    /**
        Ensure that this is not the dublicate of existing request enqueued
        Run queue dispatcher
        Dublicates are evaluated analyzing either the same <map source/job upon it> pair,
        or the same job without map source
     */
    void enqueueService(int model_id, std::unique_ptr<ServiceBase> service);
    /// abort services selected by one of pre-defined criteria
    void abortServices(AbortFilterId fid, int map_id);
    void abortService(int id);
    void abortServiceByMap(int map_id);
    void abortServiceByFunction(ServiceFunction func);
    void abortServiceByFolder(int folder_id);
    void abortServiceByPrint(int print_id);

    virtual void serviceFinished(ServiceFunction func, int ctx, bool complete);
private:
    struct QueueItem
    {
        bool deleted{false};
        bool is_running{false};
        std::unique_ptr<ServiceBase> service;
        QueueItem (std::unique_ptr<ServiceBase>&& serv) :
            service(std::move(serv))
        {}
    };

    bool abortFilter(AbortFilterId f, const QueueItem& it);

    /// Ensure there are free resources for at least one request in queue and run it
    void queueDispather();

    /// every service is associated with it's unique never repeated id
    int generateQueueId() {
        return ++last_ctx;
    }

    std::map<int,QueueItem> queued;
    int last_ctx{0};
    int free_resources{ServiceBase::Memory};
    ModelReactor* const model;
    GUI* const gui;
};


#endif // QUEUE

