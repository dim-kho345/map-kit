﻿#ifndef MODEL_H
#define MODEL_H

#include "model/model.h"
#include "queue.h"
#include "service/map_supplier.h"
#include "path_resolver.h"


class ModelImpl : public ModelInterface, public ModelReactor
{

public:

    ModelImpl(GUI* pgui, ServiceQueue& queue);
    // TODO remove "request" prefix
    //requests from GUI
    void requestDownload(MapSource * const src, int zoom, bool guru_maps) override;
    void requestLoadMap(std::string name, std::string map_path, bool is_topo, int zoom) override;
    void requestStitchTiles(int map_id) override;
    void requestConvert2Ozf(int map_id) override;
    void requestConvert2Kmz(int map_id) override;
    void requestConvert2Jnx(int map_id) override;
    void requestConvert2Guru(int map_id) override;
    void requestEnhanceImage(int map_id) override;
    void requestWikiPnt() override;
    void requestPrintOut(int print_id, std::string printer_name) override;

    /// copy&rename sources across target kit folder. call zip process
    void requestZipFolder(int folder_id) override;

    void createUploadScript() override;

    void setRegion(const GeoRegion reg) override;
    void setLocation(const std::string&loc) override;
    void setHQPoint(const GeoPoint, std::string place=std::string()) override;
    void setGrid(UTMRegion reg, unsigned step, GridOrigin origin) override;
    void setWikiPnt(const std::vector<POIPoint>& pnt) override;
    void deleteMap(int map_id) override;

    void setPrintRegion(const GeoRegion reg) override;
    void setEnhanceParam(int map_id, EnhanceOptions opts) override;
    void setMap4Grp(const GeoRegion reg, int map_id) override;
    void setPrintOptions(int print_id, PrintOptions opt) override;

    void setJnxMap(int map_id) override;
    void setKmzMap(int map_id) override;
    void setGuruTopo(int map_id) override;
    void setGuruSat(int map_id) override;

    /// forward abort request from GUI to service queue
    void abortService(int queue_id)override;


    /// get map sources avaialable
    std::vector<MapSource*> enumerateMapSources() override;
    ModelState currentState() override;

    // callbacks from Services
    void onTilesReady(int map_id, bool complete) override;
    void onImageReady(int map_id, bool complete, unsigned w, unsigned h, unsigned icon_w, unsigned icon_h) override;
    void onImageReady(std::string name, bool is_topo, int zoom,
                      unsigned w, unsigned h,
                      unsigned icon_w, unsigned icon_h,
                      const std::string crs) override;
    void onOzfReady(int map_id, bool complete) override;
    void onKmzReady(int map_id, bool complete) override;
    void onJnxReady(int map_id, bool complete) override;
    void onGuruReady(int map_id, bool complete) override;
    void onEnhanceReady(int map_id) override {}
    void onGridReady(std::vector<POIPoint> grid_points) override;
    void onWikiPntReady() override;
    void onZipFolderReady(int folder_id, bool complete) override;

private:
    int generateId() {return ++last_generated_id;}
    void assignFolder(int map_id);
    void assignPrint(int map_id);
    /// assign Jnx and/or Kmz sources to map_id if not assigned already to some other source
    void weakAssignJnxKmz(int map_id);

    /// create Ozi MAP header for source image and for Ozf2 version
    void createOzfHeader(int map_id);
    /// map and it's model id
    std::map<int, ModelMap> map;
    std::vector<std::unique_ptr<MapSource>> custom_map_source;
    /// folder and it's model id
    std::map<int, ModelFolder> folder;
    /// printout and it's model id
    std::map<int, ModelPrint> print;
    std::vector<POIPoint> wiki_pnt;
    std::vector<POIPoint> grid_pnt;

    int jnx_source{NoSource};
    int kmz_source{NoSource};
    int guru_topo_source{NoSource};
    int guru_sat_source{NoSource};
    bool wikipnt_ready{false};
    bool grid_ready{false};
    bool coord2_ready{false};
    GeoRegion region;
    UTMRegion grid;
    unsigned grid_step{0};
    /// grid origin - letter and number (А1 <=> {0,0})
    GridOrigin grid_origin;
    GeoRegion print_bounds_4grp;
    int map4grp_src;
    GeoRegion print_bounds;
    std::string location;
    time_t kit_date;
    bool wipe_target_folders{false};
    auto findFolder(int map_id);
    auto findPrint(int map_id);
    void deleteFolder(decltype(folder)::iterator folder_it);
    void deletePrint(decltype (print)::iterator print_it);
    int last_generated_id{0};

    GUI* gui;
    ServiceQueue& service_queue;
    MapSupplier map_supplier;
    char current_dir[256];
    friend struct SetPrintOption;
};


#endif //MODEL_H
