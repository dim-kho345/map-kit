﻿#include <algorithm>
#include <chrono>
#include "model_impl.h"
#include "log.h"
#include "service.h"
#include "settings.h"
#include "service/factory.h"
#include "ozi.h"
#include "portable/util_port.h"

ModelImpl::ModelImpl(GUI* pgui, ServiceQueue& queue)  :
    gui(pgui),
    service_queue(queue)
{
    Logger::write("Checking settings");

    if(settings["GoogleVersion"].empty())
        settings.setValue("GoogleVersion","865");
    auto icon_side = settings["IconSideSize"];
    if(std::stoul(icon_side) > 4096 || std::stoul(icon_side)<512)
        settings.setValue("IconSideSize","2048");

    if(settings["SASPath"].empty())
    {
        settings.setValue("SASPath","<путь к кэшу SASPlanet>");
        throw(std::logic_error("Путь к кешу SASPlanet не прописан в settings.ini"));
    }
    if(settings["AssemblyFolder"].empty())
    {
        settings.setValue("AssemblyFolder","<пусть к каталогу карт>");
        throw(std::logic_error("Путь к каталогу карт не прописан в settings.ini"));
    }

    try {
        settings["FTPServer"];
    } catch (std::out_of_range) {
        settings.setValue("FTPServer","maps.lizaalert.ru:21");
        settings.setValue("FTPUser","user");
        settings.setValue("FTPPassword","password");
        settings.setValue("FTPFolder","maps");
    }

    Logger::write("Settings checked");


    PathResolver::setSourceFolder(settings["AssemblyFolder"]+"/Sources");
    auto now = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
    kit_date = now.count();
    folder.emplace(generateId(),ModelFolder{ModelFolder::Points,
                                            ModelFolder::NonMapFolderId,
                                            "3-Points"});
    folder.emplace(generateId(),ModelFolder{ModelFolder::Other,
                                            ModelFolder::NonMapFolderId,
                                            "8-Other"});
    folder.emplace(generateId(),ModelFolder{ModelFolder::Map4Print,
                                            ModelFolder::NonMapFolderId,
                                            "9-Map_4_print"});
    folder.emplace(generateId(),ModelFolder{ModelFolder::Garmin,
                                            ModelFolder::NonMapFolderId,
                                            "Garmin"});
    folder.emplace(generateId(),ModelFolder{ModelFolder::Tracks,
                                            ModelFolder::NonMapFolderId,
                                            "10-Tracks"});
    gui->onModelUpdated(GUI::UpdatedFolders);
}



ModelInterface::ModelState ModelImpl::currentState()
{
    PathResolver path(kit_date, location, grid_step);
    std::map<int,ModelMap4GUI> maps4gui;

    // преобразовать ModelMap в ModelMap4GUI, попутно присвоив пути мини-изображений карт
    std::transform(map.cbegin(),
                   map.cend(),
                   std::inserter(maps4gui,maps4gui.end()),
                   [&path](auto& model_map)->auto {
        auto& mmap = model_map.second;
        ModelMap4GUI m(mmap);
        m.icon_path=path.mapSourcePath(PathResolver::Icon,
                                       mmap.source,
                                       mmap.zoom);
        return std::make_pair(model_map.first,m);
    });


        return {maps4gui, folder, print, jnx_source, kmz_source, guru_topo_source, guru_sat_source,
                    wikipnt_ready, grid_ready, grid_step,coord2_ready, region, print_bounds_4grp,
                    print_bounds, location, kit_date};
}


void ModelImpl::requestDownload(MapSource* const src, int zoom, bool guru_maps)
{

    if(!region)
        return;
    ModelMap mmap(src, zoom);
    decltype (generateId()) map_id;

    // Make sure combination src/zoom not already present
    auto it=std::find_if(map.cbegin(),map.cend(),
                         [&mmap](const decltype (map)::value_type& val)->bool {
        return mmap == val.second;
    });

    if(it!=map.cend())
        deleteMap(it->first);

    //if(it==map.cend())
    //{
    mmap.src_type=ModelMap::TiledSource;
    map_id=generateId();
    map.emplace(map_id, mmap);
    gui->onModelUpdated(GUI::UpdatedMaps);
    //}


    auto srv = ServiceFactory::getService<ServiceDownloadMap>(
                ServiceFunction::DownloadMap, gui);
    srv->map_source=src;
    srv->zoom=zoom;
    srv->region=region;
    srv->guru_maps=guru_maps;
    srv->src_name = mmap.nameFull();
    service_queue.enqueueService(map_id, std::move(srv));
}


void ModelImpl::requestLoadMap(std::string name, std::string map_path, bool is_topo, int zoom)
{
    custom_map_source.emplace_back(std::make_unique<MapSourceCustom>(name, std::vector<int>{zoom}, is_topo, "EPSG3785"));
    auto msrc=custom_map_source.back().get();
    ModelMap mmap(msrc, zoom);
    auto it = std::find_if(map.cbegin(), map.cend(), [&mmap](const auto& m)->bool {
        return mmap==m.second;
    });
    if (it!=map.cend())
        deleteMap(it->first);

    mmap.src_type=ModelMap::ImageSource;
    auto map_id=generateId();
    map.emplace(map_id, mmap);

    auto srv = ServiceFactory::getService<ServiceLoadMap>(
                ServiceFunction::LoadMap, gui);
    PathResolver path(kit_date, location, grid_step);
    srv->map_path = map_path;
    srv->map_name = name;
    srv->kit_region = region;
    srv->icon_side_size=std::stoul(settings["IconSideSize"]);
    srv->icon_path_out=path.mapSourcePath(PathResolver::Icon, msrc, zoom);
    srv->img_path_out=path.mapSourcePath(PathResolver::Image, msrc, zoom);
    gui->onModelUpdated(GUI::UpdatedMaps);
    service_queue.enqueueService(map_id,std::move(srv));
}

void ModelImpl::requestStitchTiles(int map_id)
{
    ModelMap& m= map.at(map_id);
    PathResolver path(kit_date, location);
    if(m.src_type == ModelMap::ImageSource)
        return;

    auto srv = ServiceFactory::getService<ServiceStitcher>(
                ServiceFunction::Stitch, gui);

    srv->map_source=m.source;
    srv->zoom=m.zoom;
    srv->region=region;
    srv->output_format=ServiceStitcher::bmp;
    srv->image_path_out=path.mapSourcePath(path.Image, m.source, m.zoom);
    srv->icon_path_out= path.mapSourcePath(path.Icon, m.source, m.zoom);
    srv->icon_side_size=std::stoul(settings["IconSideSize"]);
    service_queue.enqueueService(map_id,std::move(srv));
}

void ModelImpl::requestConvert2Ozf(int map_id)
{
    auto srv = ServiceFactory::getService<ServiceImg2Ozf>(
                ServiceFunction::Conv2Ozf, gui);
    PathResolver path(kit_date, location, grid_step);
    ModelMap& m=map.at(map_id);
    if(!m.img_ready)
        return;
    srv->image_path_in=path.mapSourcePath(PathResolver::Image, m.source, m.zoom);
    srv->image_path_out=path.mapSourcePath(PathResolver::Ozf, m.source, m.zoom);
    m.ozf_ready=false;
    gui->onModelUpdated(GUI::UpdatedMaps);
    service_queue.enqueueService(map_id,std::move(srv));

}

void ModelImpl::requestConvert2Jnx(int map_id)
{
    auto srv = ServiceFactory::getService<ServiceImg2Jnx>(
                ServiceFunction::Conv2Jnx, gui);

    PathResolver path(kit_date, location, grid_step);
    ModelMap& m= map.at(map_id);
    if(!m.img_ready)
        return;
    srv->location_name = location;
    srv->region = region;
    srv->map_path_in=path.mapSourcePath(path.Map, m.source, m.zoom);
    srv->image_path_out=path.mapSourcePath(path.Jnx, m.source, m.zoom);
    m.jnx_ready=false;
    gui->onModelUpdated(GUI::UpdatedMaps);
    service_queue.enqueueService(map_id,std::move(srv));
}


void ModelImpl::requestConvert2Kmz(int map_id)
{
    auto srv = ServiceFactory::getService<ServiceImg2Kmz>(
                ServiceFunction::Conv2Kmz, gui);

    PathResolver path(kit_date, location, grid_step);
    ModelMap& m= map.at(map_id);
    if(!m.img_ready)
        return;
    srv->location_name = location;
    srv->region = region;
    srv->map_path_in=path.mapSourcePath(path.Map, m.source, m.zoom);
    srv->kmz_path_out=path.mapSourcePath(path.Kmz, m.source, m.zoom);
    m.kmz_ready=false;
    gui->onModelUpdated(GUI::UpdatedMaps);
    service_queue.enqueueService(map_id,std::move(srv));
}

void ModelImpl::requestConvert2Guru(int map_id)
{
    auto srv = ServiceFactory::getService<ServiceImg2Guru>(
                ServiceFunction::Conv2Guru, gui);

    PathResolver path(kit_date, location, grid_step);
    ModelMap& m= map.at(map_id);
    if(!m.img_ready)
        return;
    srv->location_name = location;
    srv->region = region;
    srv->image_path_in=path.mapSourcePath(path.Image, m.source, m.zoom);
    srv->guru_path_out=path.mapSourcePath(path.Guru, m.source, m.zoom);
    srv->zoom = m.zoom;
    srv->map_source=m.source;
    m.guru_ready=false;
    gui->onModelUpdated(GUI::UpdatedMaps);
    service_queue.enqueueService(map_id,std::move(srv));
}

void ModelImpl::requestEnhanceImage(int map_id)
{

}


void ModelImpl::setPrintRegion(const GeoRegion reg)
{
    print_bounds=reg;
    gui->onModelUpdated(GUI::UpdatedCommon);
}
void ModelImpl::setEnhanceParam(int map_id, EnhanceOptions opts)
{
    map.at(map_id).enhance_opts = opts;
}

void ModelImpl::setMap4Grp(const GeoRegion reg, int map_id)
{
    auto it = std::find_if(print.begin(), print.end(), [](const auto& prn)->bool{
        if(prn.second.type==PrintType::Map4Group)
            return true;
        return false;
    });

    if(map_id==-1)
    {
        map4grp_src = NoSource;
        print_bounds_4grp=GeoRegion();
        if(it!=print.end())
            print.erase(it);
    }
    else
    {
        if(map.find(map_id)==map.cend())
        {
            Logger::printf("Non existing map4grp ID:%i region %s",map_id,region ? "exist" : "empty");
            return;
        }
        print_bounds_4grp = reg;
        map4grp_src = map_id;

        if(it==print.cend())
        {
            print.insert({generateId(), ModelPrint(map_id, PrintFormat::A4, PrintType::Map4Group)});
        }
        else
            it->second.map_id=map_id;
    }
    gui->onModelUpdated(GUI::UpdatedCommon | GUI::UpdatedPrintout);
}

void ModelImpl::requestWikiPnt()
{
    auto srv = ServiceFactory::getService<ServiceWiki2Wpt>(
                ServiceFunction::Wiki2Wpt, gui);
    service_queue.enqueueService(-1,std::move(srv));
    srv->region = region;
    srv->grid_step = grid_step;
    service_queue.enqueueService(NoSource,std::move(srv));
}


void ModelImpl::requestPrintOut(int print_id, std::string printer_name)
{
    auto srv = ServiceFactory::getService<ServicePrintout>(
                ServiceFunction::Printout, gui);
    PathResolver path(kit_date, location, grid_step);
    auto& prn = print.at(print_id);
    auto& m = map.at(prn.map_id);
    srv->grid = grid;
    srv->grid_step = grid_step;
    srv->wiki_pnt = wiki_pnt;
    srv->grid_pnt = grid_pnt;
    srv->region = region;
    srv->paper_size = prn.format;
    srv->prn_type = prn.type;
    srv->prn_region = prn.type==PrintType::Map4Group ? print_bounds_4grp : print_bounds;
    srv->printer_name = printer_name;
    srv->img_width = m.img_width;
    srv->img_height = m.img_height;
    srv->is_topo = m.source->is_topo;
    srv->skip_poi = m.source->name == "MMB";
    srv->epsg = CRS::crsFromString(m.source->crs);
    srv->image_path = PathResolver::mapSourcePath(PathResolver::Image, m.source, m.zoom);
    if(prn.type == Map4Group)
        srv->print_path = PathResolver::sourcePath(PathResolver::Map4Grp);
    else
        srv->print_path = path.mapDestinationFilename(PathResolver::Printout,
                                                      m.source,
                                                      m.zoom,
                                                      prn.format,
                                                      prn.type);
    service_queue.enqueueService(print_id,std::move(srv));
}

void ModelImpl::requestZipFolder(int folder_id)
{
    auto srv = ServiceFactory::getService<ServiceZip>(
                ServiceFunction::ZipFolder, gui);

    auto& f = folder.at(folder_id);

    ///@todo сделать validate функцию вместо флага
    //if(!f.ready_to_zip)
    //    return;
    PathResolver path(kit_date, location, grid_step);
    // ключевое место - рассовываем файлы по каталогам.

    srv->folder = f.name;
    srv->zip_path = f.name + ".zip";
    srv->kit_folder = path.kitFolder();

    if(!grid_ready)
    {
        gui->onUserMessage(GUI::Critical, "Сетка не определена");
        return;
    }
    if(!wikipnt_ready)
    {
        gui->onUserMessage(GUI::Warning, "Wiki точки отсутствуют");
    }

    if(f.type==ModelFolder::Map)
    {
        auto& m = map.at(f.map_id);
        auto* msrc = m.source;
        srv->file_copy.emplace_back(path.mapSourcePath(PathResolver::Ozf, msrc, m.zoom),
                                    path.mapDestinationPath(PathResolver::Ozf, f.name+"/Maps",msrc, m.zoom));
        /** @todo эта операция подменяет файл изображение в map файле, в каталоге исходника
            т.о. map больше не указывает на исходное изображение, т.е становится недействительным
            можно конечно генерить исходный map взад, но как то некрасиво.
            как вариант генерить сразу в каталог карты, конечно.. т.е. без file_copy
            но это non-consistent
        */

        OziMapHeader::writeHeader(region,{m.img_width, m.img_height},
                                  msrc->is_topo,
                                  grid.nw.zone == GeoCalc::utmZone(region.nw) ?
                                      grid_step :
                                      OziMapHeader::TurnOffGrid,
                                  path.mapDestinationFilename(PathResolver::Ozf,msrc,m.zoom),
                                  path.mapSourcePath(PathResolver::OzfMap,msrc,m.zoom));

        srv->file_copy.emplace_back(path.mapSourcePath(PathResolver::OzfMap, msrc, m.zoom),
                                    path.mapDestinationPath(PathResolver::OzfMap, f.name+"/Maps", msrc, m.zoom));


        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridGpx),
                                    path.destinationPath(PathResolver::GridGpx, f.name + "/Data"));
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridKml),
                                    path.destinationPath(PathResolver::GridKml, f.name + "/Data"));
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridDmx),
                                    path.destinationPath(PathResolver::GridDmx, f.name + "/Data"));
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridPlt),
                                    path.destinationPath(PathResolver::GridPlt, f.name + "/Data"));
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridWpt),
                                    path.destinationPath(PathResolver::GridWpt, f.name + "/Data"));
        srv->file_copy.emplace_back(path.sourcePath(msrc->is_topo ?
                                                        PathResolver::WikiPntTopo :
                                                        PathResolver::WikiPntSat),
                                    path.destinationPath(msrc->is_topo ?
                                                                PathResolver::WikiPntTopo :
                                                                PathResolver::WikiPntSat,
                                                                f.name + "/Data"));
    }
    if(f.type == ModelFolder::Points)
    {
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridGpx),
                                    path.destinationPath(PathResolver::GridGpx,f.name + "/Garmin/GPX"));
    }

    if(f.type == ModelFolder::Tracks)
    {
    }

    if(f.type == ModelFolder::Map4Print)
    {
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::Map4Grp),
                                    path.destinationPath(PathResolver::Map4Grp,f.name));
        srv->no_zip=true;
    }

    if(f.type == ModelFolder::Other)
    {
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::Hlg),
                                    path.destinationPath(PathResolver::Hlg,""));
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::Readme),
                                    path.destinationPath(PathResolver::Readme,""));
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::Coord2Txt),
                                    path.destinationPath(PathResolver::Coord2Txt,""));


        if(guru_sat_source != NoSource || guru_topo_source != NoSource)
            srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridKml),
                                        path.destinationPath(PathResolver::GridKml, f.name + "/GuruMaps"));

        if(guru_sat_source != NoSource)
        {
            auto m_guru = map.at(guru_sat_source);
            if(m_guru.guru_ready)
            {
                auto m_src = m_guru.source;
                srv->file_copy.emplace_back(path.sourcePath(PathResolver::WikiPntGpx),
                                            path.destinationPath(PathResolver::WikiPntGpx, f.name + "/GuruMaps"));
                srv->file_copy.emplace_back(path.sourcePath(PathResolver::WikiPntGpi),
                                            path.destinationPath(PathResolver::WikiPntGpi, f.name + "/POI"));
                srv->file_copy.emplace_back(path.mapSourcePath(path.Guru, m_src, m_guru.zoom),
                                            path.mapDestinationPath(PathResolver::Guru, f.name + "/GuruMaps", m_src, m_guru.zoom));
            }
        }

        if(guru_topo_source != NoSource)
        {
            auto m_guru = map.at(guru_topo_source);
            if(m_guru.guru_ready)
            {
                auto m_src = m_guru.source;
                srv->file_copy.emplace_back(path.sourcePath(PathResolver::WikiPntGpx),
                                            path.destinationPath(PathResolver::WikiPntGpx, f.name + "/GuruMaps"));
                srv->file_copy.emplace_back(path.sourcePath(PathResolver::WikiPntGpi),
                                            path.destinationPath(PathResolver::WikiPntGpi, f.name + "/POI"));
                srv->file_copy.emplace_back(path.mapSourcePath(path.Guru, m_src, m_guru.zoom),
                                            path.mapDestinationPath(PathResolver::Guru, f.name + "/GuruMaps", m_src, m_guru.zoom));
            }
        }
    }

    if(f.type == ModelFolder::Garmin)
    {
        if(jnx_source==NoSource)
        {
            gui->onUserMessage(GUI::Critical, "Источник JNX не определен");
            return;
        }
        if(kmz_source==NoSource)
        {
            gui->onUserMessage(GUI::Critical, "Источник KMZ не определен");
            return;
        }

        if(!map.at(kmz_source).kmz_ready || !map.at(jnx_source).jnx_ready)
        {
            gui->onUserMessage(GUI::Critical, "Файл JNX/KMZ не готов");
            return;
        }

        auto& m_jnx = map.at(jnx_source);
        auto& m_kmz = map.at(kmz_source);
        auto m_src = m_jnx.source;
        srv->file_copy.emplace_back(path.sourcePath(PathResolver::GridGpx, m_jnx.zoom),
                                    path.destinationPath(PathResolver::GridGpx, f.name + "/GPX"));
        srv->file_copy.emplace_back(path.mapSourcePath(PathResolver::Jnx, m_src, m_jnx.zoom),
                                    path.mapDestinationPath(PathResolver::Jnx, f.name + "/BirdsEye", m_src, m_jnx.zoom));
        m_src = m_kmz.source;
        srv->file_copy.emplace_back(path.mapSourcePath(path.Kmz, m_src, m_kmz.zoom),
                                    path.mapDestinationPath(PathResolver::Kmz, f.name + "/CustomMaps", m_src, m_kmz.zoom));
        srv->zip_path = "4-Garmin.zip";
    }

    service_queue.enqueueService(folder_id,std::move(srv));
    folder[folder_id].zipped=false;
    gui->onModelUpdated(GUI::UpdatedFolders);
}

void ModelImpl::setRegion(const GeoRegion reg)
{
    region=reg;
}
void ModelImpl::setLocation(const std::string& loc)
{
    location=loc;
}

void ModelImpl::createUploadScript()
{
    PathResolver path(kit_date, location);
    std::ofstream script(path.destinationPath(PathResolver::UploadScript,""));
    std::vector<std::string> name;
    for (auto& f : folder)
    {
        if(f.second.name=="Garmin")
        {
            name.push_back("4-Garmin.zip");
            continue;
        }
        if(f.second.name=="9-Map_4_print" || f.second.name=="10-Tracks")
            continue;
        name.push_back(f.second.name+".zip");
    }

    name.push_back(path.destinationFilename(PathResolver::Hlg));
    name.push_back(path.destinationFilename(PathResolver::Readme));
    name.push_back(path.destinationFilename(PathResolver::Coord2Txt));

    for(const auto& n : name)
    {
        script << "curl --ftp-create-dirs -T \"" << n << "\" -u " ;
        script << settings.value("FTPUser") << ':' << settings.value("FTPPassword") << ' ';
        script << settings.value("FTPServer") << '/' <<  settings.value("FTPFolder") << '/';
        script << path.kitFolder() << "/\n";
    }

    gui->onUserMessage(GUI::Info, "Скрипт создан");
}

void ModelImpl::setHQPoint(const GeoPoint p, std::string place)
{
    PathResolver path(kit_date, location);
    auto srv = ServiceFactory::getService<ServiceGenerator2Coord>(ServiceFunction::Gen2Coords, gui);
    srv->geopoint = p;
    srv->location_name = "";
    srv->path = path.sourcePath(PathResolver::Coord2Txt);
    srv->start();
    coord2_ready=true;
    gui->onModelUpdated(GUI::UpdatedCommon);
}

void ModelImpl::setGrid(UTMRegion reg, unsigned int step, GridOrigin origin)
{
    PathResolver path(kit_date, location, step);
    grid = reg;
    grid_step = step;
    grid_origin = origin;
    if(!grid)
    {
        grid_ready = false;
    }
    else
    {
        auto srv = ServiceFactory::getService<ServiceGeneratorGrid>(ServiceFunction::GenGrid, gui);
        srv->step = grid_step;
        srv->region = reg;
        srv->kit_date = kit_date;
        srv->offset_x = grid_origin.first;
        srv->offset_y = grid_origin.second;
        srv->wpt_path = path.sourcePath(PathResolver::GridWpt);
        srv->plt_path = path.sourcePath(PathResolver::GridPlt);
        srv->kml_path = path.sourcePath(PathResolver::GridKml);
        srv->gpx_path = path.sourcePath(PathResolver::GridGpx);
        srv->dmx_path = path.sourcePath(PathResolver::GridDmx);
        srv->start();
        grid_pnt = srv->grid;
        grid_ready=true;
    }
    gui->onModelUpdated(GUI::UpdatedCommon);
}

void ModelImpl::setWikiPnt(const std::vector<POIPoint>& pnt)
{
    wiki_pnt=pnt;
    wikipnt_ready=true;
    gui->onModelUpdated(GUI::UpdatedCommon);
}

void ModelImpl::setJnxMap(int map_id)
{
    if (map.at(map_id).img_ready)
        if(jnx_source != map_id && ! map.at(map_id).source->is_topo)
        {
            service_queue.abortServiceByFunction(ServiceFunction::Conv2Jnx);
            if(map.at(map_id).jnx_ready)
                jnx_source=map_id;
            else
            {
                jnx_source = map_id;
                requestConvert2Jnx(map_id);
            }
            gui->onModelUpdated(GUI::UpdatedMaps);
        }
}

void ModelImpl::setKmzMap(int map_id)
{
    if (map.at(map_id).img_ready)
        if(kmz_source != map_id && map.at(map_id).source->is_topo )
        {
            service_queue.abortServiceByFunction(ServiceFunction::Conv2Kmz);
            if(map.at(map_id).kmz_ready)
                kmz_source=map_id;
            else
            {
                kmz_source = map_id;
                requestConvert2Kmz(map_id);
            }
            gui->onModelUpdated(GUI::UpdatedMaps);
        }
}

void ModelImpl::setGuruTopo(int map_id)
{
    if (map.at(map_id).img_ready)
        if(guru_topo_source != map_id && map.at(map_id).source->is_topo )
        {
            service_queue.abortServiceByFunction(ServiceFunction::Conv2Guru);
            if(map.at(map_id).guru_ready)
                guru_topo_source=map_id;
            else
            {
                guru_topo_source = map_id;
                requestConvert2Guru(map_id);
            }
            gui->onModelUpdated(GUI::UpdatedMaps);
        }
}

void ModelImpl::setGuruSat(int map_id)
{
    if (map.at(map_id).img_ready)
        if(guru_sat_source != map_id && !map.at(map_id).source->is_topo )
        {
            service_queue.abortServiceByFunction(ServiceFunction::Conv2Guru);
            if(map.at(map_id).guru_ready)
                guru_sat_source=map_id;
            else
            {
                guru_sat_source = map_id;
                requestConvert2Guru(map_id);
            }
            gui->onModelUpdated(GUI::UpdatedMaps);
        }
}

auto ModelImpl::findFolder(int map_id)
{
    auto folder_it = std::find_if(folder.begin(), folder.end(),
                                  [map_id](auto& val)->bool
    {
        if(val.second.map_id == map_id)
            return true;
        return false;
    });
    return folder_it;
}

auto ModelImpl::findPrint(int map_id)
{
    auto print_it = std::find_if(print.begin(), print.end(),
                                 [map_id](auto& val)->bool
    {
        if(val.second.map_id == map_id)
            return true;
        return false;
    });
    return print_it;
}

struct SetPrintOption
{
    ModelPrint& prn;
    SetPrintOption(ModelPrint& p) :
        prn(p) {}
    void operator()(PrintType t) {prn.type = t;}
    void operator()(PrintFormat f) {
        if (prn.type!=Map4Group)
            prn.format = f;
    }
};



void ModelImpl::setPrintOptions(int print_id, PrintOptions opt)
{
    auto& prn = print.at(print_id);
    for(auto& o : opt)
        std::visit(SetPrintOption(prn),o);

    gui->onModelUpdated(GUI::UpdatedPrintout);
}

void ModelImpl::onTilesReady(int map_id, bool complete)
{
    if(!complete)
    {
        deleteMap(map_id);
    }
    else
        if(map.find(map_id)!=map.end())
        {
            map.at(map_id).tiles_ready=true;
            if(map.at(map_id).source->name == "Wiki")
                return;
            requestStitchTiles(map_id);
        }
}


void ModelImpl::onImageReady(int map_id, bool complete, unsigned w, unsigned h,
                             unsigned icon_w, unsigned icon_h)
{
    if(!complete)
    {
        deleteMap(map_id);
    }
    else
        if(map.find(map_id)!=map.end())
        {
            auto& m = map.at(map_id);
            m.img_width = w;
            m.img_height = h;
            m.icon_width = icon_w;
            m.icon_height = icon_h;
            if(jnx_source == map_id)
                jnx_source=NoSource;
            if(kmz_source == map_id)
                kmz_source=NoSource;

            createOzfHeader(map_id);
            map.at(map_id).img_ready=true;
            requestConvert2Ozf(map_id);

        }
    gui->onModelUpdated(GUI::UpdatedMaps);
}

void ModelImpl::onImageReady(std::string name, bool is_topo,
                             int zoom, unsigned w, unsigned h,
                             unsigned icon_w, unsigned icon_h,
                             const std::string crs="EPSG3785")
{
    //custom_map_source.emplace_back(name, std::vector<int>{zoom}, is_topo, crs);
    ModelMap mm(custom_map_source.back().get(), zoom);
    mm.img_width = w;
    mm.img_height = h;
    mm.icon_width = icon_w;
    mm.icon_height = icon_h;
    mm.src_type=ModelMap::ImageSource;
    auto map_id=generateId();
    map.emplace(map_id,mm);
    createOzfHeader(map_id);
    gui->onModelUpdated(GUI::UpdatedMaps);
    assignFolder(map_id);
    requestConvert2Ozf(map_id);
}

void ModelImpl::onOzfReady(int map_id, bool complete)
{
    if(!complete)
    {
        deleteMap(map_id);
    }
    else
    {
        if(map.find(map_id)!=map.end())
        {
            map.at(map_id).ozf_ready=true;
            assignFolder(map_id);
            assignPrint(map_id);
        }
    }
    gui->onModelUpdated(GUI::UpdatedMaps);
}

void ModelImpl::onJnxReady(int map_id, bool complete)
{
    if(complete)
        if(map.find(map_id)!=map.end())
            map.at(map_id).jnx_ready=true;
    gui->onModelUpdated(GUI::UpdatedMaps);
}

void ModelImpl::onKmzReady(int map_id, bool complete)
{
    if(complete)
        if(map.find(map_id)!=map.end())
            map.at(map_id).kmz_ready=true;
    gui->onModelUpdated(GUI::UpdatedMaps);
}

void ModelImpl::onGuruReady(int map_id, bool complete)
{
    if(complete)
        if(map.find(map_id)!=map.end())
            map.at(map_id).guru_ready=true;
    gui->onModelUpdated(GUI::UpdatedMaps);

}


void ModelImpl::onWikiPntReady()
{
    wikipnt_ready=true;
}

void ModelImpl::onGridReady(std::vector<POIPoint> grid_points)
{
    grid_pnt = grid_points;
    grid_ready=true;
}

void ModelImpl::onZipFolderReady(int folder_id, bool complete)
{
    if(folder.find(folder_id)==folder.cend())
        return;
    if(!complete)
    {
        auto name = folder.at(folder_id).name;
        gui->onUserMessage(GUI::Warning, "Упаковка папки " + name + " не завершена");
        return;
    }

    folder[folder_id].zipped=true;
    gui->onModelUpdated(GUI::UpdatedFolders);
}

void ModelImpl::deleteMap(int map_id)
{
    auto map_it = map.find(map_id);
    if( map_it!= map.end() )
    {
        service_queue.abortServiceByMap(map_id);
        map.erase(map_id);
    }

    auto folder_it = findFolder(map_id);
    if(folder_it!=folder.end())
        deleteFolder(folder_it);

    // распечаток к карте может быть привязано несколько, нужно удалить их все
    decltype (print)::iterator print_it;
    while ((print_it=findPrint(map_id)) != print.cend())
    {
        deletePrint(print_it);
    }


    // TODO check for map[map_id] to exist
    if(jnx_source==map_id)
        jnx_source = NoSource;
    if(kmz_source==map_id)
        kmz_source = NoSource;
    gui->onModelUpdated(GUI::UpdatedAll);
}

void ModelImpl::deleteFolder(decltype(folder)::iterator folder_it)
{
    if(folder_it == folder.end())
        return;
    service_queue.abortServiceByFolder(folder_it->first);
    folder.erase(folder_it);
}

void ModelImpl::deletePrint(decltype (print)::iterator print_it)
{
    if(print_it == print.end())
        return;
    service_queue.abortServiceByPrint(print_it->first);
    print.erase(print_it);
}


void ModelImpl::weakAssignJnxKmz(int map_id)
{
    auto src = map.at(map_id).source;
    if(src->is_topo && kmz_source==NoSource)
        kmz_source=map_id;
    if(!src->is_topo && jnx_source==NoSource)
        jnx_source=map_id;
}

void ModelImpl::assignFolder(int map_id)
{
    std::string name;
    if(findFolder(map_id) != folder.cend())
        return;
    auto m = map.at(map_id);
    auto m_src=m.source;
    if(m_src->is_topo)
    {
        if(m_src->name=="MMB")
            name="7";
        else
            name="5";

        name.append("-Ozi(Win&Android)_Topo_"+m_src->name);

    }
    else
    {
        name="6";
        name.append("-Ozi(Win&Android)_Satell_"+m_src->name);

    }
    folder.emplace(generateId(),ModelFolder{ModelFolder::Map,
                                            map_id,
                                            name,
                                            false});
    gui->onModelUpdated(GUI::UpdatedFolders);
}

void ModelImpl::assignPrint(int map_id)
{
    std::string name;
    if(findPrint(map_id) != print.cend())
        return;
    auto m = map.at(map_id);

    ModelPrint prn(map_id, A0, Whole);
    print.emplace(generateId(), ModelPrint (map_id, A0, Whole));
    print.emplace(generateId(), ModelPrint (map_id, A0, Splitted));
    gui->onModelUpdated(GUI::UpdatedPrintout);
}

void ModelImpl::abortService(int queue_id)
{
    service_queue.abortService(queue_id);
}

std::vector<MapSource*> ModelImpl::enumerateMapSources()
{
    return map_supplier.availableMaps();
}




void ModelImpl::createOzfHeader(int map_id)
{
    auto& mmap = map.at(map_id);
    auto msrc = mmap.source;
    PathResolver path(kit_date,location,grid_step);
    OziMapHeader::writeHeader(region,{mmap.img_width, mmap.img_height},
                              msrc->is_topo,
                              grid_step,
                              path.mapSourcePath(PathResolver::Image,msrc,mmap.zoom),
                              path.mapSourcePath(PathResolver::Map,msrc,mmap.zoom));
    OziMapHeader::writeHeader(region,{mmap.img_width, mmap.img_height},
                              msrc->is_topo,
                              grid_step,
                              path.mapSourcePath(PathResolver::Ozf,msrc,mmap.zoom),
                              path.mapSourcePath(PathResolver::OzfMap,msrc,mmap.zoom));
}
