﻿#ifndef MODELINTERFACE_H
#define MODELINTERFACE_H
#include <string>
#include <map>
#include "map_source.h"
#include "geo.h"
#include "print_options.h"

struct EnhanceOptions
{
    float gamma{0};
    float clahe{0};
};

class ModelMap
{
public:
    enum SourceType {TiledSource, ImageSource};
    /// subscript enumerator

    ModelMap(MapSource* src, int zoom=0) :
        source(src), zoom(zoom) {}
    MapSource* source;

    /// 4 GUI
    std::string nameFull();


    EnhanceOptions enhance_opts;
    bool tiles_ready{false};
    bool img_ready{false};
    bool ozf_ready{false};
    bool kmz_ready{false};
    bool jnx_ready{false};
    bool guru_ready{false};
    bool enhanced{false};
    SourceType src_type;
    /// zoom level
    const int zoom;
    bool operator==(const ModelMap& rhs) {
        return *source==*rhs.source && zoom==rhs.zoom;
    }
    const std::string path;
    unsigned img_height;
    unsigned img_width;
    unsigned icon_height;
    unsigned icon_width;
};


struct ModelFolder
{
    enum Type {
        Map,
        Points,
        Garmin,
        Map4Print,
        Other,
        Tracks
    };
    Type type;
    static constexpr int NonMapFolderId=-1;
    int map_id;
    std::string name;
    bool zipped{false};
    bool ready_to_zip{false};
};


struct ModelPrint
{
    ModelPrint()= default;
    ModelPrint(int map_id, PrintFormat format, PrintType type) :
        map_id(map_id), format(format), type(type) {}
    int map_id;
    PrintFormat format;
    PrintType type;
};


class ModelInterface
{
public:
    virtual ~ModelInterface() {}
    static constexpr int NoSource{-1};
    struct ModelMap4GUI : ModelMap
    {
        ModelMap4GUI(const ModelMap& mm) :
            ModelMap(mm) {}

        std::string icon_path;
    };

    struct ModelState
    {
        std::map<int, ModelMap4GUI> map;
        std::map<int, ModelFolder> folder;
        std::map<int, ModelPrint> print;
        int jnx_source;
        int kmz_source;
        int guru_topo_source;
        int guru_sat_source;
        bool wikipnt_ready;
        bool grid_ready;
        unsigned grid_step;
        bool coord2_ready;
        GeoRegion region;
        GeoRegion map4grp;
        GeoRegion print_area;
        std::string location;
        time_t kit_date;

    };

    using GridOrigin = std::pair<unsigned, unsigned>;

    /// @todo remove "request" prefix
    
    virtual void requestDownload(MapSource * const src, int zoom, bool guru_maps) =0;
    virtual void requestLoadMap(std::string name, std::string map_path, bool is_topo, int zoom) =0;
    virtual void requestStitchTiles(int map_id) =0;
    virtual void requestConvert2Ozf(int map_id) =0;
    virtual void requestConvert2Kmz(int map_id) =0;
    virtual void requestConvert2Jnx(int map_id) =0;
    virtual void requestConvert2Guru(int map_id) =0;
    virtual void requestEnhanceImage(int map_id) =0;
    virtual void requestWikiPnt() =0;
    virtual void requestPrintOut(int print_id, std::string printer_name) =0;

    /// Распределяет и переименовывает исходники по каталогам. Далее вызывается просто
    virtual void requestZipFolder(int folder_id) =0;
    virtual void createUploadScript() =0;

    virtual void setRegion(const GeoRegion reg) =0;
    virtual void setPrintRegion(const GeoRegion reg) =0;
    virtual void setEnhanceParam(int map_id, EnhanceOptions opts) =0;
    virtual void setMap4Grp(const GeoRegion reg, int map_id) =0;
    virtual void setPrintOptions(int print_id, PrintOptions) =0;
    virtual void setLocation(const std::string&loc) =0;
    virtual void setHQPoint(const GeoPoint, std::string place=std::string()) =0;
    virtual void setGrid(UTMRegion reg, unsigned step, GridOrigin origin) =0;
    virtual void setWikiPnt(const std::vector<POIPoint>& pnt) =0;
    virtual void deleteMap(int map_id) =0;

    virtual void setJnxMap(int map_id) =0;
    virtual void setKmzMap(int map_id) =0;
    virtual void setGuruTopo(int map_id) =0;
    virtual void setGuruSat(int map_id) =0;
    /// forward abort request from GUI to service queue
    virtual void abortService(int queue_id) =0;


    /// get map sources avaialable
    virtual std::vector<MapSource*> enumerateMapSources() =0;
    virtual ModelState currentState()=0;

};


class ModelReactor
{
public:
    virtual ~ModelReactor() {}
    // callbacks from Services
    virtual void onTilesReady(int map_id, bool complete)=0;
    virtual void onImageReady(int map_id, bool complete, unsigned w, unsigned h,
                              unsigned icon_w, unsigned icon_h)=0;
    virtual void onImageReady(std::string name, bool is_topo, int zoom,
                         unsigned w, unsigned h,
                         unsigned icon_w, unsigned icon_h,
                         const std::string crs)=0;
    virtual void onOzfReady(int map_id, bool complete)=0;
    virtual void onKmzReady(int map_id, bool complete)=0;
    virtual void onJnxReady(int map_id, bool complete)=0;
    virtual void onGuruReady(int map_id, bool complete)=0;
    virtual void onEnhanceReady(int map_id)=0;
    virtual void onGridReady(std::vector<POIPoint>)=0;
    virtual void onWikiPntReady()=0;
    virtual void onZipFolderReady(int folder_id, bool complete)=0;

};

#endif // MODELINTERFACE_H
