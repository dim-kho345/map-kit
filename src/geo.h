#ifndef GEO_H
#define GEO_H
#include <limits>
#include <cmath>
#include "crs.h"
#include "config.h"

constexpr static double R{6378137};
constexpr static double PI{M_PI};
double constexpr double_min = std::numeric_limits<double>().min();



struct GeoPoint;

struct Tile
{
    Tile() = default;
    Tile(unsigned x, unsigned y)  :
        x(x), y(y), off_x(0), off_y(0) {}
    Tile(unsigned x, unsigned y, unsigned off_x, unsigned off_y) :
        x(x), y(y), off_x(off_x), off_y(off_y) {}
    Tile(const GeoPoint pnt, int zoom, CRS::CoordRefSys crs);
    GeoPoint toGeoPoint(int zoom, CRS::CoordRefSys tile_crs, CRS::CoordRefSys dst_crs) const;
    GeoPoint toGeoPoint(int zoom, CRS::CoordRefSys tile_crs) const;
    /// преобразовать в тайл в другой СК
    Tile reproject(int zoom, CRS::CoordRefSys tile_crs, CRS::CoordRefSys dst_crs) const;
    std::pair<int, int> offset(const Tile& rhs) const;
    int raw_x() const;
    int raw_y() const;
    unsigned int x;
    unsigned int y;
    unsigned int off_x;
    unsigned int off_y;
};


struct GeoPoint
{
    GeoPoint(const std::pair<double,double> rhs) :
        x(rhs.first), y(rhs.second) {}
    GeoPoint() = default;
    GeoPoint(double x,double y) :
        x(x), y(y){}

    double x{0};
    double y{0};

    operator bool() const {return fabs(x)>double_min || fabs(y)>double_min;}
    bool operator==(const GeoPoint& rhs) const;

    /// @brief convert to UTM point
    /// @param force_zone force use UTM zone, autocalc if -1
    struct UTMPoint toUTM(int force_zone=-1) const;
};

// Alias helper
struct LLPoint
{
    LLPoint(const GeoPoint& p) :
        lon(p.x), lat(p.y) {}
    const double& lon;
    const double& lat;
};

struct UTMPoint
{
    double x{0},y{0};
    int zone;
    operator bool() const {return fabs(x)>double_min || fabs(y)>double_min;}
    //operator GeoPoint() const;
};


struct TileRegion
{
    Tile nw;
    Tile se;
    unsigned imageWidth();
    unsigned imageHeight();
    unsigned imageWidthAligned();
    unsigned imageHeightAligned();
};

struct GeoRegion
{
    GeoPoint nw;
    GeoPoint se;
    operator bool() const {return nw || se;}
};

struct UTMRegion
{
    UTMPoint nw;
    UTMPoint se;
    operator bool() const {return nw || se;}
};

struct GeoCalc
{
    ///@todo преобразователи координат в string перенести к интересантам, здесь им нечего делать, как и toDegMin/toDegMinSec
    static std::string toDegMinString(const GeoPoint&);
    static std::string toDegMinSecString(const GeoPoint&);
    static void toDegMin(double coord, int &deg, double& min);
    static void toDegMinSec(double coord, int& deg, int &min, double& sec);
    static double distance(GeoPoint, GeoPoint);
    static int utmZone(const GeoPoint &);
};


struct POIPoint : public GeoPoint
{
    POIPoint() = default;
    POIPoint(std::string name, int type, const GeoPoint& p) :
        GeoPoint(p),
        name(name),
        type(type) {
    }
    std::string name;
    /// OziExplorer type
    int type;
};


#endif // GEO_H

