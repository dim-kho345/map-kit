#ifndef SAS_PLANET_H
#define SAS_PLANET_H

#include "tiles_storage.h"
#include "settings.h"


class SASPlanetTiles : public TileStorage
{
public:
    SASPlanetTiles(MapSource* map, int zoom, std::string cache_path, std::string extension);
    bool save(unsigned x, unsigned y, const char *data, int len) override;
    bool load(unsigned x, unsigned y, char* data, size_t* len) override;

    bool exists(unsigned x, unsigned y) override;
    /// absolute tile path in cache
    std::string tilePath(unsigned x, unsigned y);
private:
    /// Directory path in SAS tiles cache, ex. d:/SAS/cache/sat, d:/SAS/cache/yasat
    std::string cache_path;
    /// file extension for map source - ".png", ".jpg", ...
    std::string ext;

};

#endif // SAS_PLANET_H
