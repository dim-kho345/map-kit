#ifndef TILESTORAGEFACTORY_H
#define TILESTORAGEFACTORY_H

#include <memory>
#include "tiles_storage.h"

class TileStorageFactory
{
public:
    enum StorageType {
        SASPlanet,
        Sqlite
    };
    std::unique_ptr<TileStorage> getTileStorage(StorageType type);

};

#endif // TILESTORAGEFACTORY_H
