#ifndef TILESTORAGE_H
#define TILESTORAGE_H
#include "map_source.h"
#include "config.h"

class TileStorage
{
public:
    TileStorage(MapSource* map,  unsigned zoom) :
        map(map), zoom(zoom)
    {}
    /// save encoded image
    virtual bool save(unsigned x, unsigned y, const char *data, int len) =0;
    /// load undecoded image file or blob
    virtual bool load(unsigned x, unsigned y, char* data, size_t* len) =0;
    /// check for presence
    virtual bool exists(unsigned x, unsigned y) =0;
protected:
    const MapSource* const map;
    const unsigned zoom;
};

#endif // TILESTORAGE_H
