#include "factory.h"
#include "sas_planet.h"

std::unique_ptr<TileStorage> TileStorageFactory::getTileStorage(StorageType type)
{
    if(type==SASPlanet)
        return std::unique_ptr<TileStorage>(new SASPlanetTiles(nullptr,4,"",""));
}
