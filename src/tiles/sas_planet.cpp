#include <fstream>
#include <QtCore>
#include "sas_planet.h"


SASPlanetTiles::SASPlanetTiles(MapSource* map, int zoom, std::string cache_path, std::string content_type) :
    TileStorage(map,zoom),
    cache_path(cache_path), ext('.' + content_type)
{
}

bool SASPlanetTiles::load(unsigned x, unsigned y, char* data, size_t* len)
{

}

bool SASPlanetTiles::save(unsigned x, unsigned y, const char* data, int len)
{
    using namespace std;
    auto tile_path = tilePath(x,y);
    QDir dir(QString::fromStdString(cache_path));
    if(!dir.mkpath(QString::fromStdString(tile_path.substr(0,tile_path.rfind('/')))))
        return false;

    ofstream ftile(cache_path + '/' + tile_path, ios_base::binary);
    auto& str = ftile.write(data, len);
    return (str.bad()) ? false : true;
}

bool SASPlanetTiles::exists(unsigned x, unsigned y)
{
    auto path = cache_path + '/' + tilePath(x,y);
    return QFile::exists(QString::fromStdString(path));
}


std::string SASPlanetTiles::tilePath(unsigned int x, unsigned int y)
{
    char str[160];
    std::string path;

        snprintf(str, sizeof(str),
                 "z%i/%i/x%i/%i/y%i",
                 zoom+1,
                 x/1024,
                 x,
                 y/1024,
                 y);
        path=str;
        path.append(ext);
    return path;
}
