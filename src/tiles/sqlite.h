#ifndef SQLITETILES_H
#define SQLITETILES_H

#include "tiles_storage.h"

class SqliteTiles : public TileStorage
{
public:
    SqliteTiles(MapSource* map,  unsigned zoom);

};

#endif // SQLITETILES_H
