#include <memory>
#include "proxy/factory.h"
#include "container/local_model.h"


std::shared_ptr<ProxyFactory::Model> ProxyFactory::getModel(GUI* pgui)
{
    return std::make_shared<LocalModel>(pgui);
}
