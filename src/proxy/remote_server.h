#ifndef REMOTEMODELSERVER_H
#define REMOTEMODELSERVER_H

#include "gui/gui.h"
#include "model/model.h"

class RemoteModelServer : GUI
{
public:
    RemoteModelServer(ModelInterface* pmodel);
private:
    ModelInterface* model;
};

#endif // REMOTEMODELSERVER_H
