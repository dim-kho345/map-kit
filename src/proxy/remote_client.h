#ifndef REMOTEMODELCLIENT_H
#define REMOTEMODELCLIENT_H

#include "model/model.h"
#include "gui/gui.h"

class RemoteModelClient :  public ModelInterface
{
public:
    RemoteModelClient(GUI* pgui);

    void requestDownload(MapSource * const src, int zoom, bool guru_maps) override {}
    void requestLoadMap(std::string name, std::string map_path, bool is_topo, int zoom) override {}
    void requestStitchTiles(int map_id) override {}
    void requestConvert2Ozf(int map_id) override{}
    void requestConvert2Kmz(int map_id) override{}
    void requestConvert2Jnx(int map_id) override{}
    void requestEnhanceImage(int map_id)override{}
    void requestWikiPnt() override{}
    void requestPrintOut(int print_id, std::string printer_name) override{}

    /// Распределяет и переименовывает исходники по каталогам. Далее вызывается просто
    void requestZipFolder(int folder_id) override{}

    void setRegion(const GeoRegion reg) override{}
    void setLocation(const std::string&loc) override{}
    void setGrid(UTMRegion reg, unsigned step, GridOrigin origin) override{}
    void setWikiPnt(const std::vector<POIPoint>& pnt) override{}
    void deleteMap(int map_id) override{}

    /// forward abort request from GUI to service queue
    void abortService(int queue_id) override{}


    /// get map sources avaialable
    std::vector<MapSource*> enumerateMapSources() override{ return {};}
    ModelState currentState() override{return ModelState();}


private:
    GUI* gui;
};

#endif // REMOTEMODELCLIENT_H
