#ifndef PROXYFACTORY_H
#define PROXYFACTORY_H

#include "model/model.h"
#include "gui/gui.h"

class ProxyFactory
{
public:
    using Model = ModelInterface;
    static std::shared_ptr<Model> getModel(GUI* pgui);
    GUI* getGUI(Model* pgui);
};

#endif // PROXYFACTORY_H
