#ifndef SERV_FUNC_H
#define SERV_FUNC_H

#include <map>

/**
@page threads Threads

No service is done at GUI context, model is intented to

Service - Thread/location

DownLoadMap - Model/server
LoadMap - dedicated/server
LoadWpt - model/client, send points/grid to Model
CoordFile - model/client, send point/location to Model
Stitch - dedicated/server
Conv2... - dedicated/server
Enhance - dedicated/server
Printout - dedicated/server
Wiki2Wpt - fork model, put points to Model
ZipFolder - dedicated/server
GenGrid - Model/client
*/


enum ServiceFunction{
    /// Скачать карту в кэш SASPlanet.
    DownloadMap,

    /// Склеить карту из кэша SASPlanet в каталог settings[AssemblyFolder]. Одновременно дать фиксированный буст
    /// получившемуся изображению для спутника для Ozi.
    Stitch,

    /// Конвертировать исходник в ozf. Эта и другие конвертации осуществляются в каталоге settings[AssemblyFolder],
    /// там же хранятся исходники
    Conv2Ozf,
    Conv2Jnx,
    Conv2Kmz,
    Conv2Guru,

    /// Преобразовать с заданными параметрами улучшения. используется также GUI в его личной песочнице, отдельный
    /// процесс о котором очередь сервисов не знает (потому что он занимает мало памяти)
    Enhance,


    Printout,

    /// Скопировать из settings[AssemblyFolder] все необходимые файлы для указанного каталога и запаковать его
    /// Нецелесообразно, я думаю после работы каждого сервиса копировать файлы по каталогам. Пусть эта функция
    /// сама за это отвечает, ей это надо.
    ZipFolder,

    UploadZip,

    /// Загрузка и обработка WPT файла осуществляется напрямую без очереди сервисов. А результат - точку и/или
    /// сетку предлагается потом присвоить комплекту через запрос модели
    LoadWpt,

    /// Обратный геокодинг OSM бы запилить тут для сохранения 10-15 секунд.
    CoordFile,

    /// Синхронный запуск асинхроного сервиса. Запустили и забыли. Ответка - через IModel. Необходимые файлы
    /// образуются в каталоге сборки (settings[AssemblyFolder]). Копируются по каталогам ф-цией ZipFolder
    Wiki2Wpt,

    /// Принадлежит GUI. Вызывает отдельное окно, которое в свою очередь вызывает обратный геокодинг - область,
    /// район, нп по координатам. Сначала ориентируемся по точкам, потом ищем города в мультиполигонах
    Gen2Coords,

    /// генерация сетки; ответка ввиде вектора точек и файлов сетки.
    GenGrid,

    /// Указание файла в GUI, он передает в сервис .MAP. Сервис обрезает и возврашщает результат в виде path
    /// изображения и MapSource (или нет..)
    LoadMap
};

/*
 Что-то вроде таблицы инвалидации. окончание сервиса инвалидирует зависимые от него
enum ServiceFunction{
    DownloadMap ->
    Stitch -> конвертация, расширение, печать, упаковка map_id
    Conv2Ozf -> упаковка map_id
    Conv2Jnx -> упаковка map_id, garmin
    Conv2Kmz -> упаковка map_id, garmin
    Enhance -> печать map_id
    Printout ->
    ZipFolder ->
    LoadWpt ->  упаковка все
    CoordFile ->
    Wiki2Wpt -> упаковка все
    Gen2Coords ->
    GenGrid -> упаковка все, печать все
    LoadMap
};
*/


/// function is associated with operation on map
inline bool isMapAssociated(ServiceFunction f)
{
    /// @todo вообще то не должно зависить от положения энумератора
    if (f < ZipFolder)
        return true;
    else
        return false;
}
/// function is associated with operation on folder
inline bool isFolderAssociated(ServiceFunction f)
{
    if (f == ZipFolder || f == UploadZip)
        return true;
    else
        return false;
}


inline bool isPrintAssociated(ServiceFunction f)
{
    if (f == Printout)
        return true;
    else
        return false;
}

const std::string servFuncName(ServiceFunction func);
#endif // SERV_FUNC_H

