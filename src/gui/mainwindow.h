﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <array>
#include "dialog_download.h"
#include "ui_mainwindow.h"
#include "proxy/factory.h"
#include "gui_adapter.h"
#include "map_navigator.h"
#include "print_options.h"

class MainWindow : public QMainWindow, Ui_MainWindow, GUI
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
private:
    using Model = ModelInterface;
    DownloadMapDialog* dlg_download;
    GUIAdapter* gui_adapter;
    MapNavigator* navigator;
    void onModelUpdated(int what) override;
    void onProgress(ServiceFunction func, int model_id, int job_id, int progress, std::string message="") override;
    void onUserMessage(UserMessageType, std::string) override;
    const std::array<int, 6> maps_column_width{250,45,40,40,90,90};
    const std::array<int, 7> prints_column_width{100,25,25,25,25,25,25};
private slots:
    void mapCellClicked(int, int);
    void openHLG();
    void openMap();
    void openWpt();
    void settingsDialog();
    void deleteMap();
    void convert2Jnx();
    void convert2Kmz();
    void convert2Ozf();
    void convert2Guru();
    void enhanceImage();
    void launchWiki2Wpt();
    void zipFolders();
    void setPrintSizeA0();
    void setPrintSizeA1();
    void setPrintSizeA2();
    void setPrintSizeA3();
    void setPrinter();
    void startPrint();
    void excludeSplited();
    void includeSplited();
    void makeCurlScript();
private:
    std::shared_ptr<Model> model;
    void setSelectedPrintFormat(PrintFormat f);
    /// find table item in first row for map/folder id if exist,
    ///  otherwise return nullptr
    QTableWidgetItem *tableItem(QTableWidget* table, int id);
    const QString grid_avail_text[2] {"<font color=darkred>Сетка</font>",
                                      "<font color=green>Сетка</font>"};

    const QString wiki_avail_text[2] {"<font color=darkred>Wiki точки</font>",
                                      "<font color=green>Wiki точки</font>"};

    const QString coord2_avail_text[2] {"<font color=darkred>Координаты</font>",

                                        "<font color=green>Координаты</font>"};

    const QString map4grp_avail_text[2] {"<font color=darkred>Карта для групп</font>",
                                         "<font color=green>Карта для групп</font>"};

    const QString print_area_text[2] {"<font color=darkred>Область печати</font>",
                                      "<font color=green>Область печати</font>"};

    const QString jnx_avail_text[2] {"<font color=darkred>JNX спутник</font>",
                                     "<font color=green>JNX спутник</font>"};

    const QString kmz_avail_text[2] {"<font color=darkred>KMZ Топо</font>",
                                     "<font color=green>KMZ Топо</font>"};
    std::map<PrintFormat, std::string> print_fmt{{A0,"A0"},{A1,"A1"},{A2,"A2"},{A3,"A3"}};
};


#endif // MAINWINDOW_H
