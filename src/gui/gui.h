#ifndef GUIINTERFACE_H
#define GUIINTERFACE_H

#include <string>
#include "serv_func.h"

class GUI
{
public:
    virtual ~GUI() {}
    enum WhatUpdated{
        UpdatedMaps=0x1,
        UpdatedFolders=0x2,
        UpdatedCommon=0x4,
        UpdatedPrintout=0x8,
        UpdatedAll=0xf
    };
    enum UserMessageType {
        Info,
        ///@todo предупреждения должны появляться в ленте а не строке сообщений, отдельное окошко завести
        Warning,
        Critical
    };

    virtual void onModelUpdated(int what)=0;

    static constexpr int NoProgress{-1};
    static constexpr int NoJob{-1};

    /// Let service job identify itself, report progress and provide abort ID for service to call with abort()
    /// @todo поле job_id фактически не используется, но возможно в будущем идентификатор job в ServiceQueue пригодится
    virtual void onProgress(ServiceFunction func, int model_id, int job_id, int progress, std::string message="")=0;
    virtual void onUserMessage(UserMessageType, std::string) =0;
};

#endif // GUIINTERFACE_H
