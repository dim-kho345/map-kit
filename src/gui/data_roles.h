#ifndef DATA_ROLES_H
#define DATA_ROLES_H

// custom data roles
namespace Qt
{
    enum ItemDataRoleCustom
    {
        /// Data for progress bar (0-100)
        ProgressRole = 0x101,
        /// Message for progress bar (0-100)
        ProgressMessageRole = 0x102,
        /// Identification of corresponding map/folder/print in model p.s. "Context" maybe not the best fit term
        ContextRole = 0x103,
        /// Identification of corresponding service queue id
        JobContextRole = 0x104
    };
}

#endif // DATA_ROLES_H
