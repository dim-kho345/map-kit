#ifndef GUIADAPTER_H
#define GUIADAPTER_H

#include <QObject>
#include <QEvent>
#include "gui/gui.h"

class GUIAdapter : public QObject, public GUI
{
    Q_OBJECT
public:
    GUIAdapter(GUI* gui_interface) :
        QObject(dynamic_cast<QObject*>(gui_interface)),
        gui(gui_interface) {

    }

    void onModelUpdated(int what) override;
    /// Let service job identify itself, report progress and provide abort ID for service to call with abort()
    void onProgress(ServiceFunction,
                            int model_id,
                            int job_id,
                            int progress,
                            std::string message="") override;
    void onUserMessage(UserMessageType type, std::string what) override;
private:
    GUI* gui;
private slots:
    bool event(QEvent *event) override;
};

#endif // GUIADAPTER_H
