﻿#include <QFileDialog>
#include <QPrintDialog>
#include <QPrinter>
#include <QMessageBox>
#include <array>
#include "mainwindow.h"
#include "dialog_settings.h"
#include "dialog_import_map.h"
#include "table_maps.h"
#include "data_roles.h"
#include "path_resolver.h"
#include "service/hlg_reader.h"
#include "util.h"
#include "proxy/factory.h"
#include "map_loader.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    gui_adapter = new GUIAdapter(this);
    model = ProxyFactory::getModel(static_cast<GUI*>(gui_adapter));

    dlg_download = new DownloadMapDialog(this, Qt::WindowFlags(),model.get());
    setupUi(this);
    show();
    //showMaximized();
    for(unsigned i=0; i<maps_column_width.size(); i++)
        tb_maps->setColumnWidth(static_cast<int>(i),maps_column_width[i]);
    for(unsigned i=0; i<prints_column_width.size(); i++)
        tb_printout->setColumnWidth(static_cast<int>(i),prints_column_width[i]);
    tb_maps->horizontalHeader()->setFont(QFont("Arial",8));
    tb_maps->setItemDelegate(new TableMapsDelegate);
    navigator = new MapNavigator(model.get());
    tab_main->insertTab(1,navigator,"Карты");
    connect(b_download_map, &QPushButton::clicked, dlg_download, &DownloadMapDialog::openDialog);
    connect(b_delete_map, &QPushButton::clicked, this, &MainWindow::deleteMap);
    connect(b_conv2jnx, &QPushButton::clicked, this, &MainWindow::convert2Jnx);
    connect(b_conv2kmz, &QPushButton::clicked, this, &MainWindow::convert2Kmz);
    connect(b_conv2ozf, &QPushButton::clicked, this, &MainWindow::convert2Ozf);
    connect(b_conv2sqlitedb, &QPushButton::clicked, this, &MainWindow::convert2Guru);
    connect(b_load_map, &QPushButton::clicked, this, &MainWindow::openMap);
    connect(b_A0, &QPushButton::clicked, this, &MainWindow::setPrintSizeA0);
    connect(b_A1, &QPushButton::clicked, this, &MainWindow::setPrintSizeA1);
    connect(b_A2, &QPushButton::clicked, this, &MainWindow::setPrintSizeA2);
    connect(b_A3, &QPushButton::clicked, this, &MainWindow::setPrintSizeA3);
    connect(tb_maps, &QTableWidget::cellClicked, this, &MainWindow::mapCellClicked);
    connect(a_open_hlg, &QAction::triggered, this, &MainWindow::openHLG);
    connect(a_exit, &QAction::triggered, this, &MainWindow::close);
    connect(a_open_map, &QAction::triggered, this, &MainWindow::openMap);
    connect(a_open_wpt, &QAction::triggered, this,  &MainWindow::openWpt);
    connect(a_core_settings, &QAction::triggered, this, &MainWindow::settingsDialog);
    connect(b_printout, &QPushButton::clicked, this, &MainWindow::startPrint);
    connect(a_set_printer, &QAction::triggered, this, &MainWindow::setPrinter);
    connect(b_wiki2wpt, &QPushButton::clicked, this, &MainWindow::launchWiki2Wpt);
    connect(b_zip_folder, &QPushButton::clicked, this, &MainWindow::zipFolders);
    connect(b_curl_script, &QPushButton::clicked, this, &MainWindow::makeCurlScript);
    a_open_hlg->setShortcut(QKeySequence("Ctrl+O"));
    a_open_map->setShortcut(QKeySequence("Ctrl+M"));
}



QTableWidgetItem* MainWindow::tableItem(QTableWidget* table, int id)
{
    for(int i=0; i<table->rowCount(); i++)
        if(table->item(i,0)->data(Qt::ContextRole).toInt()==id)
            return table->item(i, 0);
    return nullptr;
}

void MainWindow::openHLG()
{
    QString hlg_path=QFileDialog::getOpenFileName(nullptr,
                                                  tr("Open SAS Saved region"),
                                                  "",
                                                  tr("SAS region(*.hlg)"));
    if(hlg_path.isEmpty())
        return;
    std::string save_path(PathResolver::sourcePath(PathResolver::Hlg));
    QFile::remove(QString::fromStdString(save_path));
    QFile::copy(hlg_path,QString::fromStdString(save_path));
    auto reg = HLGReader::region(hlg_path.toStdString());
    if(!reg)
    {
        onUserMessage(Warning, "Ошибка загрузки области выделения");
        return;
    }
    model->setRegion(reg);
    model->setLocation(HLGReader::location(hlg_path.toStdString()));
    a_open_hlg->setDisabled(true);
    b_curl_script->setDisabled(false);
}

void MainWindow::makeCurlScript()
{
    model->createUploadScript();
}

void MainWindow::openMap()
{
    if(!model->currentState().region)
    {
        onUserMessage(Critical, "Зона не загружена");
        return;
    }
    QString map_path=QFileDialog::getOpenFileName(nullptr,
                                                  tr("Открыть карту OZI Map"),
                                                  "",
                                                  tr("OZI Map(*.map)"));
    if(map_path.isEmpty())
        return;
    map_path = QDir(QDir::current()).absoluteFilePath(map_path);
    OziMapImportDialog diag(map_path,this);
    if(diag.exec()==QDialog::Accepted)
        model->requestLoadMap(diag.mapName().toStdString(),
                              map_path.toStdString(),
                              diag.isTopo(),
                              diag.zoom());

}

void MainWindow::settingsDialog()
{
    DialogSettings set(this);
    set.exec();

    ///@todo чюдовичщный хак, переместить это в модель. model->setupSettings(), напр.
    QFile::copy(QApplication::applicationDirPath()+"/readme.txt",
                QString::fromStdString(settings["AssemblyFolder"])+"/readme.txt");

    QDir(QString::fromStdString(settings["AssemblyFolder"])).mkpath("Sources");
    if(set.settingsChanged())
    {
        close();
    }
}

void MainWindow::setPrinter()
{
    QPrinter printer;
    QPrintDialog dialog(&printer,this);
    if (dialog.exec() == QDialog::Accepted)
        settings.setValue("PrinterName",printer.printerName().toStdString());
}
