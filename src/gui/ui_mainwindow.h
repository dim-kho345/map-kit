/********************************************************************************
** Form generated from reading UI file 'mainwindownjTkoO.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAINWINDOWNJTKOO_H
#define MAINWINDOWNJTKOO_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *a_open_hlg;
    QAction *a_open_map;
    QAction *a_open_wpt;
    QAction *a_exit;
    QAction *a_printout_settings;
    QAction *a_map4grp_settings;
    QAction *a_auto_ozf2;
    QAction *a_autofill;
    QAction *a_autozip;
    QAction *actionProgress_messages;
    QAction *a_jnx_enh_required;
    QAction *action_0;
    QAction *action_1;
    QAction *action_2;
    QAction *action_3;
    QAction *action_4;
    QAction *a_set_printer;
    QAction *a_core_settings;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lab_grid_exist;
    QLabel *lab_wiki_exist;
    QLabel *lab_coord2_exist;
    QLabel *lab_map4grp_exist;
    QLabel *lab_print_area_exist;
    QLabel *lab_jnx_exist;
    QLabel *lab_kmz_exist;
    QHBoxLayout *horizontalLayout_4;
    QTabWidget *tab_main;
    QWidget *tab_kit;
    QGridLayout *gridLayout;
    QPushButton *b_download_map;
    QPushButton *b_delete_map;
    QPushButton *b_printout;
    QTableWidget *tb_printout;
    QPushButton *b_conv2kmz;
    QPushButton *b_splitted_ena;
    QTableWidget *tb_maps;
    QLabel *label_7;
    QPushButton *b_zip_folder;
    QPushButton *b_conv2jnx;
    QPushButton *b_splitted_dis;
    QCheckBox *b_pre_cleanout;
    QLabel *label;
    QLabel *label_6;
    QPushButton *b_curl_script;
    QPushButton *b_load_map;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *b_A0;
    QPushButton *b_A1;
    QPushButton *b_A2;
    QPushButton *b_A3;
    QPushButton *b_conv2ozf;
    QPushButton *b_wiki2wpt;
    QTableWidget *tb_folders;
    QPushButton *b_conv2sqlitedb;
    QHBoxLayout *horizontalLayout_6;
    QPlainTextEdit *te_status;
    QPlainTextEdit *te_warnings;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuSettings;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1170, 641);
        QFont font;
        font.setPointSize(8);
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resource/globus.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        a_open_hlg = new QAction(MainWindow);
        a_open_hlg->setObjectName(QString::fromUtf8("a_open_hlg"));
        a_open_map = new QAction(MainWindow);
        a_open_map->setObjectName(QString::fromUtf8("a_open_map"));
        a_open_wpt = new QAction(MainWindow);
        a_open_wpt->setObjectName(QString::fromUtf8("a_open_wpt"));
        a_exit = new QAction(MainWindow);
        a_exit->setObjectName(QString::fromUtf8("a_exit"));
        a_printout_settings = new QAction(MainWindow);
        a_printout_settings->setObjectName(QString::fromUtf8("a_printout_settings"));
        a_map4grp_settings = new QAction(MainWindow);
        a_map4grp_settings->setObjectName(QString::fromUtf8("a_map4grp_settings"));
        a_auto_ozf2 = new QAction(MainWindow);
        a_auto_ozf2->setObjectName(QString::fromUtf8("a_auto_ozf2"));
        a_auto_ozf2->setCheckable(true);
        a_autofill = new QAction(MainWindow);
        a_autofill->setObjectName(QString::fromUtf8("a_autofill"));
        a_autofill->setCheckable(true);
        a_autozip = new QAction(MainWindow);
        a_autozip->setObjectName(QString::fromUtf8("a_autozip"));
        a_autozip->setCheckable(true);
        actionProgress_messages = new QAction(MainWindow);
        actionProgress_messages->setObjectName(QString::fromUtf8("actionProgress_messages"));
        a_jnx_enh_required = new QAction(MainWindow);
        a_jnx_enh_required->setObjectName(QString::fromUtf8("a_jnx_enh_required"));
        action_0 = new QAction(MainWindow);
        action_0->setObjectName(QString::fromUtf8("action_0"));
        action_1 = new QAction(MainWindow);
        action_1->setObjectName(QString::fromUtf8("action_1"));
        action_2 = new QAction(MainWindow);
        action_2->setObjectName(QString::fromUtf8("action_2"));
        action_3 = new QAction(MainWindow);
        action_3->setObjectName(QString::fromUtf8("action_3"));
        action_4 = new QAction(MainWindow);
        action_4->setObjectName(QString::fromUtf8("action_4"));
        a_set_printer = new QAction(MainWindow);
        a_set_printer->setObjectName(QString::fromUtf8("a_set_printer"));
        a_core_settings = new QAction(MainWindow);
        a_core_settings->setObjectName(QString::fromUtf8("a_core_settings"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setStyleSheet(QString::fromUtf8("centralWidget {\n"
"background: yellow;\n"
"width: 10px; /* when vertical /\n"
"height: 10px; / when horizontal */\n"
"}"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lab_grid_exist = new QLabel(centralwidget);
        lab_grid_exist->setObjectName(QString::fromUtf8("lab_grid_exist"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font1.setPointSize(8);
        lab_grid_exist->setFont(font1);
        lab_grid_exist->setTextFormat(Qt::AutoText);
        lab_grid_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_grid_exist);

        lab_wiki_exist = new QLabel(centralwidget);
        lab_wiki_exist->setObjectName(QString::fromUtf8("lab_wiki_exist"));
        lab_wiki_exist->setFont(font1);
        lab_wiki_exist->setTextFormat(Qt::AutoText);
        lab_wiki_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_wiki_exist);

        lab_coord2_exist = new QLabel(centralwidget);
        lab_coord2_exist->setObjectName(QString::fromUtf8("lab_coord2_exist"));
        lab_coord2_exist->setFont(font1);
        lab_coord2_exist->setTextFormat(Qt::AutoText);
        lab_coord2_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_coord2_exist);

        lab_map4grp_exist = new QLabel(centralwidget);
        lab_map4grp_exist->setObjectName(QString::fromUtf8("lab_map4grp_exist"));
        lab_map4grp_exist->setFont(font1);
        lab_map4grp_exist->setTextFormat(Qt::AutoText);
        lab_map4grp_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_map4grp_exist);

        lab_print_area_exist = new QLabel(centralwidget);
        lab_print_area_exist->setObjectName(QString::fromUtf8("lab_print_area_exist"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font2.setPointSize(8);
        font2.setBold(false);
        font2.setWeight(50);
        font2.setKerning(true);
        lab_print_area_exist->setFont(font2);
        lab_print_area_exist->setTextFormat(Qt::AutoText);
        lab_print_area_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_print_area_exist);

        lab_jnx_exist = new QLabel(centralwidget);
        lab_jnx_exist->setObjectName(QString::fromUtf8("lab_jnx_exist"));
        lab_jnx_exist->setFont(font1);
        lab_jnx_exist->setTextFormat(Qt::AutoText);
        lab_jnx_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_jnx_exist);

        lab_kmz_exist = new QLabel(centralwidget);
        lab_kmz_exist->setObjectName(QString::fromUtf8("lab_kmz_exist"));
        lab_kmz_exist->setFont(font1);
        lab_kmz_exist->setTextFormat(Qt::AutoText);
        lab_kmz_exist->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lab_kmz_exist);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        tab_main = new QTabWidget(centralwidget);
        tab_main->setObjectName(QString::fromUtf8("tab_main"));
        tab_main->setEnabled(true);
        tab_main->setAutoFillBackground(false);
        tab_main->setTabShape(QTabWidget::Rounded);
        tab_main->setUsesScrollButtons(true);
        tab_main->setTabsClosable(false);
        tab_main->setMovable(false);
        tab_main->setTabBarAutoHide(false);
        tab_kit = new QWidget();
        tab_kit->setObjectName(QString::fromUtf8("tab_kit"));
        gridLayout = new QGridLayout(tab_kit);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        b_download_map = new QPushButton(tab_kit);
        b_download_map->setObjectName(QString::fromUtf8("b_download_map"));
        b_download_map->setFont(font);

        gridLayout->addWidget(b_download_map, 3, 0, 1, 1);

        b_delete_map = new QPushButton(tab_kit);
        b_delete_map->setObjectName(QString::fromUtf8("b_delete_map"));

        gridLayout->addWidget(b_delete_map, 3, 2, 1, 1);

        b_printout = new QPushButton(tab_kit);
        b_printout->setObjectName(QString::fromUtf8("b_printout"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(b_printout->sizePolicy().hasHeightForWidth());
        b_printout->setSizePolicy(sizePolicy);

        gridLayout->addWidget(b_printout, 3, 7, 1, 1);

        tb_printout = new QTableWidget(tab_kit);
        if (tb_printout->columnCount() < 1)
            tb_printout->setColumnCount(1);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tb_printout->setHorizontalHeaderItem(0, __qtablewidgetitem);
        tb_printout->setObjectName(QString::fromUtf8("tb_printout"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tb_printout->sizePolicy().hasHeightForWidth());
        tb_printout->setSizePolicy(sizePolicy1);
        tb_printout->setColumnCount(1);
        tb_printout->horizontalHeader()->setCascadingSectionResizes(false);
        tb_printout->horizontalHeader()->setMinimumSectionSize(30);
        tb_printout->horizontalHeader()->setDefaultSectionSize(60);
        tb_printout->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tb_printout->horizontalHeader()->setStretchLastSection(true);
        tb_printout->verticalHeader()->setVisible(false);
        tb_printout->verticalHeader()->setHighlightSections(true);

        gridLayout->addWidget(tb_printout, 1, 7, 1, 3);

        b_conv2kmz = new QPushButton(tab_kit);
        b_conv2kmz->setObjectName(QString::fromUtf8("b_conv2kmz"));

        gridLayout->addWidget(b_conv2kmz, 4, 0, 1, 1);

        b_splitted_ena = new QPushButton(tab_kit);
        b_splitted_ena->setObjectName(QString::fromUtf8("b_splitted_ena"));
        b_splitted_ena->setEnabled(false);
        sizePolicy.setHeightForWidth(b_splitted_ena->sizePolicy().hasHeightForWidth());
        b_splitted_ena->setSizePolicy(sizePolicy);

        gridLayout->addWidget(b_splitted_ena, 4, 8, 1, 1);

        tb_maps = new QTableWidget(tab_kit);
        if (tb_maps->columnCount() < 6)
            tb_maps->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tb_maps->setHorizontalHeaderItem(0, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tb_maps->setHorizontalHeaderItem(1, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tb_maps->setHorizontalHeaderItem(2, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tb_maps->setHorizontalHeaderItem(3, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tb_maps->setHorizontalHeaderItem(4, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tb_maps->setHorizontalHeaderItem(5, __qtablewidgetitem6);
        tb_maps->setObjectName(QString::fromUtf8("tb_maps"));
        tb_maps->setMaximumSize(QSize(16777215, 16777215));
        QFont font3;
        font3.setKerning(true);
        tb_maps->setFont(font3);
        tb_maps->setAutoFillBackground(false);
        tb_maps->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        tb_maps->setSelectionMode(QAbstractItemView::SingleSelection);
        tb_maps->setSelectionBehavior(QAbstractItemView::SelectItems);
        tb_maps->horizontalHeader()->setVisible(true);
        tb_maps->horizontalHeader()->setCascadingSectionResizes(false);
        tb_maps->horizontalHeader()->setMinimumSectionSize(36);
        tb_maps->horizontalHeader()->setStretchLastSection(false);
        tb_maps->verticalHeader()->setCascadingSectionResizes(false);
        tb_maps->verticalHeader()->setStretchLastSection(false);

        gridLayout->addWidget(tb_maps, 1, 0, 1, 3);

        label_7 = new QLabel(tab_kit);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_7, 0, 8, 1, 1);

        b_zip_folder = new QPushButton(tab_kit);
        b_zip_folder->setObjectName(QString::fromUtf8("b_zip_folder"));

        gridLayout->addWidget(b_zip_folder, 3, 3, 1, 1);

        b_conv2jnx = new QPushButton(tab_kit);
        b_conv2jnx->setObjectName(QString::fromUtf8("b_conv2jnx"));

        gridLayout->addWidget(b_conv2jnx, 4, 2, 1, 1);

        b_splitted_dis = new QPushButton(tab_kit);
        b_splitted_dis->setObjectName(QString::fromUtf8("b_splitted_dis"));
        b_splitted_dis->setEnabled(false);
        sizePolicy.setHeightForWidth(b_splitted_dis->sizePolicy().hasHeightForWidth());
        b_splitted_dis->setSizePolicy(sizePolicy);

        gridLayout->addWidget(b_splitted_dis, 4, 9, 1, 1);

        b_pre_cleanout = new QCheckBox(tab_kit);
        b_pre_cleanout->setObjectName(QString::fromUtf8("b_pre_cleanout"));
        b_pre_cleanout->setEnabled(false);
        b_pre_cleanout->setCheckable(true);
        b_pre_cleanout->setChecked(false);
        b_pre_cleanout->setTristate(false);

        gridLayout->addWidget(b_pre_cleanout, 4, 3, 1, 1);

        label = new QLabel(tab_kit);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 3);

        label_6 = new QLabel(tab_kit);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_6, 0, 3, 1, 2);

        b_curl_script = new QPushButton(tab_kit);
        b_curl_script->setObjectName(QString::fromUtf8("b_curl_script"));
        b_curl_script->setEnabled(false);

        gridLayout->addWidget(b_curl_script, 4, 4, 1, 1);

        b_load_map = new QPushButton(tab_kit);
        b_load_map->setObjectName(QString::fromUtf8("b_load_map"));
        b_load_map->setEnabled(true);

        gridLayout->addWidget(b_load_map, 3, 1, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        b_A0 = new QPushButton(tab_kit);
        b_A0->setObjectName(QString::fromUtf8("b_A0"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(b_A0->sizePolicy().hasHeightForWidth());
        b_A0->setSizePolicy(sizePolicy2);
        b_A0->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(b_A0);

        b_A1 = new QPushButton(tab_kit);
        b_A1->setObjectName(QString::fromUtf8("b_A1"));
        b_A1->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(b_A1);

        b_A2 = new QPushButton(tab_kit);
        b_A2->setObjectName(QString::fromUtf8("b_A2"));
        b_A2->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(b_A2);

        b_A3 = new QPushButton(tab_kit);
        b_A3->setObjectName(QString::fromUtf8("b_A3"));
        b_A3->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(b_A3);


        gridLayout->addLayout(horizontalLayout_5, 3, 8, 1, 2);

        b_conv2ozf = new QPushButton(tab_kit);
        b_conv2ozf->setObjectName(QString::fromUtf8("b_conv2ozf"));
        b_conv2ozf->setEnabled(true);
        b_conv2ozf->setFlat(false);

        gridLayout->addWidget(b_conv2ozf, 4, 1, 1, 1);

        b_wiki2wpt = new QPushButton(tab_kit);
        b_wiki2wpt->setObjectName(QString::fromUtf8("b_wiki2wpt"));

        gridLayout->addWidget(b_wiki2wpt, 3, 4, 1, 1);

        tb_folders = new QTableWidget(tab_kit);
        if (tb_folders->columnCount() < 1)
            tb_folders->setColumnCount(1);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tb_folders->setHorizontalHeaderItem(0, __qtablewidgetitem7);
        tb_folders->setObjectName(QString::fromUtf8("tb_folders"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(tb_folders->sizePolicy().hasHeightForWidth());
        tb_folders->setSizePolicy(sizePolicy3);
        tb_folders->setSelectionMode(QAbstractItemView::ExtendedSelection);
        tb_folders->setSelectionBehavior(QAbstractItemView::SelectRows);
        tb_folders->horizontalHeader()->setStretchLastSection(true);

        gridLayout->addWidget(tb_folders, 1, 3, 1, 2);

        b_conv2sqlitedb = new QPushButton(tab_kit);
        b_conv2sqlitedb->setObjectName(QString::fromUtf8("b_conv2sqlitedb"));

        gridLayout->addWidget(b_conv2sqlitedb, 5, 0, 1, 1);

        tab_main->addTab(tab_kit, QString());

        horizontalLayout_4->addWidget(tab_main);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, -1, 0);
        te_status = new QPlainTextEdit(centralwidget);
        te_status->setObjectName(QString::fromUtf8("te_status"));

        horizontalLayout_6->addWidget(te_status);

        te_warnings = new QPlainTextEdit(centralwidget);
        te_warnings->setObjectName(QString::fromUtf8("te_warnings"));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 127, 127, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(255, 63, 63, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(127, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(170, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        QBrush brush7(QColor(255, 255, 220, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush8(QColor(0, 0, 0, 128));
        brush8.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush8);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush8);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush8);
#endif
        te_warnings->setPalette(palette);

        horizontalLayout_6->addWidget(te_warnings);


        verticalLayout->addLayout(horizontalLayout_6);

        verticalLayout->setStretch(1, 10);
        verticalLayout->setStretch(2, 2);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1170, 21));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuSettings = new QMenu(menubar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuSettings->menuAction());
        menuFile->addAction(a_open_hlg);
        menuFile->addAction(a_open_map);
        menuFile->addAction(a_open_wpt);
        menuFile->addSeparator();
        menuFile->addAction(a_exit);
        menuSettings->addAction(a_core_settings);
        menuSettings->addAction(a_set_printer);

        retranslateUi(MainWindow);

        tab_main->setCurrentIndex(0);
        b_conv2ozf->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Map Assembler", nullptr));
        a_open_hlg->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \320\267\320\276\320\275\321\203 HLG", nullptr));
        a_open_map->setText(QCoreApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214 \320\272\320\260\321\200\321\202\321\203 MAP", nullptr));
        a_open_wpt->setText(QCoreApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214 \321\201\320\265\321\202\320\272\321\203/\321\202\320\276\321\207\320\272\320\270 WPT", nullptr));
        a_exit->setText(QCoreApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", nullptr));
        a_printout_settings->setText(QCoreApplication::translate("MainWindow", "Printout", nullptr));
        a_map4grp_settings->setText(QCoreApplication::translate("MainWindow", "Map4Group", nullptr));
        a_auto_ozf2->setText(QCoreApplication::translate("MainWindow", "Auto OZF2 generation", nullptr));
        a_autofill->setText(QCoreApplication::translate("MainWindow", "Autofill folders", nullptr));
        a_autozip->setText(QCoreApplication::translate("MainWindow", "AutoZIP folders", nullptr));
        actionProgress_messages->setText(QCoreApplication::translate("MainWindow", "Progress messages", nullptr));
        a_jnx_enh_required->setText(QCoreApplication::translate("MainWindow", "\320\242\321\200\320\265\320\261\320\276\320\262\320\260\321\202\321\214 \320\276\320\261\321\200\320\260\320\261\320\276\321\202\320\272\321\203 \320\264\320\273\321\217 JNX", nullptr));
        action_0->setText(QCoreApplication::translate("MainWindow", "\320\2200", nullptr));
        action_1->setText(QCoreApplication::translate("MainWindow", "\320\2201", nullptr));
        action_2->setText(QCoreApplication::translate("MainWindow", "\320\2202", nullptr));
        action_3->setText(QCoreApplication::translate("MainWindow", "\320\2203", nullptr));
        action_4->setText(QCoreApplication::translate("MainWindow", "\321\203\320\272\321\206", nullptr));
        a_set_printer->setText(QCoreApplication::translate("MainWindow", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214 \320\277\321\200\320\270\320\275\321\202\320\265\321\200", nullptr));
        a_core_settings->setText(QCoreApplication::translate("MainWindow", "\320\236\320\261\321\211\320\270\320\265", nullptr));
        lab_grid_exist->setText(QCoreApplication::translate("MainWindow", "\320\241\320\265\321\202\320\272\320\260", nullptr));
        lab_wiki_exist->setText(QCoreApplication::translate("MainWindow", "Wiki \321\202\320\276\321\207\320\272\320\270", nullptr));
        lab_coord2_exist->setText(QCoreApplication::translate("MainWindow", "\320\232\320\276\320\276\321\200\320\264\320\270\320\275\320\260\321\202\321\213 \321\201\320\261\320\276\321\200\320\260/\321\210\321\202\320\260\320\261\320\260", nullptr));
        lab_map4grp_exist->setText(QCoreApplication::translate("MainWindow", "\320\232\320\260\321\200\321\202\320\260 \320\264\320\273\321\217 \320\263\321\200\321\203\320\277\320\277", nullptr));
        lab_print_area_exist->setText(QCoreApplication::translate("MainWindow", "\320\236\320\261\320\273\320\260\321\201\321\202\321\214 \320\277\320\265\321\207\320\260\321\202\320\270", nullptr));
        lab_jnx_exist->setText(QCoreApplication::translate("MainWindow", "JNX \320\241\320\277\321\203\321\202\320\275\320\270\320\272", nullptr));
        lab_kmz_exist->setText(QCoreApplication::translate("MainWindow", "KMZ \320\242\320\276\320\277\320\276", nullptr));
        b_download_map->setText(QCoreApplication::translate("MainWindow", "\320\241\320\272\320\260\321\207\320\260\321\202\321\214", nullptr));
        b_delete_map->setText(QCoreApplication::translate("MainWindow", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", nullptr));
        b_printout->setText(QCoreApplication::translate("MainWindow", "\320\240\320\260\321\201\320\277\320\265\321\207\320\260\321\202\320\260\321\202\321\214", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tb_printout->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("MainWindow", "\320\232\320\260\321\200\321\202\320\260", nullptr));
        b_conv2kmz->setText(QCoreApplication::translate("MainWindow", "\320\232\320\276\320\275\320\262\320\265\321\200\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214\n"
"\320\262 KMZ", nullptr));
        b_splitted_ena->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\320\273.\n"
"\321\201\320\272\320\273\320\265\320\271\320\272\320\270", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tb_maps->horizontalHeaderItem(0);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("MainWindow", "\320\230\321\201\321\202\320\276\321\207\320\275\320\270\320\272", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tb_maps->horizontalHeaderItem(1);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("MainWindow", "ozf2", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tb_maps->horizontalHeaderItem(2);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("MainWindow", "kmz", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tb_maps->horizontalHeaderItem(3);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("MainWindow", "jnx", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tb_maps->horizontalHeaderItem(4);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("MainWindow", "guru topo", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tb_maps->horizontalHeaderItem(5);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("MainWindow", "guru sat", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "\320\237\320\265\321\207\320\260\321\202\321\214", nullptr));
        b_zip_folder->setText(QCoreApplication::translate("MainWindow", "\320\243\320\277\320\260\320\272\320\276\320\262\320\260\321\202\321\214 ZIP", nullptr));
        b_conv2jnx->setText(QCoreApplication::translate("MainWindow", "\320\232\320\276\320\275\320\262\320\265\321\200\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214\n"
"\320\262 JNX", nullptr));
        b_splitted_dis->setText(QCoreApplication::translate("MainWindow", "\320\222\320\272\320\273.\n"
"\321\201\320\272\320\273\320\265\320\271\320\272\320\270", nullptr));
        b_pre_cleanout->setText(QCoreApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214 \320\277\320\260\320\277\320\272\321\203\n"
"\320\277\320\265\321\200\320\265\320\264 \321\203\320\277\320\260\320\272\320\276\320\262\321\201\320\272\320\276\320\271", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "\320\232\320\260\321\200\321\202\321\213", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "\320\237\320\260\320\277\320\272\320\270", nullptr));
        b_curl_script->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\320\267\320\264\320\260\321\202\321\214 \321\201\320\272\321\200\320\270\320\277\321\202 \320\264\320\273\321\217 Curl", nullptr));
        b_load_map->setText(QCoreApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214 MAP", nullptr));
        b_A0->setText(QCoreApplication::translate("MainWindow", "A0", nullptr));
        b_A1->setText(QCoreApplication::translate("MainWindow", "A1", nullptr));
        b_A2->setText(QCoreApplication::translate("MainWindow", "A2", nullptr));
        b_A3->setText(QCoreApplication::translate("MainWindow", "A3", nullptr));
        b_conv2ozf->setText(QCoreApplication::translate("MainWindow", "\320\232\320\276\320\275\320\262\320\265\321\200\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214\n"
"\320\262 OZF2", nullptr));
        b_wiki2wpt->setText(QCoreApplication::translate("MainWindow", "Wiki2Wpt", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tb_folders->horizontalHeaderItem(0);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("MainWindow", "\320\232\320\260\321\200\321\202\320\260", nullptr));
        b_conv2sqlitedb->setText(QCoreApplication::translate("MainWindow", "\320\232\320\276\320\275\320\262\320\265\321\200\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\262 SqliteDB", nullptr));
        tab_main->setTabText(tab_main->indexOf(tab_kit), QCoreApplication::translate("MainWindow", "\320\232\320\276\320\274\320\277\320\273\320\265\320\272\321\202", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "\320\244\320\260\320\271\320\273", nullptr));
        menuSettings->setTitle(QCoreApplication::translate("MainWindow", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAINWINDOWNJTKOO_H
