#include <QApplication>
#include <QEvent>
#include "gui/gui_adapter.h"

enum {kModelUpdatedEvent=QEvent::User,
      kServiceProgressEvent=QEvent::User+1,
      kUserMessageEvent=QEvent::User+2};


class ModelUpdatedEvent : public QEvent
{
public:
    ModelUpdatedEvent(GUI::WhatUpdated w) :
        QEvent(QEvent::Type(kModelUpdatedEvent)),
        what(w)
    {}
    const GUI::WhatUpdated what;
};

class UserMessageEvent : public QEvent
{
public:
    UserMessageEvent(GUI::UserMessageType type, std::string what) :
        QEvent(QEvent::Type(kUserMessageEvent)),
        type(type),
        what(what)
    {}
    const GUI::UserMessageType type;
    const std::string what;
};

class ServiceProgressEvent : public QEvent
{
public:
    ServiceProgressEvent(ServiceFunction func,
                         int model_id,
                         int job_id,
                         int progress,
                         std::string message="") :
        QEvent(QEvent::Type(kServiceProgressEvent)),
        func(func),
        model_id(model_id),
        job_id(job_id),
        progress(progress),
        message(message)
    {}
    ServiceFunction func;
    int model_id;
    int job_id;
    int progress;
    std::string message;
};

bool GUIAdapter::event(QEvent *event)
{
    if(event->type() == kModelUpdatedEvent)
    {
        gui->onModelUpdated(static_cast<ModelUpdatedEvent*>(event)->what);
        return true;
    }

    if(event->type() == kServiceProgressEvent)
    {
        auto ev  = static_cast<ServiceProgressEvent*>(event);
        gui->onProgress(ev->func, ev->model_id, ev->job_id,
                        ev->progress, ev->message );
        return true;
    }

    if(event->type() == kUserMessageEvent)
    {
        auto ev  = static_cast<UserMessageEvent*>(event);
        gui->onUserMessage(ev->type, ev->what);
        return true;
    }

    return QObject::event(event);
}

void GUIAdapter::onModelUpdated(int what)
{
    QApplication::postEvent(this,
                            new ModelUpdatedEvent(GUI::WhatUpdated(what)));
}

void GUIAdapter::onProgress(ServiceFunction func,
                            int model_id,
                            int job_id,
                            int progress,
                            std::string message)
{
    QApplication::postEvent(this,
                            new ServiceProgressEvent(func,
                                                     model_id,
                                                     job_id,
                                                     progress,
                                                     message));
}

void GUIAdapter::onUserMessage(UserMessageType type, std::string what)
{
    QApplication::postEvent(this,
                            new UserMessageEvent(type, what));
}
