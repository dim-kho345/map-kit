#ifndef MAP_VIEW_H
#define MAP_VIEW_H
#include <QGraphicsView>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QGraphicsItem>

class MapView : public QGraphicsView
{
    Q_OBJECT
public:
    /**
     * @brief Режим работы окна
     * Navigate - перетаскивание окна рабочего пространстра мышью
     * Select - выбор прямоугольной области
     * Hover - возить объектом (напр. прямоугольником) по окну до нажатия мышью
     */
    enum Mode {Navigate,Select,HoverItem};
    MapView(QWidget* parent=nullptr);
    void setMode(Mode m);
    auto currentMode(){
        return mode;
    }
signals:
    void areaSelected(QRectF);
    void pointSelected(QPointF);
    void hoverPoint(QPointF);
protected:
    void wheelEvent(QWheelEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void keyPressEvent(QKeyEvent* ev) override;
private:
    double scale;

    Mode mode{Navigate};
    QGraphicsRectItem* rect{nullptr};
    QPointF anchor;
    QTransform t;
    QMouseEvent* sub_ev;
    QPen pen;
};

#endif // MAP_VIEW_H
