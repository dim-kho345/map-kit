#include "mainwindow.h"
#include "data_roles.h"
#include "settings.h"


void MainWindow::setPrintSizeA0()
{
    setSelectedPrintFormat(A0);
}
void MainWindow::setPrintSizeA1()
{
    setSelectedPrintFormat(A1);
}
void MainWindow::setPrintSizeA2()
{
    setSelectedPrintFormat(A2);
}
void MainWindow::setPrintSizeA3()
{
    setSelectedPrintFormat(A3);
}

void MainWindow::setSelectedPrintFormat(PrintFormat f)
{
    auto items = tb_printout->selectedItems();
    for(auto i : items)
    {
        auto model_id = i->data(Qt::ContextRole).toInt();
        model->setPrintOptions(model_id,{f});
    }
}

void MainWindow::startPrint()
{
    auto items = tb_printout->selectedItems();
    for(auto i : items)
    {
        auto model_id = i->data(Qt::ContextRole).toInt();
        model->requestPrintOut(model_id, settings["PrinterName"]);
    }
}
void MainWindow::excludeSplited()
{

}
void MainWindow::includeSplited()
{

}
