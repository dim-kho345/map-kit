/********************************************************************************
** Form generated from reading UI file 'dialog_settingsFMZEUt.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef DIALOG_SETTINGSFMZEUT_H
#define DIALOG_SETTINGSFMZEUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *le_kit_root_path;
    QToolButton *tb_kit_root_path;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *le_sas_path;
    QToolButton *tb_sas_path;
    QLabel *label_6;
    QLineEdit *le_ftp_server;
    QLabel *label_7;
    QLabel *label_4;
    QDialogButtonBox *buttonBox;
    QLineEdit *le_ftp_user;
    QLineEdit *le_ftp_password;
    QLabel *label_8;
    QLabel *label_9;
    QLineEdit *le_ftp_folder;
    QLabel *label_3;
    QSpinBox *sb_google_version;
    QSpinBox *sb_icon_side;
    QLabel *label_5;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(560, 332);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Dialog->sizePolicy().hasHeightForWidth());
        Dialog->setSizePolicy(sizePolicy);
        formLayout = new QFormLayout(Dialog);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setLabelAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label = new QLabel(Dialog);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        le_kit_root_path = new QLineEdit(Dialog);
        le_kit_root_path->setObjectName(QString::fromUtf8("le_kit_root_path"));
        le_kit_root_path->setReadOnly(true);

        horizontalLayout_2->addWidget(le_kit_root_path);

        tb_kit_root_path = new QToolButton(Dialog);
        tb_kit_root_path->setObjectName(QString::fromUtf8("tb_kit_root_path"));

        horizontalLayout_2->addWidget(tb_kit_root_path);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout_2);

        label_2 = new QLabel(Dialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        le_sas_path = new QLineEdit(Dialog);
        le_sas_path->setObjectName(QString::fromUtf8("le_sas_path"));
        le_sas_path->setReadOnly(true);

        horizontalLayout->addWidget(le_sas_path);

        tb_sas_path = new QToolButton(Dialog);
        tb_sas_path->setObjectName(QString::fromUtf8("tb_sas_path"));

        horizontalLayout->addWidget(tb_sas_path);


        formLayout->setLayout(2, QFormLayout::FieldRole, horizontalLayout);

        label_6 = new QLabel(Dialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_6);

        le_ftp_server = new QLineEdit(Dialog);
        le_ftp_server->setObjectName(QString::fromUtf8("le_ftp_server"));
        le_ftp_server->setReadOnly(false);

        formLayout->setWidget(3, QFormLayout::FieldRole, le_ftp_server);

        label_7 = new QLabel(Dialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_7);

        label_4 = new QLabel(Dialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(13, QFormLayout::LabelRole, label_4);

        buttonBox = new QDialogButtonBox(Dialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(13, QFormLayout::FieldRole, buttonBox);

        le_ftp_user = new QLineEdit(Dialog);
        le_ftp_user->setObjectName(QString::fromUtf8("le_ftp_user"));
        le_ftp_user->setReadOnly(false);

        formLayout->setWidget(4, QFormLayout::FieldRole, le_ftp_user);

        le_ftp_password = new QLineEdit(Dialog);
        le_ftp_password->setObjectName(QString::fromUtf8("le_ftp_password"));
        le_ftp_password->setReadOnly(false);

        formLayout->setWidget(5, QFormLayout::FieldRole, le_ftp_password);

        label_8 = new QLabel(Dialog);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_8);

        label_9 = new QLabel(Dialog);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_9);

        le_ftp_folder = new QLineEdit(Dialog);
        le_ftp_folder->setObjectName(QString::fromUtf8("le_ftp_folder"));
        le_ftp_folder->setReadOnly(false);

        formLayout->setWidget(6, QFormLayout::FieldRole, le_ftp_folder);

        label_3 = new QLabel(Dialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_3);

        sb_google_version = new QSpinBox(Dialog);
        sb_google_version->setObjectName(QString::fromUtf8("sb_google_version"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(sb_google_version->sizePolicy().hasHeightForWidth());
        sb_google_version->setSizePolicy(sizePolicy1);
        sb_google_version->setMaximum(9999);
        sb_google_version->setSingleStep(1);
        sb_google_version->setValue(874);

        formLayout->setWidget(8, QFormLayout::FieldRole, sb_google_version);

        sb_icon_side = new QSpinBox(Dialog);
        sb_icon_side->setObjectName(QString::fromUtf8("sb_icon_side"));
        sizePolicy1.setHeightForWidth(sb_icon_side->sizePolicy().hasHeightForWidth());
        sb_icon_side->setSizePolicy(sizePolicy1);
        sb_icon_side->setMinimum(1024);
        sb_icon_side->setMaximum(4095);
        sb_icon_side->setValue(2048);

        formLayout->setWidget(9, QFormLayout::FieldRole, sb_icon_side);

        label_5 = new QLabel(Dialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_5);


        retranslateUi(Dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", nullptr));
        label->setText(QCoreApplication::translate("Dialog", "\320\232\320\276\321\200\320\275\320\265\320\262\320\276\320\271 \320\272\320\260\321\202\320\260\320\273\320\276\320\263\n"
"\320\272\320\276\320\274\320\277\320\273\320\265\320\272\321\202\320\276\320\262", nullptr));
        tb_kit_root_path->setText(QCoreApplication::translate("Dialog", "...", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog", "\320\237\320\260\320\277\320\272\320\260 \320\272\321\215\321\210\320\260 \321\204\320\276\321\200\320\274\320\260\321\202\320\260 SAS.Planet", nullptr));
        tb_sas_path->setText(QCoreApplication::translate("Dialog", "...", nullptr));
        label_6->setText(QCoreApplication::translate("Dialog", "FTP \321\201\320\265\321\200\320\262\320\265\321\200", nullptr));
        label_7->setText(QCoreApplication::translate("Dialog", "FTP \320\273\320\276\320\263\320\270\320\275", nullptr));
        label_4->setText(QString());
        label_8->setText(QCoreApplication::translate("Dialog", "FTP \320\277\320\260\321\200\320\276\320\273\321\214", nullptr));
        label_9->setText(QCoreApplication::translate("Dialog", "FTP \320\272\320\260\321\202\320\260\320\273\320\276\320\263", nullptr));
        label_3->setText(QCoreApplication::translate("Dialog", "\320\222\320\265\321\200\321\201\320\270\321\217 Google", nullptr));
        label_5->setText(QCoreApplication::translate("Dialog", "\320\240\320\260\320\267\320\274\320\265\321\200 \320\274\320\270\320\275\320\270\320\260\321\202\321\216\321\200\321\213 \320\272\320\260\321\200\321\202\321\213", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // DIALOG_SETTINGSFMZEUT_H
