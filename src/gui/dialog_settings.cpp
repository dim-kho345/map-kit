#include <QFileDialog>
#include <QMessageBox>
#include "dialog_settings.h"

DialogSettings::DialogSettings(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    le_kit_root_path->setText(settings["AssemblyFolder"].c_str());
    le_sas_path->setText(settings["SASPath"].c_str());
    le_ftp_server->setText(settings["FTPServer"].c_str());
    le_ftp_user->setText(settings["FTPUser"].c_str());
    le_ftp_password->setText(settings["FTPPassword"].c_str());
    le_ftp_folder->setText(settings["FTPFolder"].c_str());
    sb_google_version->setValue(std::stoi(settings["GoogleVersion"]));
    sb_icon_side->setValue(std::stoi(settings["IconSideSize"]));

    connect(tb_kit_root_path,&QToolButton::pressed, this, &DialogSettings::selectKitRootPath);
    connect(tb_sas_path,&QToolButton::pressed, this, &DialogSettings::selectSASCachePath);
    connect(tb_sas_path,&QToolButton::pressed, this, &DialogSettings::selectSASCachePath);
    connect(le_ftp_server,&QLineEdit::textEdited, this, [this](){settings_changed=true;});
    connect(le_ftp_user,&QLineEdit::textEdited, this, [this](){settings_changed=true;});
    connect(le_ftp_password,&QLineEdit::textEdited, this, [this](){settings_changed=true;});
    connect(le_ftp_folder,&QLineEdit::textEdited, this, [this](){settings_changed=true;});

    connect(sb_icon_side, QOverload<int>::of(&QSpinBox::valueChanged),
            this, [this](int){settings_changed=true;});
    connect(sb_google_version, QOverload<int>::of(&QSpinBox::valueChanged),
            this, [this](int){settings_changed=true;});

}



void DialogSettings::accept()
{
    for(const auto& s : {le_sas_path->text(), le_kit_root_path->text()})
    {
        QDir d(s);
        if(!d.exists())
        {
            QMessageBox::critical(this, "Ошибка", "Папка\n" + d.path() + "\nне существует", QMessageBox::Ok);
            return;
        }
    }

    if(!settings_changed)
    {
        QDialog::accept();
        return;
    }

    settings.setValue("AssemblyFolder", le_kit_root_path->text().toStdString());
    settings.setValue("SASPath", le_sas_path->text().toStdString());
    settings.setValue("FTPServer", le_ftp_server->text().toStdString());
    settings.setValue("FTPUser", le_ftp_user->text().toStdString());
    settings.setValue("FTPPassword", le_ftp_password->text().toStdString());
    settings.setValue("FTPFolder", le_ftp_folder->text().toStdString());
    settings.setValue("GoogleVersion", std::to_string(sb_google_version->value()));
    settings.setValue("IconSideSize", std::to_string(sb_icon_side->value()));

    QMessageBox::information(this, "Изменение настроек",
                             "Настройки изменены\nПерезапустите программу", QMessageBox::Ok);
    change_accepted=true;
    QDialog::accept();
}


void DialogSettings::selectKitRootPath()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Корневой каталог карт"),
                                                     le_kit_root_path->text(),
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
    if(!dir.isEmpty())
    {
        le_kit_root_path->setText(dir);
        settings_changed=true;
    }
}
void DialogSettings::selectSASCachePath()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Каталог кэша SAS.Planet"),
                                                     le_sas_path->text(),
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
    if(!dir.isEmpty())
    {
        le_sas_path->setText(dir);
        settings_changed = true;
    }
}
