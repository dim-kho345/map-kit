#include  <QRegExpValidator>
#include "dialog_import_map.h"
#include "util.h"


OziMapImportDialog::OziMapImportDialog(QString filename, QWidget *parent) :
    QDialog(parent)
{
    layf = new QFormLayout;
    lay = new QHBoxLayout;
    lay_src = new QVBoxLayout;
    lay_zoom = new QVBoxLayout;

    le_name = new QLineEdit;
    bb = new QDialogButtonBox(QDialogButtonBox::Ok |
                              QDialogButtonBox::Cancel,
                              this);
    layf->addRow(tr("Введите название карты (\"GGC_Edited\",\"Mymap\",...)"),le_name);
    layf->addWidget(bb);

    auto fname = filenameFromPath(filename.toStdString());
    fname = fname.substr(0,fname.rfind('.'));
    le_name->setText(QString::fromStdString(fname));
    connect(bb,&QDialogButtonBox::rejected,this,&QDialog::reject);
    connect(bb,&QDialogButtonBox::accepted,this,&OziMapImportDialog::okPressed);

    rb_sat = new QRadioButton(tr("Sattelite"));
    rb_topo = new QRadioButton(tr("Topo"));
    rb_zoom14 = new QRadioButton("z14");
    rb_zoom15 = new QRadioButton("z15");
    rb_zoom16 = new QRadioButton("z16");
    rb_zoom17 = new QRadioButton("z17");

    lay_src->addWidget(rb_topo);
    lay_src->addWidget(rb_sat);

    lay_zoom->addWidget(rb_zoom14);
    lay_zoom->addWidget(rb_zoom15);
    lay_zoom->addWidget(rb_zoom16);
    lay_zoom->addWidget(rb_zoom17);

    box_src=new QGroupBox(tr("Source"));
    box_zoom=new QGroupBox(tr("Source zoom"));
    box_zoom->setToolTip(tr("Mandatory for sattelite sources"));

    box_src->setLayout(lay_src);
    box_zoom->setLayout(lay_zoom);
    lay->addWidget(box_zoom);
    lay->addWidget(box_src);
    lay->addLayout(layf);
    rb_zoom17->setChecked(true);
    rb_sat->setChecked(true);

    for (auto& s : QStringList{"topo","mmb","ggc","osm","yandex"})
    if(filename.contains(s,Qt::CaseInsensitive))
    {
        rb_topo->setChecked(true);
        rb_zoom16->setChecked(true);
    }

    if(filename.contains("satel",Qt::CaseInsensitive))
        rb_sat->setChecked(true);
    if(filename.contains("z14",Qt::CaseInsensitive))
        rb_zoom14->setChecked(true);
    if(filename.contains("z15",Qt::CaseInsensitive))
        rb_zoom15->setChecked(true);
    if(filename.contains("z16",Qt::CaseInsensitive))
        rb_zoom16->setChecked(true);
    if(filename.contains("z17",Qt::CaseInsensitive))
        rb_zoom17->setChecked(true);

    setLayout(lay);
    setModal(true);
    le_name->setFocus();
    show();
    raise();
    activateWindow();
}

void OziMapImportDialog::okPressed()
{
    if(!le_name->text().isEmpty())
        done(QDialog::Accepted);
}

int OziMapImportDialog::zoom()
{
    if(rb_zoom14->isChecked())
        return 13;
    if(rb_zoom15->isChecked())
        return 14;
    if(rb_zoom16->isChecked())
        return 15;
    if(rb_zoom17->isChecked())
        return 16;
    return 16;
}
bool OziMapImportDialog::isTopo() const
{
    if(rb_sat->isChecked())
        return false;
    else
        return true;
}
