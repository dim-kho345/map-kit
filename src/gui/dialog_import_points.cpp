#include <QFileDialog>
#include <QMessageBox>
#include "mainwindow.h"
#include "ozi.h"

void MainWindow::openWpt()
{
    if(model->currentState().map.empty())
    {
        onUserMessage(Critical, "Карты отсутствуют");
        return;
    }
    std::string wpt_file=QFileDialog::getOpenFileName(nullptr,
                                                  tr("Open WPT Points"),
                                                     "",
                                                 tr("Ozi WPT (*.wpt)")).toStdString();
    if(wpt_file.empty())
        return;
    OziWptLoader wpt;
    if(wpt.load(wpt_file))
    {
        wpt.parse();
        QMessageBox wpt_dlg;
        wpt_dlg.setWindowTitle(tr("Import wpt"));
        QPushButton* b_grid{nullptr};
        QPushButton* b_poi{nullptr};
        QPushButton* b_both{nullptr};
        if(wpt.hasGrid())
            b_grid=wpt_dlg.addButton(tr("Grid"),QMessageBox::ActionRole);
        if(wpt.numPoints())
            b_poi=wpt_dlg.addButton(tr("Points"),QMessageBox::ActionRole);
        if(wpt.hasGrid() && wpt.numPoints())
            b_both=wpt_dlg.addButton(tr("Both"),QMessageBox::ActionRole);
        if(!wpt.hasGrid() && !wpt.numPoints())
            wpt_dlg.setText(tr("No points or grid detected"));
        else
            wpt_dlg.setText(tr("%1 custom points found. What to import:").
                            arg(wpt.numPoints()));
        wpt_dlg.addButton(QMessageBox::StandardButton::Cancel);

        wpt_dlg.exec();

        ///@todo добавить в состояние модели сетку и изменять сетку в GUI из модели
        /// а не тут напрямую.
        /// также переименовать эти get-ы ебучие
        if(wpt_dlg.clickedButton()==b_grid)
        {
            model->setGrid(wpt.getBounds(),
                                    wpt.getGridStep(),
                                    wpt.getGridFirstPoint());
            navigator->setGrid(wpt.getBounds(),
                               wpt.getGridStep(),
                               wpt.getGridFirstPoint());
        }
        if(wpt_dlg.clickedButton()==b_poi)
        {
            model->setWikiPnt(wpt.points());
        }
        if(wpt_dlg.clickedButton()==b_both)
        {
            model->setGrid(wpt.getBounds(),
                                    wpt.getGridStep(),
                                    wpt.getGridFirstPoint());
            model->setWikiPnt(wpt.points());
            navigator->setGrid(wpt.getBounds(),
                               wpt.getGridStep(),
                               wpt.getGridFirstPoint());
        }
    }
    else
    {
        QMessageBox::information(this,tr("Grid loader: "),
                                 QString::fromStdString(wpt.error()),QMessageBox::Ok);
    }
}


