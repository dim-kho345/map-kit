#include "map_view.h"

MapView::MapView(QWidget* parent) :
    QGraphicsView(parent)
{
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    //setDragMode(DragMode::NoDrag);
    pen.setCosmetic(true);
    pen.setWidth(0);
    setCursor(Qt::CursorShape::ArrowCursor);
}


void MapView::setMode(Mode m)
{
    mode=m;
    if(mode==Navigate)
        setDragMode(ScrollHandDrag);
    else
    {
        setDragMode(NoDrag);
        setCursor(Qt::CursorShape::ArrowCursor);
    }   
}


void MapView::wheelEvent(QWheelEvent *ev)
{
    double scale;
    if(ev->angleDelta().y()<0) scale=0.87;else scale=1/0.87;
    QTransform t=transform();
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    t.scale(scale,scale);
    if(t.m11()>1.05 || t.m11()<0.05)
        return;
    setTransform(t);
}


void MapView::mouseMoveEvent(QMouseEvent *ev)
{
    switch(mode)
    {
    case Navigate:    
        break;
    case Select:
        if(rect)
            rect->setRect(QRectF(anchor,mapToScene(ev->pos())));
        break;
    case HoverItem:
        emit hoverPoint(mapToScene(ev->pos()));
    }
    QGraphicsView::mouseMoveEvent(ev);
}



void MapView::mousePressEvent(QMouseEvent *ev)
{
    switch(mode)
    {
    case Navigate:
        break;
    case Select:
        /*
          В режиме выбора точки Coord2 после нажатия событие об отжатии кнопки
          не поступает в этот Widget и мы оказываемся в режиме grabbing
        */
        if(ev->button()==Qt::LeftButton)
        {
            anchor=mapToScene(ev->pos());
            rect=scene()->addRect(QRectF(anchor,anchor+QPointF(1,1)),pen);
            ev->accept();
        }
        break;
    case HoverItem:
        if(ev->button()==Qt::LeftButton)
        {
            QGraphicsView::mousePressEvent(ev);
            mode=Navigate;
            emit pointSelected(mapToScene(ev->pos()));
            ev->accept();
            return;
        }
        break;
    }
    QGraphicsView::mousePressEvent(ev);
}

void MapView::mouseReleaseEvent(QMouseEvent *ev)
{
    switch(mode)
    {
    case Navigate:
        break;
    case Select:
        if(ev->button()==Qt::LeftButton && rect)
        {
            scene()->removeItem(rect);
            rect->~QGraphicsRectItem();
            rect=nullptr;
            emit areaSelected(QRectF(anchor,mapToScene(ev->pos())));
        }
        break;
    case HoverItem:
        break;
    }
    QGraphicsView::mouseReleaseEvent(ev);
}


void  MapView::keyPressEvent(QKeyEvent* ev)
{
    QGraphicsView::keyPressEvent(ev);
}
