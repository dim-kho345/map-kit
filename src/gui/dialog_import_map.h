#ifndef DIALOG_IMPORT_OZI_H
#define DIALOG_IMPORT_OZI_H

#include <QDialog>
#include <QFormLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include "map_source.h"


class OziMapImportDialog : public QDialog
{
    Q_OBJECT
public:
    OziMapImportDialog(QString filename, QWidget *parent=nullptr);
    QString mapName() const {return le_name->text();}
    int zoom();
    bool isTopo() const;
private:
    QFormLayout* layf;
    QHBoxLayout* lay;
    QVBoxLayout *lay_src, *lay_zoom;
    QLabel* lab_name;
    QLineEdit* le_name;
    QGroupBox* box_zoom;
    QGroupBox* box_src;
    QRadioButton *rb_zoom14,*rb_zoom15,*rb_zoom16,*rb_zoom17;
    QRadioButton *rb_sat,*rb_topo;
    QDialogButtonBox* bb;

private slots:
    void okPressed();
};

#endif // DIALOG_IMPORT_OZI_H
