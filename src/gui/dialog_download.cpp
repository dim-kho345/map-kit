#include <QSpacerItem>
#include "dialog_download.h"

DownloadMapDialog::DownloadMapDialog(QWidget *parent, Qt::WindowFlags f, Model* model) :
    QDialog(parent, f),
    model(model)
{
    setWindowTitle(tr("Download map"));
}


void DownloadMapDialog::openDialog()
{
    int row {0};
    auto ms = model->currentState();
    if(!ms.region)
        return;
    lay = new QGridLayout;
    map_src=model->enumerateMapSources();
    for(auto m : map_src)
    {
        auto m_info = m->info();
        QString name(QString::fromStdString(m_info.name));
        if(m_info.is_topo)
            name.append(" топо");
        else
            name.append(" спутник");
        auto gb = new QGroupBox;

        auto lay_zoom = new QHBoxLayout;
        gb->setLayout(lay_zoom);
        gb->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        lay->addWidget(new QLabel(name),row,0);
        lay->addWidget(gb,row,1);
        auto skip_download = new QRadioButton("Нет");
        rb_zoom.push_back({{SkipDownload,skip_download}});
        lay_zoom->addWidget(skip_download);
        skip_download->setChecked(true);

        for(auto z : m_info.zoom_level)
        {
            rb_zoom.back().push_back({z,new QRadioButton((QString("%1").arg(z+1)))});
            lay_zoom->addWidget(rb_zoom.back().back().second);
        }
        apply_guru.emplace(row,new QCheckBox);
        lay->addWidget(apply_guru.at(row),row,2);
        apply_guru.at(row)->setToolTip("Карта будет выгружена в\nsqlitedb формат");

        ++row;

    }

    // set spacing for Start/Cancel buttons
    lay->setRowMinimumHeight(row,30);
    ++row;
    b_start = new QPushButton("Скачать");
    b_cancel = new QPushButton("Отмена");
    lay->addWidget(b_start,row,0);
    lay->addWidget(b_cancel,row,1);

    b_cancel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    setLayout(lay);
    connect(b_start, &QPushButton::clicked, this, [=](){done(QDialog::Accepted);});
    connect(b_cancel, &QPushButton::clicked, this, [=](){done(QDialog::Rejected);});
    show();
}

void DownloadMapDialog::done(int r)
{

    if(r == QDialog::Accepted)
    {
        for(unsigned i=0; i<rb_zoom.size(); i++)
        {
            for(const auto& rb : rb_zoom.at(i))
                if(rb.second->isChecked() && rb.first!=SkipDownload)
                {
                    model->requestDownload(map_src.at(i),
                                           rb.first,
                                           apply_guru.at(i)->isChecked());
                }
        }
    }
    delete lay;
    rb_zoom.clear();
    apply_guru.clear();
    QDialog::done(r);
}
