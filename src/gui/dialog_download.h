#ifndef DOWNLOADMAP_H
#define DOWNLOADMAP_H

#include <QDialog>
#include <QLabel>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include "model/model.h"

class DownloadMapDialog : public QDialog
{
public:
    using Model=ModelInterface;
    DownloadMapDialog(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags(), Model* model = nullptr);
private:
    enum {SkipDownload=-1};
    QGridLayout* lay;
    QPushButton* b_start;
    QPushButton* b_cancel;
    std::vector<MapSource*> map_src;
    /// zoom level and it's radio button in selection box
    std::vector<std::vector<std::pair<int,QRadioButton*>>> rb_zoom;
    std::map<int, QCheckBox*> apply_guru;
    Model* model;
public slots:
    void openDialog();
    void done(int r) override;
};

#endif // DOWNLOADMAP_H
