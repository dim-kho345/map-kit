#include "mainwindow.h"
#include "service/wiki2wpt.h"
#include "path_resolver.h"


void MainWindow::launchWiki2Wpt()
{
    auto ms = model->currentState();

    std::string err_txt[4]{"место, ","HLG, ","сетка, "};
    int error = (ms.location.empty())<<0 | (!ms.region)<<1 | (!ms.grid_ready)<<2;

    if(error)
    {
        std::string err("Не определены: ");
        for(auto i=0;i<3;i++)
            if(error & 1<<i)
                err.append(err_txt[i]);
        // убать последнюю запятую и пробел
        err.pop_back();
        err.pop_back();
        onUserMessage(GUI::Warning,err);
        return;
    }

    PathResolver path(ms.kit_date, ms.location);
    Wiki2WptExecutor wiki(this);
    wiki.region = ms.region;
    wiki.location = ms.location;
    wiki.grid_step = ms.grid_step;
    wiki.kit_date = ms.kit_date;
    wiki.pnt_path_sat = path.sourcePath(PathResolver::WikiPntSat);
    wiki.pnt_path_topo = path.sourcePath(PathResolver::WikiPntTopo);
    wiki.pnt_path_gpi = path.sourcePath(PathResolver::WikiPntGpi);
    wiki.pnt_path_gpx = path.sourcePath(PathResolver::WikiPntGpx);
    wiki.start();
    if(wiki.failed())
        onUserMessage(Critical, "Точки не сформированы");
    else
        model->setWikiPnt(wiki.points());
    //std::unique_ptr<ServiceWiki2Wpt>();
}
