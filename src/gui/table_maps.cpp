#include "mainwindow.h"
#include "table_maps.h"
#include "data_roles.h"
#include <QStyle>
#include <QDebug>

TableMapsDelegate::TableMapsDelegate(QObject* parent)
    :QStyledItemDelegate (parent)
{

}

void TableMapsDelegate::paint(QPainter *painter,
                              const QStyleOptionViewItem &opt,
                              const QModelIndex &index) const
{

    if(index.column()==0)
    {
        if(index.data(Qt::ProgressRole).toInt()!=GUI::NoProgress)
        {
            QStyleOptionProgressBar popt;
            popt.rect=opt.rect;
            popt.progress=index.data(Qt::ProgressRole).toInt();
            popt.minimum=0;
            popt.maximum=100;
            popt.text=index.data(Qt::DisplayRole).toString() +
                    ": " +
                    index.data(Qt::ProgressMessageRole).toString();
            popt.textVisible=true;
            popt.textAlignment=Qt::AlignHCenter;
            QApplication::style()->drawControl(QStyle::CE_ProgressBar,&popt,painter);
        }
        else
            QStyledItemDelegate::paint(painter,opt,index);
    }
    else
    {   
        QStyleOptionButton bopt;
        QPoint c=opt.rect.center();
        bopt.rect=QRect(c.x()-5,c.y()-5,10,10);
        bopt.state = QStyle::State_Enabled |
                ((index.data(Qt::CheckStateRole)==Qt::Checked) ?
                    QStyle::State_On :
                    QStyle::State_Off);

        painter->save();
        painter->setBrush(index.data(Qt::BackgroundRole).value<QColor>());
        painter->setPen(Qt::NoPen);
        painter->drawRect(opt.rect);
        painter->restore();

        // В колонке "ozf2" нет смысла в выборе конвертировать/нет, он всегда должен присутствовать
        if(index.column()!=1)
            QApplication::style()->drawControl(QStyle::CE_CheckBox,&bopt,painter);

    }
}


void MainWindow::mapCellClicked(int row, int col)
{
    auto item = tb_maps->item(row,col);
    switch(col)
    {
        case 0:
        // if ongoing work, cancel it
        if(item->data(Qt::ProgressRole).toInt() != GUI::NoProgress)
            model->abortService(item->data(Qt::JobContextRole).toInt());
        break;
    case 1:
        break;
    case 2:
        model->setKmzMap(tb_maps->item(row,0)->data(Qt::ContextRole).toInt());
        break;
    case 3:
        model->setJnxMap(tb_maps->item(row,0)->data(Qt::ContextRole).toInt());
        break;
    case 4:
        model->setGuruTopo(tb_maps->item(row,0)->data(Qt::ContextRole).toInt());
        break;
    case 5:
        model->setGuruSat(tb_maps->item(row,0)->data(Qt::ContextRole).toInt());
        break;

    }
}

void MainWindow::deleteMap()
{
    auto index = tb_maps->currentIndex();
    if(index.column()!=0)
        return;
    model->deleteMap(index.data(Qt::ContextRole).toInt());
}

void MainWindow::convert2Jnx()
{
    auto index = tb_maps->currentIndex();
    if(index.column()!=0)
        return;
    model->requestConvert2Jnx(index.data(Qt::ContextRole).toInt());
}

void MainWindow::convert2Kmz()
{
    auto index = tb_maps->currentIndex();
    if(index.column()!=0)
        return;
    model->requestConvert2Kmz(index.data(Qt::ContextRole).toInt());
}

void MainWindow::convert2Ozf()
{
    auto index = tb_maps->currentIndex();
    if(index.column()!=0)
        return;
    model->requestConvert2Ozf(index.data(Qt::ContextRole).toInt());
}

void MainWindow::convert2Guru()
{
    auto index = tb_maps->currentIndex();
    if(index.column()!=0)
        return;
    model->requestConvert2Guru(index.data(Qt::ContextRole).toInt());
}


void MainWindow::enhanceImage()
{
    auto index = tb_maps->currentIndex();
    if(index.column()!=0)
        return;
    model->requestEnhanceImage(index.data(Qt::ContextRole).toInt());
}
