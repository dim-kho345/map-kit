#ifndef MAPNAVIGATOR_H
#define MAPNAVIGATOR_H
#include "ui_navigator.h"
#include "crs_image.h"
#include "model/model.h"

class MapNavigator : public QWidget, public Ui_MapNavigator
{
    Q_OBJECT
public:
    MapNavigator(ModelInterface* model,  QWidget* parent=nullptr);

signals:
    void enhance(int ahe, int gamma);
public slots:
    void onModelUpdated(ModelInterface::ModelState& ms);
    void setGrid(UTMRegion reg, int step, std::pair<unsigned, unsigned> first_point_name);
private:

    struct MapIcon
    {
        QString path;
        ImageCRS im_crs;
        EnhanceOptions enh;
    };

    std::map<int,MapIcon> icon;
    ModelInterface* model;
    enum SelectionMode{None,SelectGrid,Select4Group,SelectLocation};
    SelectionMode mode{None};
    QGraphicsScene* scene;
    QPixmap *pix_map{nullptr};

    QGraphicsPixmapItem* pix_item{nullptr};

    /// Основная область печати
    QGraphicsRectItem* print_area{nullptr};

    //std::vector<QGraphicsRectItem*> poi_item;
    //std::vector<QGraphicsTextItem*> poi_text_item;

    bool first_map_arrived{false};

    QPen print_area_pen;
    QPen map4grp_pen;

    uint32_t grid_point_count{510};
    uint32_t grid_width{25};
    uint32_t grid_height{20};
    float grid_aspect{25.0f/20.0f};
    int32_t grid_step;
    /// центральная выбранная точка сетки
    QPointF grid_base;
    /// выбранная сетка
    UTMRegion grid;
    /// возвращемое "специальное" значение
    static constexpr ModelInterface::GridOrigin grid_orig_invalid{-1,-1};

    /// Масштабы карты для групп, метров/см
    std::vector<int>scale4group{50,75,100,125,150,180,200,250,300,
                                325,350,375,400,425,450,470,500};
    std::vector<int>::iterator sc4grp=scale4group.begin()+7;
    /// Ориентация карты для групп
    bool map4grp_rotated{false};

    /// последнее положение мыши
    QPointF mouse_pos;

    /// Преобразовать координаты QGraphicsScene
    UTMPoint pointToUTM(QPointF p, int force_zone=-1);
    QPointF pointFromUTM(UTMPoint p);
    UTMRegion rectToUTM(QRectF r);
    GeoRegion rectToLL(QRectF r);
    GeoPoint pointToLL(QPointF p);
    QPointF pointFromLL(GeoPoint p);

    /// Прямоугольник для границ UTM сетки
    QPolygonF gridPolygon(QPointF p);
    QGraphicsPolygonItem* grid_item{nullptr};

    MapIcon& currentIcon();
    /// Прямоугольник карты для групп
    QRectF map4GrpRect(QPointF p);
    QGraphicsRectItem* map4grp_item{nullptr};
    MapIcon* current_icon;

    ModelInterface::GridOrigin gridOriginFromText(QString text);
    void removePixFromScene();

   // Enhancer* enh_worker;
   // QThread enh_thread;
    bool enhancer_busy{false};
    bool enhance_request{false};

    /** Индекс карты, которая в процессе улучшения, в списке карт.
     Если перед enhancerFinished() изображение сменили,
     загружать его на экран уже не надо
     */
    //int enhance_idx;
private slots:
    void updateImage(int row);
    void selectMap4Group();
    void selectGrid();
    void enhancerFinished();
    void selectLocation();
    void hoverPoint(QPointF p);
    void pointSelected(QPointF p);
    void gridOriginEdited(const QString& text);
    void printAreaSelected(QRectF area);
    void imageSettingsChanged();
    void assignMap4Group(int row);
protected:
    void keyPressEvent(QKeyEvent* ev) override;


};

#endif // MAPNAVIGATOR_H
