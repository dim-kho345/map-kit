#ifndef TABLE_MAPS_H
#define TABLE_MAPS_H
#include <QStyledItemDelegate>


class TableMapsDelegate : public QStyledItemDelegate
{
public:
    TableMapsDelegate(QObject* parent=nullptr);
    void paint(QPainter* painter,
               const QStyleOptionViewItem& opt,
               const QModelIndex& index) const override;
};

#endif // TABLE_MAPS_H
