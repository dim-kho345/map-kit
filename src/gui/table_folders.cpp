#include "mainwindow.h"
#include "data_roles.h"


void MainWindow::zipFolders()
{
    auto folder = tb_folders->selectedItems();
    for(auto f : folder)
        model->requestZipFolder(f->data(Qt::ContextRole).toInt());
}
