#ifndef DIALOGSETTINGS_H
#define DIALOGSETTINGS_H
#include "ui_dialog_settings.h"
#include "settings.h"

class DialogSettings : public QDialog, public Ui_Dialog
{
    Q_OBJECT
public:
    DialogSettings(QWidget *parent=nullptr);
    bool settingsChanged() const {return change_accepted;}
public slots:
    void accept() override;
    void selectKitRootPath();
    void selectSASCachePath();
private:
    bool settings_changed{false};
    bool change_accepted{false};
};

#endif // DIALOGSETTINGS_H
