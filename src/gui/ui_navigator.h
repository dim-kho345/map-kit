/********************************************************************************
** Form generated from reading UI file 'navigatorQGHLQP.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef NAVIGATORQGHLQP_H
#define NAVIGATORQGHLQP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>
#include "map_view.h"

QT_BEGIN_NAMESPACE

class Ui_MapNavigator
{
public:
    QGridLayout *gridLayout;
    QListWidget *list;
    QComboBox *cb_map4grp;
    QSlider *horizontalSlider_3;
    QPushButton *b_coord2;
    QSlider *horizontalSlider;
    QPushButton *b_500m;
    QPushButton *b_250m;
    QPushButton *b_print_area;
    QPushButton *b_map4grp;
    QPushButton *b_1000m;
    QSlider *horizontalSlider_2;
    QSlider *horizontalSlider_4;
    MapView *map_view;
    QLineEdit *le_grid_origin;
    QLabel *lab_scale;

    void setupUi(QWidget *MapNavigator)
    {
        if (MapNavigator->objectName().isEmpty())
            MapNavigator->setObjectName(QString::fromUtf8("MapNavigator"));
        MapNavigator->resize(912, 601);
        gridLayout = new QGridLayout(MapNavigator);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        list = new QListWidget(MapNavigator);
        list->setObjectName(QString::fromUtf8("list"));

        gridLayout->addWidget(list, 1, 3, 4, 1);

        cb_map4grp = new QComboBox(MapNavigator);
        cb_map4grp->setObjectName(QString::fromUtf8("cb_map4grp"));

        gridLayout->addWidget(cb_map4grp, 2, 2, 1, 1);

        horizontalSlider_3 = new QSlider(MapNavigator);
        horizontalSlider_3->setObjectName(QString::fromUtf8("horizontalSlider_3"));
        horizontalSlider_3->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_3, 3, 4, 1, 1);

        b_coord2 = new QPushButton(MapNavigator);
        b_coord2->setObjectName(QString::fromUtf8("b_coord2"));

        gridLayout->addWidget(b_coord2, 4, 0, 1, 1);

        horizontalSlider = new QSlider(MapNavigator);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider, 1, 4, 1, 1);

        b_500m = new QPushButton(MapNavigator);
        b_500m->setObjectName(QString::fromUtf8("b_500m"));

        gridLayout->addWidget(b_500m, 2, 0, 1, 1);

        b_250m = new QPushButton(MapNavigator);
        b_250m->setObjectName(QString::fromUtf8("b_250m"));
        b_250m->setStyleSheet(QString::fromUtf8(""));

        gridLayout->addWidget(b_250m, 1, 0, 1, 1);

        b_print_area = new QPushButton(MapNavigator);
        b_print_area->setObjectName(QString::fromUtf8("b_print_area"));

        gridLayout->addWidget(b_print_area, 1, 1, 1, 1);

        b_map4grp = new QPushButton(MapNavigator);
        b_map4grp->setObjectName(QString::fromUtf8("b_map4grp"));

        gridLayout->addWidget(b_map4grp, 2, 1, 1, 1);

        b_1000m = new QPushButton(MapNavigator);
        b_1000m->setObjectName(QString::fromUtf8("b_1000m"));

        gridLayout->addWidget(b_1000m, 3, 0, 1, 1);

        horizontalSlider_2 = new QSlider(MapNavigator);
        horizontalSlider_2->setObjectName(QString::fromUtf8("horizontalSlider_2"));
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_2, 2, 4, 1, 1);

        horizontalSlider_4 = new QSlider(MapNavigator);
        horizontalSlider_4->setObjectName(QString::fromUtf8("horizontalSlider_4"));
        horizontalSlider_4->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_4, 4, 4, 1, 1);
        map_view = new MapView(MapNavigator);
        map_view->setObjectName(QString::fromUtf8("map_view"));
        map_view->setStyleSheet(QString::fromUtf8(""));
        map_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        map_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        map_view->setRenderHints(QPainter::Antialiasing|QPainter::HighQualityAntialiasing|QPainter::SmoothPixmapTransform|QPainter::TextAntialiasing);
        map_view->setDragMode(QGraphicsView::ScrollHandDrag);

        gridLayout->addWidget(map_view, 0, 0, 1, 5);

        le_grid_origin = new QLineEdit(MapNavigator);
        le_grid_origin->setObjectName(QString::fromUtf8("le_grid_origin"));

        gridLayout->addWidget(le_grid_origin, 4, 1, 1, 1);

        lab_scale = new QLabel(MapNavigator);
        lab_scale->setObjectName(QString::fromUtf8("lab_scale"));

        gridLayout->addWidget(lab_scale, 3, 1, 1, 1);

        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 1);
        gridLayout->setColumnStretch(2, 2);
        gridLayout->setColumnStretch(3, 4);
        gridLayout->setColumnStretch(4, 2);

        retranslateUi(MapNavigator);

        QMetaObject::connectSlotsByName(MapNavigator);
    } // setupUi

    void retranslateUi(QWidget *MapNavigator)
    {
        MapNavigator->setWindowTitle(QCoreApplication::translate("MapNavigator", "Form", nullptr));
        b_coord2->setText(QCoreApplication::translate("MapNavigator", "\320\227\320\260\320\264\320\260\321\202\321\214 \321\202\320\276\321\207\320\272\321\203", nullptr));
        b_500m->setText(QCoreApplication::translate("MapNavigator", "500\320\274", nullptr));
        b_250m->setText(QCoreApplication::translate("MapNavigator", "250\320\274", nullptr));
        b_print_area->setText(QCoreApplication::translate("MapNavigator", "\320\236\320\261\320\273\320\260\321\201\321\202\321\214 \320\277\320\265\321\207\320\260\321\202\320\270", nullptr));
        b_map4grp->setText(QCoreApplication::translate("MapNavigator", "\320\232\320\260\321\200\321\202\320\260 \320\264\320\273\321\217 \320\263\321\200\321\203\320\277\320\277", nullptr));
        b_1000m->setText(QCoreApplication::translate("MapNavigator", "1000\320\274", nullptr));
        lab_scale->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MapNavigator: public Ui_MapNavigator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // NAVIGATORQGHLQP_H
