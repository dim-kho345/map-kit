#include <algorithm>
#include <QEvent>
#include <QMessageBox>
#include "mainwindow.h"
#include "print_options.h"
#include "data_roles.h"

void MainWindow::onModelUpdated(int what)
{
    auto ms = model->currentState();

    //if (what & UpdatedCommon)
    {
        lab_grid_exist->setText(grid_avail_text[ms.grid_ready]);
        lab_jnx_exist->setText(jnx_avail_text[ms.jnx_source==Model::NoSource ? 0 : 1]);
        lab_kmz_exist->setText(kmz_avail_text[ms.kmz_source==Model::NoSource ? 0 : 1]);
        lab_wiki_exist->setText(wiki_avail_text[ms.wikipnt_ready]);
        lab_coord2_exist->setText(coord2_avail_text[ms.coord2_ready]);
        lab_map4grp_exist->setText(map4grp_avail_text[ms.map4grp]);
        lab_print_area_exist->setText(print_area_text[ms.print_area]);
    }

    int row = 0;
    if(what & UpdatedMaps)
    {

        std::vector<int> maps_to_add;

        // O(n2), but simple and clear
        // Determine maps to add from model, not found in GUI
        for (auto it : ms.map)
        {
            if(tableItem(tb_maps, it.first)==nullptr)
                maps_to_add.push_back(it.first);

        }

        // remove maps which exist no more in model
        for (row=0; row<tb_maps->rowCount();)
        {
            auto tb_id=tb_maps->item(row,0)->data(Qt::ContextRole).toInt();
            if (std::find_if(ms.map.cbegin(),
                             ms.map.cend(),
                             [tb_id](auto model_map_item)->bool{
                             return model_map_item.first == tb_id;
        }) == ms.map.cend())
                tb_maps->removeRow(row);
            else
                ++row;
        }

        if(maps_to_add.size())
        {
            auto row = tb_maps->rowCount();
            tb_maps->setRowCount(row + static_cast<int>(maps_to_add.size()));

            for(auto id : maps_to_add)
            {
                for(int i=0;i<tb_maps->columnCount();i++)
                {
                    tb_maps->setItem(row,i,new QTableWidgetItem);
                    tb_maps->item(row,i)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
                }
                tb_maps->item(row,0)->setData(Qt::ContextRole, id);
                tb_maps->item(row,0)->setData(Qt::ProgressRole, -1);
                tb_maps->item(row++,0)->setData(Qt::JobContextRole, -1);
            }

        }
        for(auto row=0; row < tb_maps->rowCount(); row++)
        {
            auto map_id = tb_maps->item(row,0)->data(Qt::ContextRole).toInt();
            tb_maps->item(row,0)->setData(Qt::DisplayRole, QString::fromStdString(ms.map.at(map_id).nameFull()));
            tb_maps->item(row,1)->setData(Qt::ItemDataRole::BackgroundRole, ms.map.at(map_id).ozf_ready ?
                                              QColor(Qt::green) : QColor(Qt::yellow));
            tb_maps->item(row,2)->setData(Qt::BackgroundRole, ms.map.at(map_id).kmz_ready ?
                                              QColor(Qt::green) : QColor(Qt::yellow));
            tb_maps->item(row,3)->setData(Qt::BackgroundRole, ms.map.at(map_id).jnx_ready ?
                                              QColor(Qt::green) : QColor(Qt::yellow));
            tb_maps->item(row,4)->setData(Qt::BackgroundRole, ms.map.at(map_id).guru_ready ?
                                              QColor(Qt::green) : QColor(Qt::yellow));
            tb_maps->item(row,5)->setData(Qt::BackgroundRole, ms.map.at(map_id).guru_ready ?
                                              QColor(Qt::green) : QColor(Qt::yellow));

            tb_maps->item(row,2)->setData(Qt::CheckStateRole, ms.kmz_source == map_id ? Qt::Checked : Qt::Unchecked);
            tb_maps->item(row,3)->setData(Qt::CheckStateRole, ms.jnx_source == map_id ? Qt::Checked : Qt::Unchecked);
            tb_maps->item(row,4)->setData(Qt::CheckStateRole, ms.guru_topo_source == map_id ? Qt::Checked : Qt::Unchecked);
            tb_maps->item(row,5)->setData(Qt::CheckStateRole, ms.guru_sat_source == map_id ? Qt::Checked : Qt::Unchecked);
        }
    }



    if(what & UpdatedFolders)
    {
        std::vector<int> folders_to_add;

        // Determine folders to add from model, not found in GUI
        for (auto it : ms.folder)
        {
            if(tableItem(tb_folders, it.first)==nullptr)
                folders_to_add.push_back(it.first);

        }

        // remove folders which exist no more in model
        for (row=0; row<tb_folders->rowCount();)
        {
            auto tb_id=tb_folders->item(row,0)->data(Qt::ContextRole).toInt();
            if (std::find_if(ms.folder.cbegin(),
                             ms.folder.cend(),
                             [tb_id](auto model_folder_item)->bool{
                             return model_folder_item.first == tb_id;
        }) == ms.folder.cend())
                tb_folders->removeRow(row);
            else
                ++row;
        }


        if(folders_to_add.size())
        {
            auto row = tb_folders->rowCount();
            tb_folders->setRowCount(row + static_cast<int>(folders_to_add.size()));

            for(auto id : folders_to_add)
            {
                tb_folders->setItem(row,0,new QTableWidgetItem);
                tb_folders->item(row,0)->setFlags(Qt::ItemFlag::ItemIsEnabled |
                                                  Qt::ItemIsSelectable);
                tb_folders->item(row,0)->setData(Qt::ContextRole, id);
                tb_folders->item(row,0)->setData(Qt::ProgressRole, -1);
                tb_folders->item(row++,0)->setData(Qt::JobContextRole, -1);
            }
        }



        for(auto row=0; row < tb_folders->rowCount(); row++)
        {
            auto folder_id = tb_folders->item(row,0)->data(Qt::ContextRole).toInt();
            tb_folders->item(row,0)->setData(Qt::DisplayRole,
                                             QString::fromStdString(ms.folder.at(folder_id).name));
            /*tb_folders->item(row,0)->setData(Qt::BackgroundRole,
                                             ms.folder.at(folder_id).ready_to_zip ?
                                                 QColor("#e0ffe0") : QColor("#ffd0d0"));*/
            tb_folders->item(row,0)->setData(Qt::CheckStateRole, ms.folder.at(folder_id).zipped);
        }
    }




    if(what & UpdatedPrintout)
    {
        std::vector<int> print_to_add;

        // Determine printout to add from model, not found in GUI
        for (auto it : ms.print)
        {
            if(tableItem(tb_printout, it.first)==nullptr)
                print_to_add.push_back(it.first);

        }

        // remove printout which exist no more in model
        for (row=0; row<tb_printout->rowCount();)
        {
            auto tb_id=tb_printout->item(row,0)->data(Qt::ContextRole).toInt();
            if (std::find_if(ms.print.cbegin(),
                             ms.print.cend(),
                             [tb_id](auto model_print_item)->bool{
                             return model_print_item.first == tb_id;
        }) == ms.print.cend())
                tb_printout->removeRow(row);
            else
                ++row;
        }


        if(print_to_add.size())
        {
            auto row = tb_printout->rowCount();
            tb_printout->setRowCount(row + static_cast<int>(print_to_add.size()));

            for(auto id : print_to_add)
            {
                for(int i=0;i<tb_printout->columnCount();i++)
                {
                    tb_printout->setItem(row,i,new QTableWidgetItem);
                    tb_printout->item(row,i)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
                }
                tb_printout->item(row,0)->setData(Qt::ContextRole, id);
                tb_printout->item(row,0)->setData(Qt::ProgressRole, -1);
                tb_printout->item(row++,0)->setData(Qt::JobContextRole, -1);
            }
        }


        for(auto row=0; row < tb_printout->rowCount(); row++)
        {
            auto& print_item = ms.print.at(tb_printout->item(row,0)->data(Qt::ContextRole).toInt());
            QString label = QString::fromStdString(ms.map.at(print_item.map_id).nameFull());
            label.append(QString("_A%1").arg(print_item.format));
            switch(print_item.type)
            {
            case PrintType::Whole:
                break;
            case PrintType::Splitted:
                label.append("_склейка");
                break;
            case PrintType::Map4Group:
                label.append("_для групп");
            }
            tb_printout->item(row,0)->setData(Qt::DisplayRole,label);
        }
    }
    navigator->onModelUpdated(ms);
}

void MainWindow::onProgress(ServiceFunction func, int model_id, int job_id, int progress, std::string message)
{
    if (func==ServiceFunction::ZipFolder)
    {

    }

    if (func==ServiceFunction::DownloadMap || func==ServiceFunction::Stitch    ||
           func==ServiceFunction::Conv2Ozf || func==ServiceFunction::Conv2Jnx  ||
           func==ServiceFunction::Conv2Kmz || func==ServiceFunction::Conv2Guru ||
           func==ServiceFunction::Enhance)
    {
        auto item = tableItem(tb_maps, model_id);
        if(item != nullptr)
        {
            item->setData(Qt::ProgressRole, progress);
            item->setData(Qt::ProgressMessageRole, QString::fromStdString(message));
            item->setData(Qt::JobContextRole, job_id);
        }
    }
}

void MainWindow::onUserMessage(UserMessageType type, std::string what)
{
    switch (type)
    {
    case Info:
        //statusBar()->setFont(QFont("Arial",-1,QFont::Weight::Normal));
        //statusBar()->showMessage(QString::fromStdString(what), 5000);
        te_status->appendPlainText(QString::fromStdString(what));
        break;
    case Warning:
        //statusBar()->setFont(QFont("Arial",-1,QFont::Weight::Bold));
        //statusBar()->showMessage(QString::fromStdString(what), 5000);
        te_warnings->appendPlainText(QString::fromStdString(what));
        break;
    case Critical:
        QMessageBox::critical(this, "Ошибка", QString::fromStdString(what), QMessageBox::Ok);
    }
}

