#include <algorithm>
#include <set>
#include <QDebug>
#include <QResizeEvent>
#include <QMessageBox>
#include "map_navigator.h"
#include "data_roles.h"

MapNavigator::MapNavigator(ModelInterface* model, QWidget* parent):
    QWidget(parent), model(model)
{
    setupUi(this);

    scene = new QGraphicsScene(this);
    map_view->setScene(scene);
    map_view->setMode(MapView::Navigate);

    print_area_pen.setCosmetic(true);
    print_area_pen.setColor(QColor(255,0,0));
    map4grp_pen.setCosmetic(true);
    map4grp_pen.setColor(QColor(0,0,255,128));

    le_grid_origin->setText("A1");
    le_grid_origin->setValidator(new QRegExpValidator(QRegExp("[A-Z]\\d{1,2}"),this));

    /*enh_worker = new Enhancer;
    enh_worker->moveToThread(&enh_thread);
    connect(&enh_thread, &QThread::finished, enh_worker, &QObject::deleteLater);
    connect(enh_worker, &Enhancer::finished, this, &MapNavigator::enhancerFinished);
    connect(this,&MapNavigator::enhancerSetImage,enh_worker,&Enhancer::setImage);
    connect(this,&MapNavigator::enhance,enh_worker,&Enhancer::run);
    enh_thread.start();*/
    connect(list,&QListWidget::currentRowChanged,this,&MapNavigator::updateImage);
    connect(b_print_area,&QPushButton::clicked,[=](){if(pix_item) map_view->setMode(MapView::Select);});
    connect(map_view,&MapView::areaSelected,this,&MapNavigator::printAreaSelected);
    connect(map_view,&MapView::pointSelected,this,&MapNavigator::pointSelected);
    connect(map_view,&MapView::hoverPoint,this,&MapNavigator::hoverPoint);
    connect(b_250m,&QPushButton::clicked,this,[&](){ grid_step=250;selectGrid();});
    connect(b_500m,&QPushButton::clicked,this,[&](){ grid_step=500;selectGrid();});
    connect(b_1000m,&QPushButton::clicked,this,[&](){ grid_step=1000;selectGrid();});
    connect(b_map4grp,&QPushButton::clicked,this,&MapNavigator::selectMap4Group);
    connect(cb_map4grp, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MapNavigator::assignMap4Group);
    connect(b_coord2,&QPushButton::clicked,this,&MapNavigator::selectLocation);
    connect(le_grid_origin, &QLineEdit::textEdited, this, &MapNavigator::gridOriginEdited);
    //connect(sl_ahe,&QSlider::valueChanged,this,&MapNavigator::imageSettingsChanged);
    //connect(sl_gamma,&QSlider::valueChanged,this,&MapNavigator::imageSettingsChanged);
};


void MapNavigator::onModelUpdated(ModelInterface::ModelState &ms)
{
    using namespace std;

    //remove maps which have been deleted from model
    vector<int> model_maps, maps_to_add;
    set<int> this_maps;
    for(auto m : ms.map)
        model_maps.emplace_back(m.first);
    for(auto row=0; row<list->count(); row++)
        this_maps.emplace(list->item(row)->data(Qt::ContextRole).toInt());

    set_difference(model_maps.cbegin(), model_maps.cend(),
                   this_maps.cbegin(), this_maps.cend(),
                   back_inserter(maps_to_add));

    //add maps not present in list
    for(auto map_id : maps_to_add)
    {
        auto& m = ms.map.at(map_id);
        if(!m.img_ready)
            continue;
        auto p = new QListWidgetItem(QString::fromStdString(m.nameFull()));
        p->setData(Qt::ContextRole,map_id);
        list->addItem(p);
        cb_map4grp->addItem(QString::fromStdString(m.nameFull()),map_id);

        ImageCRS crs(ms.region, CRS::EPSG4326, m.icon_width, m.icon_height, CRS::crsFromString(m.source->crs));
        icon.emplace(map_id,MapIcon{QString::fromStdString(m.icon_path),
                                    crs,
                                    EnhanceOptions()});
    }
    int row{0};

    for(;;)
    {

        if(row == list->count())
            break;
        auto entry = list->item(row);
        auto map_id = entry->data(Qt::ContextRole).toInt();

        if(std::find(model_maps.cbegin(),model_maps.cend(),map_id)==model_maps.cend())
        {
            if(list->currentRow() == row)
                removePixFromScene();
            cb_map4grp->removeItem(row);
            icon.erase(map_id);
            delete entry;
        }
        else
            ++row;
    }
    if(list->count() && list->currentRow()==-1)
        list->setCurrentRow(0);
}


void MapNavigator::removePixFromScene()
{
    if(pix_item)
    {
        scene->removeItem(pix_item);
        delete pix_item;
        delete pix_map;
        /*pix_item->~QGraphicsPixmapItem();
        pix_map->~QPixmap();*/
        pix_item=nullptr;pix_map=nullptr;
    }
}

void MapNavigator::updateImage(int row)
{
    if(row==-1)
        return;
    auto list_item = list->item(row);//list->currentItem();
    if(list_item == nullptr)
        return;
    auto map_id = list_item->data(Qt::ContextRole).toInt();
    current_icon = &(icon.at(list->currentItem()->data(Qt::ContextRole).toInt()));
    // гибриды не отображаем
    /*if(map.map->getType()==MapType::Hybrid)
    {
        removePixFromScene();
        return;
    }*/


    QString path = icon.at(map_id).path;

    QImage im(path);
    if(im.isNull()) return;
    if(!pix_map)
    {
        pix_map=new QPixmap;
        pix_item=scene->addPixmap(*pix_map);
    }
    pix_map->convertFromImage(im);
    pix_item->setPixmap(*pix_map);

    uint32_t w = static_cast<uint32_t>(pix_map->width());
    uint32_t h = static_cast<uint32_t>(pix_map->height());
    scene->setSceneRect(0,0,w,h);


    //if(map.map->getType()==MapType::Sat)
    {
        /*      sl_ahe->setEnabled(true);
        sl_gamma->setEnabled(true);
        enhance_idx=row;
        emit enhancerSetImage(&map);*/
        imageSettingsChanged();
    }

    if(!first_map_arrived)
    {
        first_map_arrived=true;
        printAreaSelected(QRectF(0,0,w,h));
    }
    if(print_area)
        print_area->setZValue(1);
    if(grid_item)
        grid_item->setPolygon(gridPolygon(grid_base));
}

void MapNavigator::printAreaSelected(QRectF area)
{
    map_view->setMode(MapView::Navigate);
    if(print_area)
    {
        scene->removeItem(print_area);
        print_area->~QGraphicsRectItem();
    }
    QRect a=area.normalized().toAlignedRect();
    if(!pix_map)
        return;
    if(a.left() < 0)
        a.setLeft(0);
    if(a.top() < 0)
        a.setTop(0);
    if(a.right() >= pix_map->width())
        a.setRight(pix_map->width()-1);
    if(a.bottom() >= pix_map->height())
        a.setBottom(pix_map->height()-1);
    print_area=scene->addRect(a,print_area_pen);
    model->setPrintRegion({ pointToLL(a.topLeft()),
                            pointToLL(a.bottomRight())});
}


void MapNavigator::hoverPoint(QPointF p)
{
    mouse_pos=p;
    if(mode==Select4Group)
    {
        map4grp_item->setRect(map4GrpRect(p));
    }
    if(mode==SelectGrid)
    {
        grid_base = p;
        // хаха классическая ошибка вылетало пока не заключил в блок
        grid_item->setPolygon(gridPolygon(grid_base));
    }
}


void MapNavigator::pointSelected(QPointF p)
{
    if(mode==SelectLocation)
        model->setHQPoint(pointToLL(p));

    if(mode==Select4Group)
    {
        QRectF r=map4grp_item->rect();
        if(!pix_map || r.left()<0 || r.top() <0 || r.right() > (pix_map->width()-1) ||
                r.bottom()> (pix_map->height()-1))
        {
            QMessageBox::information(this,"","Выход за границы карты");
            assignMap4Group(-1);
        }
        else
        {
            assignMap4Group(cb_map4grp->currentIndex());
        }
    }

    if(mode==SelectGrid)
    {
        model->setGrid(grid,grid_step, gridOriginFromText( le_grid_origin->text() ));
    }
    mode=SelectionMode::None;
    map_view->setMode(MapView::Navigate);
}

void MapNavigator::setGrid(UTMRegion reg,
                           int step,
                           std::pair<unsigned, unsigned> first_point_name)
{
    char origin[8];
    sprintf_s(origin, sizeof(origin), "%c%i",'A'+first_point_name.first,first_point_name.second+1);
    grid_step=step;
    le_grid_origin->setText(QString::fromStdString(origin));
    QVector<QPointF> pol;

    /** @todo
   Зона определяется по верхней левой точке. но т.к. сетка в файле представлена просто Lon/Lat координатами,
   непонятно, в какую зону их относить. тогда берем зону левой верхней точки, и пробуем соседние зоны:
   какое расстояние будет от верхней левой точки до одной из узловых зон. если 50м, явно не та зона выбрана.
   А если пара метров - ок.
*/
    grid_base = pointFromUTM(reg.nw);

    ///@todo это хак чтоб при округлении сетка встала на требуемую позицию, а не на шаг
    /// влево или вверх - сдвигаем точку на пару пикселей. как нибудь убрать его
    grid_base.setX(grid_base.x()+3);
    grid_base.setY(grid_base.y()+3);

    grid_step = step;
    grid_width = lround(reg.se.x-reg.nw.x)/grid_step;
    grid_height = lround(reg.nw.y-reg.se.y)/grid_step;

    if(grid_item)
        grid_item->setPolygon(gridPolygon(grid_base));
    else
        grid_item=scene->addPolygon(gridPolygon(grid_base),map4grp_pen);
    grid_item->setZValue(1);

    // по идее незачем, но пусть будет для синхронности
    grid=reg;

}


void MapNavigator::selectGrid()
{
    if(icon.empty() || !pix_item)
        return;
    mode=SelectGrid;
    map_view->setMode(MapView::HoverItem);
    if(!grid_item)
        grid_item=scene->addPolygon(gridPolygon(QPointF(0,0)),map4grp_pen);
    else
        grid_item->setPolygon(gridPolygon(QPointF(0,0)));
    grid_item->setZValue(1);
    setFocus();
}

void MapNavigator::selectMap4Group()
{
    if(icon.empty() || !pix_item)
        return;
    mode=Select4Group;
    map_view->setMode(MapView::HoverItem);
    if(!map4grp_item)
        map4grp_item=scene->addRect(map4GrpRect(QPointF(0,0)),map4grp_pen);
    else
        map4grp_item->setRect(map4GrpRect(QPointF(0,0)));
    // отобразить масштаб
    lab_scale->setText(tr("1:%1").arg(*sc4grp));
    map4grp_item->setZValue(1);
    setFocus();
}

void MapNavigator::selectLocation()
{
    if(icon.empty() || !pix_item)
        return;
    mode=SelectLocation;
    map_view->setMode(MapView::HoverItem);
    map_view->setCursor(Qt::CursorShape::CrossCursor);
}

/// @todo полигон отрисовывается от левой верхней точки, а то я с принадлежностью зон задолбался
QPolygonF MapNavigator::gridPolygon(QPointF p)
{
    QVector<QPointF> pol;

    auto base = pointToUTM(p, GeoCalc::utmZone(pointToLL(p)));
    double x=base.x-fmod(base.x,grid_step);
    double y=base.y+grid_step-fmod(base.y,grid_step);

    auto& zone = base.zone;

    UTMPoint nw = {x,y,zone};
    pol.push_back(pointFromUTM({x,y,zone}));

    x+=grid_width*grid_step;
    pol.push_back(pointFromUTM({x,y,zone}));
    y-=grid_height*grid_step;
    UTMPoint se = {x,y,zone};
    pol.push_back(pointFromUTM({x,y,zone}));
    x-=grid_width*grid_step;
    pol.push_back(pointFromUTM({x,y,zone}));
    y+=grid_height*grid_step;
    pol.push_back(pointFromUTM({x,y,zone}));

    grid={nw, se};
    return QPolygonF(pol);

}

QRectF MapNavigator::map4GrpRect(QPointF p)
{
    double h=10,w=14.4;
    if(map4grp_rotated)
    {h=14.4;w=10;}
    auto pix_res = currentIcon().im_crs.m_per_pix();
    return QRectF (QPointF(p.x() + pix_res*-w* *sc4grp,p.y() + pix_res*-h* *sc4grp),
                   QPointF(p.x() + pix_res*+w* *sc4grp,p.y() + pix_res*+h* *sc4grp));
}

void MapNavigator::keyPressEvent(QKeyEvent* ev)
{

    if(mode==Select4Group)
    {
        if(ev->key()==Qt::Key_Left || ev->key()==Qt::Key_Right )
            map4grp_rotated^=0x1;
        if(ev->key()==Qt::Key_Down && sc4grp>scale4group.begin())
            --sc4grp;
        if(ev->key()==Qt::Key_Up && sc4grp<scale4group.end()-1)
            ++sc4grp;
        if(ev->key()==Qt::Key_Escape)
        {
            ///@todo заключить эту последовательность в функцию, DRY MF

            assignMap4Group(-1);
            mode=None;
        }

        if(map4grp_item)
        {
            map4grp_item->setRect(map4GrpRect(mouse_pos));
            lab_scale->setText(tr("1:%1").arg(*sc4grp));
        }
        ev->accept();
        return;
    }

    if(mode==SelectGrid)
    {
        // x = y * aspect
        // x * y = PointCount  -> y=(PointCout/aspect)^0.5; x=PointCount/y;
        if(ev->key()==Qt::Key_Left && grid_aspect<2.0f)
            grid_aspect*=1.05f;
        if(ev->key()==Qt::Key_Right && grid_aspect>0.5f)
            grid_aspect/=1.05f;
        if(ev->key()==Qt::Key_Up && grid_point_count<990)
            grid_point_count+=30;
        if(ev->key()==Qt::Key_Down && grid_point_count>390)
            grid_point_count-=30;
        if(grid_point_count>990)
            grid_point_count=990;
        grid_height=static_cast<uint32_t>(
                    sqrt(static_cast<float>(grid_point_count)/grid_aspect));
        grid_width=grid_point_count/grid_height;

        if(ev->key()==Qt::Key_Escape)
        {
            scene->removeItem(grid_item);
            grid_item->~QGraphicsPolygonItem();
            grid_item=nullptr;
            model->setGrid(UTMRegion(), 0, ModelInterface::GridOrigin());
            mode=None;
        }
        if(grid_item)
            grid_item->setPolygon(gridPolygon(mouse_pos));
        ev->accept();
        return;
    }
    if(mode==SelectLocation)
    {
        if(ev->key()==Qt::Key_Escape)
        {
            map_view->setMode(MapView::Navigate);
            mode=None;
        }
        ev->accept();
        return;
    }
    QWidget::keyPressEvent(ev);
}

void MapNavigator::enhancerFinished()
{
    // до завершения работы может быть выбран гибрид
    // -> pix_item=nullptr
    //if(pix_item && enhance_idx==image_list->currentRow())
    {
        //enh_worker->getImage(pix_map);
        pix_item->setPixmap(*pix_map);
    }
    enhancer_busy=false;
    if(enhance_request)
    {
        enhance_request=false;
        enhancer_busy=true;
        //enhance_idx=image_list->currentRow();
        //emit enhance(sl_ahe->value(),sl_gamma->value());
    }
}

void MapNavigator::imageSettingsChanged()
{
    if(enhancer_busy)
    {
        enhance_request=true;
        return;
    }
    //enhance_idx=image_list->currentRow();
    enhancer_busy=true;
    //emit enhance(sl_ahe->value(),sl_gamma->value());
}

void MapNavigator::gridOriginEdited(const QString& text)
{
    auto orig = gridOriginFromText(text);
    if (grid && orig!=grid_orig_invalid)
        model->setGrid(grid, grid_step, orig);
}


UTMPoint MapNavigator::pointToUTM(QPointF p, int force_zone)
{
    auto& im_crs= currentIcon().im_crs;
    auto geopnt = im_crs.geoPoint(p.x(), p.y());

    GeoPoint res = CRS::convert(im_crs.epsg, CRS::EPSG4326, geopnt.x, geopnt.y);
    return res.toUTM(force_zone);
}

QPointF MapNavigator::pointFromUTM(UTMPoint p)
{
    auto& im_crs= currentIcon().im_crs;
    auto im_pnt = CRS::convert(CRS::EPSG_UTM+p.zone,
                               im_crs.epsg,
                               p.x,
                               p.y);
    auto res = im_crs.imgPoint(im_pnt.first,im_pnt.second);
    return {res.x, res.y};
}

GeoPoint MapNavigator::pointToLL(QPointF p)
{
    auto& im_crs= currentIcon().im_crs;
    auto geopnt = im_crs.geoPoint(p.x(), p.y());
    auto res = CRS::convert(im_crs.epsg,
                            CRS::EPSG4326,
                            geopnt.x,
                            geopnt.y);
    return {res.first,res.second};
}

QPointF MapNavigator::pointFromLL(GeoPoint p)
{
    auto& im_crs= currentIcon().im_crs;
    auto geopnt = CRS::convert(CRS::EPSG4326,
                               im_crs.epsg,
                               p.x,
                               p.y);
    auto img_pnt = im_crs.imgPoint(geopnt.first, geopnt.second);
    return {img_pnt.x,img_pnt.y};
}

MapNavigator::MapIcon& MapNavigator::currentIcon()
{
    //return icon.at(list->currentItem()->data(Qt::ContextRole).toInt());
    return *current_icon;
}


ModelInterface::GridOrigin MapNavigator::gridOriginFromText(QString text)
{
    auto first = text[0].toLatin1();
    if(first<'A' || first>'Z')
        return grid_orig_invalid;
    first-='A';
    auto second = text.mid(1).toUInt()-1;
    if(second<0 || second > 50)
        return grid_orig_invalid;
    return {first,second};
}

void MapNavigator::assignMap4Group(int row)
{
    if(!map4grp_item /*|| map_view->currentMode()!=MapView::Navigate*/)
        return;
    auto r = map4grp_item->rect();
    if(row!=-1)
        model->setMap4Grp({pointToLL(r.topLeft()),pointToLL(r.bottomRight())},
                      cb_map4grp->itemData(row,Qt::UserRole).toInt());
    else
    {
        model->setMap4Grp(GeoRegion(), -1);
        scene->removeItem(map4grp_item);
        map4grp_item->~QGraphicsRectItem();
        map4grp_item=nullptr;
    }
}
