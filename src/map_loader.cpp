#include "map_loader.h"
#include "ozi.h"
#include "path_resolver.h"
#include <opencv2/opencv.hpp>


void MapLoader::start()
{
    try {
    if(load(map_path, kit_region))
        finished(true);
    else
        finishedVerbose(false, GUI::Critical, "Error while "+stage);
    }
    catch (std::exception& ex) {
        finishedVerbose(false, GUI::Critical, "Exception while "+stage + ":\n" + ex.what());
    }
}

struct PointI
{
    PointI(const int x, const int y) : x(x), y(y) {}
    PointI(const Point& p) :
        x(lround(p.x)),y(lround(p.y))
    {}
    operator cv::Point() {return {x,y};}
    int x;
    int y;
};

bool MapLoader::load(std::string ozi_map, GeoRegion kit_reg)
{
    using namespace cv;
    OziMapHeader ozi;
    stage="loading file";
    if(!ozi.tryOpenFile(QString::fromStdString(ozi_map)))
    {
        stage += ": " + ozi.getError();
        return false;
    }
    auto inf = ozi.info();
    stage="determinating geo transforms";
    GeoPoint ozi_nw = CRS::convert(CRS::EPSG4326, CRS::EPSG3785,
                                   inf.reg.nw.x, inf.reg.nw.y);
    GeoPoint ozi_se = CRS::convert(CRS::EPSG4326, CRS::EPSG3785,
                                   inf.reg.se.x, inf.reg.se.y);

    GeoPoint kit_nw = CRS::convert(CRS::EPSG4326, CRS::EPSG3785,
                                   kit_reg.nw.x, kit_reg.nw.y);
    GeoPoint kit_se = CRS::convert(CRS::EPSG4326, CRS::EPSG3785,
                                   kit_reg.se.x, kit_reg.se.y);

    GeoPoint p1{std::max(ozi_nw.x,kit_nw.x),std::min(ozi_nw.y,kit_nw.y)};
    GeoPoint p2{std::min(ozi_se.x,kit_se.x),std::max(ozi_se.y,kit_se.y)};

    if(p1.x >= p2.x  || p1.y <= p2.y)
        return false;

    ImageCRS ozi_crs({ozi_nw, ozi_se}, CRS::EPSG3785, inf.size.width, inf.size.height, CRS::EPSG3785);

    auto w = lround((kit_se.x-kit_nw.x)/ozi_crs.resX());
    auto h = lround((kit_se.y-kit_nw.y)/ozi_crs.resY());

    ImageCRS new_crs({kit_nw, kit_se}, CRS::EPSG3785, w,h, CRS::EPSG3785);

    stage="creating window";
    Mat new_img(h,w,CV_8UC3);
    rectangle(new_img,{0,0},{w, h},Scalar(255,255,255),-1);

    PointI win_nw = new_crs.imgPointBounded(p1);
    PointI win_se = new_crs.imgPointBounded(p2);
    PointI src_nw = ozi_crs.imgPointBounded(p1);
    PointI src_se = ozi_crs.imgPointBounded(p2);

    // rounded window sizes should be kept the same, otherwise copyTo will allocate new Mat
    // which would not reference new_img
    Size win_size(std::min(win_se.x-win_nw.x, src_se.x-src_nw.x),
                  std::min(win_se.y-win_nw.y, src_se.y-src_nw.y));
    win_se = {win_nw.x + win_size.width,win_nw.y + win_size.height};
    src_se = {src_nw.x + win_size.width,src_nw.y + win_size.height};


    Mat new_win(new_img, Rect(cv::Point(win_nw.x, win_nw.y),
                              cv::Point(win_se.x, win_se.y)));
    stage="reading source";
    Mat ozi_img=imread(inf.img_path);
    Mat ozi_win(ozi_img, Rect(cv::Point(src_nw.x,src_nw.y),
                              cv::Point(src_se.x,src_se.y)));
    stage="copying source";
    ozi_win.copyTo(new_win);
    ozi_img.release();
    imwrite(img_path_out,new_img);
    img_width = w;
    img_height = h;
    Mat icon;
    double scale=icon_side_size/(std::max<double>(w,h));
    resize(new_img,icon,Size(0,0),scale,scale,INTER_AREA);
    new_img.release();
    imwrite(icon_path_out,icon);
    icon_width = icon.cols;
    icon_height = icon.rows;
    icon.release();
    return true;
}
