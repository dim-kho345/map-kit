#include <stdexcept>
#include "crs.h"

std::unordered_map<std::string, CRS::CoordRefSys> CRS::crs_table {
    {"EPSG3395", 3395},
    {"EPSG3785", 3785},
    {"EPSG4326", 4326},
};


std::unordered_map<CRS::Transform, std::unique_ptr<OGRCoordinateTransformation>> CRS::transform;

CRS::CoordRefSys CRS::crsFromString(const std::string &crs_name)
{
    return crs_table.at(crs_name);
}

std::pair<double, double> CRS::convert(CRS::CoordRefSys src,
                                         CRS::CoordRefSys dst,
                                         double x,
                                         double y)
{
    // If required transform does not exist, create dedicated object. Otherwise reuse existing
    auto it = transform.find({src,dst});
    if(it == transform.cend())
    {
        OGRSpatialReference src_crs;
        OGRSpatialReference dst_crs;
        auto err_src = src_crs.importFromEPSG(src);
        auto err_dst = dst_crs.importFromEPSG(dst);
        if(err_dst || err_src)
            throw(std::logic_error("Error creating spatial reference src/dst " + std::to_string(err_src) +
                                   '/'+std::to_string(err_dst)));

        // various GIS Projections have different lon/lan and x/y mappings. following call sets
        // strict lon-x ant lat-y mapping; read more in OSRAxisMappingStrategy description and GDAL docs
        src_crs.SetAxisMappingStrategy(OSRAxisMappingStrategy::OAMS_TRADITIONAL_GIS_ORDER);
        dst_crs.SetAxisMappingStrategy(OSRAxisMappingStrategy::OAMS_TRADITIONAL_GIS_ORDER);
        auto tr = std::unique_ptr<OGRCoordinateTransformation>(OGRCreateCoordinateTransformation(&src_crs, &dst_crs));
        it = transform.emplace(std::make_pair(src,dst), std::move(tr)).first;
    }
    double z{0};
    int res;
    it->second->Transform(1, &x, &y, &z, &res);
    if(res!=TRUE)
        throw (std::logic_error("Coordinate conversion: conversion failed"));
    return {x,y};
}
