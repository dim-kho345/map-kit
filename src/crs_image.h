#ifndef IMAGECRS_H
#define IMAGECRS_H
#include "geo.h"

using Point=GeoPoint;

struct ImgSize
{
    ImgSize() = default;
    ImgSize(unsigned w, unsigned h) :
        width(w), height(h) {}
    unsigned width;
    unsigned height;
};


/**
 * @brief Image georeference class
 */
class ImageCRS
{
public:

    /**
     * @brief Create georeference
     * @param r image corner coordinates given in CRS defined by epsg_of_region
     * @param epsg_of_region EPSG code of region
     * @param width image width
     * @param height image height
     * @param epsg EPSG code of CRS to which image would be bounded
     */
    ImageCRS(const GeoRegion r, CRS::CoordRefSys epsg_of_region,
             unsigned width, unsigned height, CRS::CoordRefSys epsg);

    /// get geopoint of given pixel coordinates
    GeoPoint geoPoint(double x,double y) const;
    /// get geopoint of given pixel coordinates and tranform it to \e dst CRS
    GeoPoint geoPoint(double x,double y, CRS::CoordRefSys dst) const;

    /// get pixel coordinates of given geopoint
    Point imgPoint(double x,double y) const;
    Point imgPoint(const Point& p) const;

    /// get pixel coordinates of geopoint given in \e src CRS coordinates
    Point imgPoint(const Point& p, CRS::CoordRefSys src) const;
    Point imgPointBounded(double x,double y) const;
    Point imgPointBounded(const Point& p) const;
    auto inline m_per_pix() {return resolution;}
    const CRS::CoordRefSys epsg;

    /// get resolution for X (lon) axis
    double resX() const{return dx;}
    /// get resolution for Y (lat) axis
    double resY() const{return dy;}
private:
    /// upper left corner in projection units
    double x0,y0;
    /// projection units per pix (not considering scale factor for non-transverse mercator)
    double dx,dy;
    double resolution;
    unsigned width;
    unsigned height;
};

#endif //IMAGECRS_H
