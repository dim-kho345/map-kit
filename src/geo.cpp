#include <stdint.h>
#include <sstream>
#include <limits>
#include "geo.h"

Tile::Tile(const GeoPoint pnt, int zoom, CRS::CoordRefSys crs)
{
    auto coord = CRS::convert(CRS::EPSG4326, crs, pnt.x, pnt.y);
    auto tx = coord.first;
    auto ty = coord.second;

    // convert from meter to pixel
    tx=(1+tx/R/PI)/2*(static_cast<uint32_t>(1<<zoom)*TILE_SIDE);
    ty=(1-ty/R/PI)/2*(static_cast<uint32_t>(1<<zoom)*TILE_SIDE);
    x=lround(tx);y=lround(ty);

    off_x = x%TILE_SIDE;
    off_y = y%TILE_SIDE;
    x = x/TILE_SIDE;
    y = y/TILE_SIDE;
}

GeoPoint Tile::toGeoPoint(int zoom, CRS::CoordRefSys tile_crs, CRS::CoordRefSys dst_crs) const
{
    double tx,ty;
    tx=static_cast<double>(x*TILE_SIDE+off_x) / (static_cast<unsigned>(1)<<zoom) /TILE_SIDE * 2;
    ty=static_cast<double>(y*TILE_SIDE+off_y) / (static_cast<unsigned>(1)<<zoom) /TILE_SIDE * 2;
    tx=(tx-1)*R*PI;
    ty=-(ty-1)*R*PI;

    auto coord = CRS::convert(tile_crs, dst_crs, tx, ty);
    return {coord.first, coord.second};
}

GeoPoint Tile::toGeoPoint(int zoom, CRS::CoordRefSys tile_crs) const
{
    return toGeoPoint(zoom, tile_crs, tile_crs);
}

Tile Tile::reproject(int zoom, CRS::CoordRefSys tile_crs, CRS::CoordRefSys dst_crs) const
{
    Tile res;
    double tx,ty;
    tx=static_cast<double>(x*TILE_SIDE+off_x) / (static_cast<unsigned>(1)<<zoom) /TILE_SIDE * 2;
    ty=static_cast<double>(y*TILE_SIDE+off_y) / (static_cast<unsigned>(1)<<zoom) /TILE_SIDE * 2;
    tx=(tx-1)*R*PI;
    ty=-((ty-1)*R*PI);

    auto c = CRS::convert(tile_crs, dst_crs, tx, ty);
    tx = c.first;
    ty = c.second;

    // convert from meter to pixel
    tx=(1+tx/R/PI)/2*(static_cast<uint32_t>(1<<zoom)*TILE_SIDE);
    ty=(1-ty/R/PI)/2*(static_cast<uint32_t>(1<<zoom)*TILE_SIDE);
    res.x=lround(tx);res.y=lround(ty);

    res.off_x = res.x % TILE_SIDE;
    res.off_y = res.y % TILE_SIDE;
    res.x = res.x / TILE_SIDE;
    res.y = res.y / TILE_SIDE;
    return res;
}

int Tile::raw_x()const
{
    return x*TILE_SIDE+off_x;
}

int Tile::raw_y()const
{
    return y*TILE_SIDE+off_y;
}

std::pair<int, int> Tile::offset(const Tile& rhs) const
{
    return {raw_x() - rhs.raw_x(), raw_y() - rhs.raw_y()};
}

bool GeoPoint::operator==(const GeoPoint& rhs) const
{
    //@todo ���� ����� �� ���. ������ � ������� �������������� ���� ���������� ������� ��� �����
    //1<<26 - 2's power closest to Earth radius
    auto epsilon = std::numeric_limits<decltype (x)>::epsilon() * (static_cast<unsigned int>(1)<<26);
    return fabs(x-rhs.x)<epsilon && fabs(y-rhs.y)<epsilon;
}

UTMPoint GeoPoint::toUTM(int force_zone) const
{
    auto z = force_zone==-1 ? GeoCalc::utmZone(*this) : force_zone;
    auto res = CRS::convert(CRS::EPSG4326, CRS::EPSG_UTM+z, x,y);
    return {res.first, res.second, z};
}

void GeoCalc::toDegMin(double coord, int& deg, double& min)
{
    deg=trunc(coord);
    coord-=deg;
    min=coord*60;
}

void GeoCalc::toDegMinSec(double coord, int& deg, int& min, double& sec)
{
    deg=trunc(coord);
    coord-=deg;
    coord*=60;
    min=coord;
    sec=coord-min;
    sec*=60;
}

std::string GeoCalc::toDegMinString(const GeoPoint& p)
{
    using namespace std;
    int deg;
    double min;
    stringstream ss;
    const auto& lon = p.x, lat=p.y;

    toDegMin(lat, deg, min);
    ss  << ((deg>0) ? 'N' : 'S');
    ss << labs(deg) << "�" << min << '\'';

    toDegMin(lon, deg, min);
    ss << ' '<< ((deg>0) ? 'E' : 'W');
    ss.precision(5);
    ss.width(2);
    ss << labs(deg) << "�" << min << '\'';

    return ss.str();
}


std::string GeoCalc::toDegMinSecString(const GeoPoint& p)
{
    using namespace std;
    int deg;
    int min;
    double sec;
    stringstream ss;
    const auto& lon = p.x, lat=p.y;

    toDegMinSec(lat, deg, min, sec);
    ss << ((deg>0) ? 'N' : 'S');
    ss << labs(deg) << "�" << min << '\'' << sec << "\"";

    toDegMinSec(lon, deg, min, sec);
    ss << ' ' << ((deg>0) ? 'E' : 'W');
    ss.precision(3);
    ss.width(2);
    ss << labs(deg) << "�" << min << '\'' << sec << "\"";

    return ss.str();
}

double GeoCalc::distance(GeoPoint p1, GeoPoint p2)
{
    const double pi_mult=M_PI/180.0;
    auto& lon1 = p1.x, lat1=p1.y,
            lon2 = p2.x, lat2=p2.y;
    lat1*=pi_mult;lon1*=pi_mult;
    lat2*=pi_mult;lon2*=pi_mult;
    double dlon = lon1 - lon2;
    double dlat = lat1 - lat2;
    double a = sqrt(pow(sin(dlat/2),2) + cos(lat1) * cos(lat2) * pow(sin(dlon/2),2));
    a=asin(a)*2*R;
    return a;

}

int GeoCalc::utmZone(const GeoPoint& p)
{
    const auto& lon = p.x;
    int zone;
    if(lon > 0)
        zone=30+static_cast<int>(floor(lon/6));
    else
        zone=static_cast<int>(floor((lon+180)/6));
    return ++zone;
}


unsigned TileRegion::imageWidth()
{
    return se.raw_x() - nw.raw_x();
    //return (se.x-nw.x)*TILE_SIDE + se.off_x - nw.off_x;
}
unsigned TileRegion::imageHeight()
{
    return se.raw_y() - nw.raw_y();
    //return (se.y-nw.y)*TILE_SIDE + se.off_y - nw.off_y;
}

unsigned TileRegion::imageWidthAligned()
{
    return (se.x-nw.x+1)*TILE_SIDE;
}
unsigned TileRegion::imageHeightAligned()
{
    return (se.y-nw.y+1)*TILE_SIDE;
}

