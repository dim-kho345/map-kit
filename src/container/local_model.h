#ifndef LOCALMODEL_H
#define LOCALMODEL_H

#include "model/model_impl.h"

///@todo сделать нормально, с захватом мутекса через -> и освобождением в деструкторе

class LocalModel : public ModelInterface, ServiceQueue
{
public:

    /// опасная херня, т.к. если ServiceQueque::ServiceQueque будет обращаться к еще не созданной cont_model, будет капец
    LocalModel(GUI* p_gui) :
        ServiceQueue(&cont_model,p_gui),
        cont_model(p_gui, *this) {}
    /// @todo remove "request" prefix

    //requests from GUI

    void serviceFinished(ServiceFunction func, int ctx, bool complete) override;

    void requestDownload(MapSource * const src, int zoom, bool guru_maps) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestDownload(src, zoom, guru_maps);
    }
    void requestLoadMap(std::string name, std::string map_path, bool is_topo, int zoom) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestLoadMap(name, map_path, is_topo, zoom);

    }
    void requestStitchTiles(int map_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestStitchTiles(map_id);
    }
    void requestConvert2Ozf(int map_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestConvert2Ozf(map_id);
    }
    void requestConvert2Kmz(int map_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestConvert2Kmz(map_id);
    }
    void requestConvert2Jnx(int map_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestConvert2Jnx(map_id);
    }
    void requestConvert2Guru(int map_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestConvert2Guru(map_id);
    }
    void setGrid(UTMRegion reg, unsigned step, GridOrigin origin) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setGrid(reg, step, origin);
    }

    void requestEnhanceImage(int map_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestEnhanceImage(map_id);
    }

    void setWikiPnt(const std::vector<POIPoint>& pnt) override {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setWikiPnt(pnt);
    }

    void requestWikiPnt() override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestWikiPnt();
    }
    void requestPrintOut(int print_id, std::string printer_name) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestPrintOut(print_id, printer_name);
    }
    void requestZipFolder(int folder_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.requestZipFolder(folder_id);
    }
    void setRegion(const GeoRegion reg) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setRegion(reg);
    }
    void setLocation(const std::string& loc) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setLocation(loc);
    }

    void createUploadScript() override {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.createUploadScript();
    }

    void setHQPoint(const GeoPoint p, std::string place=std::string()) override {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setHQPoint(p, place);
    }

    void setJnxMap(int map_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setJnxMap(map_id);
    }

    void setKmzMap(int map_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setKmzMap(map_id);
    }

    void setGuruTopo(int map_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setGuruTopo(map_id);
    }

    void setGuruSat(int map_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setGuruSat(map_id);
    }

    void deleteMap(int map_id) override{
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.deleteMap(map_id);
    }

    /// forward abort request from GUI to service queue
    void abortService(int queue_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.abortService(queue_id);
    }


    /// get map sources avaialable
    std::vector<MapSource*> enumerateMapSources() override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        return cont_model.enumerateMapSources();
    }

    ModelImpl::ModelState currentState() override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        return cont_model.currentState();
    }

    void setPrintRegion(const GeoRegion reg) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setPrintRegion(reg);
    }
    void setEnhanceParam(int map_id, EnhanceOptions opts) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setEnhanceParam(map_id, opts);
    }

    void setMap4Grp(const GeoRegion reg, int map_id) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setMap4Grp(reg, map_id);
    }

    void setPrintOptions(int print_id, PrintOptions s) override
    {
        std::lock_guard<std::recursive_mutex> mlock(mx);
        cont_model.setPrintOptions(print_id, s);
    }

private:
    std::recursive_mutex mx;
    ModelImpl cont_model;
};


#endif //LOCALMODEL_H
