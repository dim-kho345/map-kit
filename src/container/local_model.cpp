#include "container/local_model.h"


void LocalModel::serviceFinished(ServiceFunction func, int ctx, bool complete)
{
    std::lock_guard<std::recursive_mutex> mlock(mx);
    ServiceQueue::serviceFinished(func, ctx, complete);
}
