#include "gui/mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include <windows.h>
#include "settings.h"
#include "log.h"
#include <string>


WIN32_FIND_DATAA find_data;
LPWIN32_FIND_DATAA pfind_data = &find_data;

int main(int argc, char *argv[])
{
    char current_path[256];
    GetCurrentDirectoryA(sizeof(current_path), current_path);
    std::string s(current_path);
    qputenv("PROJ_LIB",current_path);
    qputenv("GDAL_DATA",current_path);
    s.append("/settings.ini");
    QApplication a(argc, argv);

    try
    {
        settings.openFile(s);
        if(!settings.isValid())
            throw(std::runtime_error("Настройки: settings.ini не открыт"));
        SetCurrentDirectoryA(settings["AssemblyFolder"].c_str());
        MainWindow w;

        return a.exec();
    }
    catch (std::exception& ex)
    {
        QMessageBox::critical(nullptr,"Аварийное завершение",
                              ex.what());
        Logger::write("Terminated: ",ex.what());
    }
    return 0;
}
