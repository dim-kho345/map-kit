#include <stdexcept>
#include "path_resolver.h"
#include "util.h"

PathResolver::PathResolver(time_t unix_date,
             const std::string location,
             int grid_step_) :
    date(unix_date),
    location(location)
{
    switch(grid_step_)
    {
    case 250:
        grid_step = "250m";
        break;
    case 500:
        grid_step = "500m";
        break;
    case 1000:
        grid_step = "1000m";
        break;
    case 0:
        grid_step = "grid_step";
    }
}


std::string PathResolver::kitFolder()
{
    return kitPrefix(date, location);
}

std::string PathResolver::sourcePath(FileType t, bool is_topo)
{
    switch(t)
    {
    case GridGpx:
    case GridKml:
    case GridPlt:
    case GridWpt:
    case GridDmx:
        return src_folder+"grid" + name_suffix.at(t);
    case Coord2Txt:
        return src_folder+"2-Coords.txt";
    case Map4Grp:
        return src_folder+"!4Group.pdf";
    case WikiPntSat:
        return src_folder+"WikiPntSat.wpt";
    case WikiPntTopo:
        return src_folder+"WikiPntTopo.wpt";
    case WikiPntGpi:
        return src_folder+"WikiPnt.gpi";
    case WikiPntGpx:
        return src_folder+"WikiPnt.gpx";
    case Hlg:
        return src_folder+"region.hlg";
    case Readme:
        return "readme.txt";
    case UploadScript:
        return src_folder+"upload.bat";
    default:
        throw std::out_of_range("Resolver: illegal file type requested");
    }

}


std::string PathResolver::mapSourcePath(FileType t, const MapSource * const m_src, int zoom)
{
    return mapSourcePath(t, m_src->name, m_src->is_topo, zoom);
}

std::string PathResolver::mapSourcePath(FileType t, const std::string& name, bool is_topo, int zoom)
{
    return src_folder +
            name +
            (is_topo ? "_topo_z" : "_sat_z") +
            std::to_string(zoom+1) +
            name_suffix.at(t);
}

std::string PathResolver::mapDestinationPath(FileType t, std::string dst_dir, const MapSource * const m_src, int zoom)
{
    std::string res(kitPrefix(date, location));
    res.append('/' + dst_dir + '/');
    res.append(mapDestinationFilename(t, m_src, zoom));
    return res;
}
std::string PathResolver::destinationPath(FileType t, std::string dst_dir)
{
    std::string res = kitPrefix(date, location);
    if(!dst_dir.empty())
        res.append('/' + dst_dir + '/');
    else
        res+='/';
    res.append(destinationFilename(t));
    return res;
}

std::string PathResolver::destinationFilename(FileType t)
{
    switch(t)
    {
    case GridGpx:
    case GridKml:
    case GridPlt:
    case GridWpt:
    case GridDmx:
        return kitPrefix(date, location) + '_' +
                grid_step +
                name_suffix.at(t);
        break;
    case Coord2Txt:
        return "2-Coords.txt";
    case Readme:
        return "1-Read_Me_ver_3.txt";
    case Map4Grp:
        return "!4Group.pdf";
    case WikiPntSat:
        return kitPrefix(date, location) + "_Wiki_Satell.wpt";
    case WikiPntTopo:
        return kitPrefix(date, location) + "_Wiki_Topo.wpt";
    case WikiPntGpi:
        return kitPrefix(date, location) + "_Wiki.gpi";
    case WikiPntGpx:
        return kitPrefix(date, location) + "_Wiki.gpx";
    case Hlg:
        return kitPrefix(date, location)+".hlg";
    case UploadScript:
        return "upload.bat";
    default:
        throw std::range_error("Resolver: illegal file type");
    }
}


std::string PathResolver::mapDestinationFilename(FileType t,
                                                 const MapSource * const m_src,
                                                 int zoom,
                                                 PrintFormat paper_size,
                                                 bool is_splitted)
{
    std::string res(kitPrefix(date, location));
    res.append(m_src->is_topo ? "_Topo_" : "_Satell_" );
    if(t != Jnx)
        res.append(m_src->name + '_');
    if(t == Printout)
    {
        res.append(grid_step + '_');
        res.append('z' + std::to_string(zoom+1) + '_');
        if(is_splitted)
            res.append("skleyka_A4");
        else
            res.append(print_fmt.at(paper_size));
    }
    else
    {
        res.append('z' + std::to_string(zoom+1));
    }
    res.append(name_suffix.at(t));
    return res;
}

std::string PathResolver::src_folder;
const  std::map<PathResolver::FileType,std::string> PathResolver::name_suffix{
    {PathResolver::GridWpt, ".wpt"},
    {PathResolver::GridGpx, ".gpx"},
    {PathResolver::GridPlt, ".plt"},
    {PathResolver::GridKml, ".kml"},
    {PathResolver::GridDmx, ".txt"},
    {PathResolver::Kmz, ".kmz"},
    {PathResolver::Jnx, ".jnx"},
    {PathResolver::Guru, ".sqlitedb"},
    {PathResolver::Ozf, ".ozf2"},
    {PathResolver::Map, ".map"},
    {PathResolver::OzfMap, "_ozf.map"},
    {PathResolver::Image, ".bmp"},
    {PathResolver::Icon, "_ico.bmp"},
    {PathResolver::Printout, ".pdf"}
};


 const std::map<PrintFormat, std::string> PathResolver::print_fmt{
     {A0,"A0"},{A1,"A1"},{A2,"A2"},{A3,"A3"},{A4,"A4"}
 };
