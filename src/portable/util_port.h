#include <string>

/// get time zone shift in seconds, e.g. "+3" -> 180
int timeZoneShift();

bool folderExists();
