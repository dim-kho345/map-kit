# Состав репозитория

**assets** - файлы в этом каталоге должны лежать рядом с основным исполняемым map-ka.exe. *Wiki2Wpt* идет вместе default.ini, *gpsbabel* - инструмент для конвертации точек и треков, *nono_list.txt* - список фильтруемых компонентов имени wiki точек, *7z* используется для упаковки каталогов, *rmaps0.exe* - сервис карт (аналог сборника zmp для SASPlanet)

**doc** - описание архитектуры и всякие полезные знания

**lib** - готовые библиотеки с бинарниками. Для отладочных сборок DLL также должны делать в Debug, а набор из assets - рядом с исполняемым файлом.
Файлы *lib/share/gdal/***.csv* и *lib/share/proj/proj.db* также должны находиться рядом с *map-ka.exe*, для корректной работы GDAL и PROJ

**resource** - ассеты для Qt проекта

**src** - исходники

# Сборка

## Список библиотек

Сразу советую не качать откуда-то готовые бинарники разных библиотек, собранные разными компиляторами в разных окружениях.
Придется учитывать, на чем собран сам Qt, совместимость ABI библиотек между собой, MS Runtime DLL-ом и т.п.
Трудно, но тем не менее проще и надежней собирать самому

Для сборки потребуются следующие библиотеки (приведены в порядке зависимости aka порядке сборки)

1. zlib:

    http://zlib.net.ru/

    https://github.com/zlib-ng/zlib-ng

2. libzip:

    https://libzip.org/

    https://github.com/nih-at/libzip

3. libpng:

    http://www.libpng.org/pub/png/libpng.html

    http://www.libpng.org/pub/png/libpng.html

4. Turbo JPEG:

    https://libjpeg-turbo.org

    https://github.com/libjpeg-turbo/libjpeg-turbo

5. sqlite3:

    https://www.sqlite.org/download.html

    sqlite3: скачать "amalgamation source code" (одним куском) и собрать с помощью следующего скрипта Cmake:

    ```
    project(sqlite3)

    add_executable(shell shell.c)
    add_library(sqlite3 SHARED sqlite3.c sqlite3.h sqlite3ext.h)
    target_link_libraries(shell sqlite3)
    ```
6. PROJ:

    https://proj.org/download.html

    https://github.com/OSGeo/PROJ

    Библиотека геодезических преобразований, также является зависимостью GDAL

7. GDAL:
    
    https://gdal.org/download.html

    https://github.com/OSGeo/gdal

    К сожалению, в GDAL еще не добавили поддержку CMake. Поэтому придется либо собирать в linux/msys2 окружении с помощью autotools (скрипт для configure приведен ниже), либо внимательно пройтись по *nmake.opt* - для MSVC.
    Скрипт сборки makefile: 
    `./configure --enable-static=no --disable-all-optional-drivers --with-libz=/c/msys64/usr/local --with-proj=/c/msys64/usr/local --with-png=/c/msys64/usr/local --with-sqlite3=/c/msys64/usr/local --with-jpeg=/c/msys64/usr/local  --with-cpp14 --with-hide-internal-symbols=yes --with-rename-internal-libtiff-symbols=yes --enable-driver-bmp --enable-driver-map`

    обращаю внимание, что пути к библиотекам в этом скрипте - к каталогу, в котором находятся bin, include, lib,share, а не непосредственно путь к каталогу с `*.dll.a/*.a` библиотеками

8. OpenCV:

    https://opencv.org/

    https://github.com/opencv/opencv

9. OpenSSL:

    https://www.openssl.org/source/

    https://github.com/openssl/openssl
    
    лично я не собирал, использовал из пакета mingw-w64-openssl в составе msys2

10. Программа GPSBabel:

    https://www.gpsbabel.org/index.html

    https://github.com/gpsbabel/gpsbabel

## Использование готовых библиотек

Можно также использовать собранные библиотеки, расположенные в каталоге lib. Собрано с помощью компилятора mingw64 (отдельно ставился в msys2) и Qt 5.14.2 (думаю подойдет любая версия >5.14.2, насчет 6.x не знаю). В Qt был выбран комплект mingw64 из msys2, а не родной - ради C++17 (хотелось fold expressions), но это легко обойти, а вот autotools с Qt не поставляется, а они нужны для GDAL.

GPSBabel используется для конвертации в GPI формат. Он располагается в каталоге assets и собран напротив рабочей версии Qt

Версия mingw64 в комплекте Qt: `g++ (x86_64-posix-seh-rev0, Built by MinGW-W64 project) 7.3.0`

Версия mingw64 из msys: `g++ (Rev1, Built by MSYS2 project) 10.2.0` 

## Выбор среды

В Windows можно собирать с помощью родного MSVC++ либо в MSYS2 окружении и mingw64.
В любой среде настраивать сборку удобней в cmake-gui. В зависимостях проектов указывать путь к уже собранных библиотекам и заголовкам. GDAL и OpenCV сами содержат в своем составе zlib, libpng, jpeg и прочие кодеки. Их нужно, соответственно, повыключать в cmake-gui. Среди поддерживаемых форматов указывать только jpeg, png, bmp. GDAL также должен поддерживать работу с Ozi MAP: `--enable-driver-map`

В CMake не забывать про CMAKE_INSTALL_PREFIX, чтобы с помощью `cmake --install .` получать готовое дерево с bin, lib и include (для MSYS2)

Qt должен также обязательно быть собран с помощью соответсвующего компилятора.

Если использовать MSYS2, компилируемые библиотеки лучше традиционно коллекционировать где нибудь в c:/msys64/usr/local/. Хотя принципиальной разницы, где они лежат - нет.
